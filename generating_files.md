Generating files. PlasmidSPAdes, Recycler, cBAR and Recycler reconstruction
================

-   [Running SPAdes, PlasmidSPAdes and Recycler](#running-spades-plasmidspades-and-recycler)
-   [Reconstruction per genome](#reconstruction-per-genome)
    -   [Running cBAR and PlasmidFinder. Parsing and comparing the predictions from PlasmidSPAdes, Recycler, cBAR and PlasmidFinder.](#running-cbar-and-plasmidfinder.-parsing-and-comparing-the-predictions-from-plasmidspades-recycler-cbar-and-plasmidfinder.)
-   [Reconstruction per plasmid](#reconstruction-per-plasmid)

Running SPAdes, PlasmidSPAdes and Recycler
------------------------------------------

The script [pipeline\_hpc.sh](/scripts/pipeline_hpc.sh) was used to perform *de novo* assembly of the 42 genome projects. The script has two arguments:

-   1st: SRA link
-   2nd: Three possible flags: --pspades; -- recycler; --both

The SRA link can be found in [all\_genomes.txt](all_genomes.txt). The flag "--both" runs the entire analysis. The analysis was carried out on a high performance computer running CentOS7 and contigs larger than 500 bp were filtered out. For example we run the analysis with the genome project: *Citrobacter freundii* CFNIH1 in the following way:

``` r
bash pipeline_hpc.sh SRR1284629 --both
```

This would create two new folders:

-   /new\_results/pspades\_SRR1284629
-   /new\_results/recycler\_SRR1284629

Within the folder "recycler\_SRR1284269" we also find the results from SPAdes 3.8.2.

Reconstruction per genome
-------------------------

### Running cBAR and PlasmidFinder. Parsing and comparing the predictions from PlasmidSPAdes, Recycler, cBAR and PlasmidFinder.

The script [analysis\_per\_genome.sh](analysis_per_genome.sh) required three arguments.

-   1st: SRA link
-   2nd: Short name to identify the strain
-   3rd: Complete name from the strain

``` r
bash pipeline_hpc.sh SRR1284629 CFNIH1 Citrobacter_freundii_CFNIH1
```

After running the command a new folder was created:

-   /Analysis/CAV1321

This directory contained the results from PlasmidSPAdes, Recycler, cBAR and PlasmidFinder for that strain.

-   /Analysis/All\_Genomes

In the above directory, the results from each genome project for each program were written. E.g. the results of all the 42 genome projects of Recycler are available in the file [all\_Recycler\_R](/Analysis/All_Genomes/all_Recycler_R). The files in that directory were the input for [detailed R analysis](detailed_analysis.md)

Reconstruction per plasmid
--------------------------

Once the reconstruction per genome was completed, we performed the reconstruction per plasmid. The script [coverage\_per\_plasmid.sh](coverage_per_plasmid.sh) was used to compare the plasmid predictions from PlasmidSPAdes, Recycler, cBAR and PlasmidFinder against each reference plasmid.

``` r
bash coverage_per_plasmid.sh
```

This script generated the file [Benchmark\_perPlasmid.csv](/Analysis/All_Genomes/Benchmark_perPlasmid.csv) which is another input for [detailed R analysis](detailed_analysis.md)
