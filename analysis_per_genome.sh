#!/bin/bash


##Checks
##Check for command line arguments
if [ $# -eq 0 ]; then
    echo """
############################
Analysis_per_genome.sh
############################
This script is used to obtain to compare the predictions from PlasmidSPAdes, Recycler, cBAR and PlasmidFinder to all plasmids of a certain genome.

The script works with three arguments: 
1st SRA_Number
2nd Short_name_identifying_strain
3rd Complete_name_strain. 
      
As an example, run: 
      
    bash analysis_per_genome.sh	SRR2965690 CAV1321  Citrobacter_freundii_strain_CAV1321
	     
After running the command you should see the following folder /Analysis/CAV1321 with all the results from PlasmidSPAdes,Recycler,cBar and PlasmidFind
er. Additionally within the folder /Analysis/All_Genomes you will see different files containning information to generate the figures in R. 
      
To run the analysis for all the genomes then use:
      
   while read line; do bash analysis_per_genome.sh $line; done < all_genomes.txt"""
 exit
fi


HOME=$(pwd)

if [[ ! $(python --version 2>&1) == *2\.7* ]]; then
 printf "Python 2.7 required but not found. Aborting\n"; exit 1
fi

command python $CURRENTDIR/programmes/quast-4.1/quast.py\ 
 >  /dev/null 2>&1 || { echo >&2 "QUAST required but not found in folder programmes or exits with error. Aborting."; exit 1; }
command -v blastn > /dev/null 2>&1 || { echo >&2 "ncbi-blast required but not found. Aborting."; exit 1; }



#We redefine and export some variables to use them later in a house-developed python script (parsing_results.py). 

strain=$2
name_complete_strain=$3
export "strain"
export "name_complete_strain"
#We run Quast using the results from PlasmidSPAdes, in the file "contigs.fasta" we have the results from the program. 

python $HOME/programmes/quast-4.1/quast.py $HOME/resultsspades/$2/contigs.fasta -R $HOME/completegenomes/$2_all_references.fasta -o $HOME/Analysis/$2/PlasmidSPAdes/quast_analysis_genome --min-alignment 500 --ambiguity-usage all 

python $HOME/programmes/quast-4.1/quast.py $HOME/resultsspades/$2/contigs.fasta -R $HOME/completegenomes/merged_plasmids/$2_plasmids_references.fasta -o $HOME/Analysis/$2/PlasmidSPAdes/quast_analysis_plasmids --min-alignment 500 --ambiguity-usage all 

python $HOME/programmes/quast-4.1/quast.py $HOME/resultsspades/$2/contigs.fasta -R $HOME/completegenomes/chromosome/$2_chromosome_references.fasta -o $HOME/Analysis/$2/PlasmidSPAdes/quast_analysis_chromosome --min-alignment 500 --ambiguity-usage all 



# We run Quast using the results from Recycler. Different runs are necessary because the reference genomes are different in each case. 1st case: we merged in a single file plasmids and chromosome(s);
# 2nd case: the fasta file only contained the plasmid of the strain ; 3rd case: the fasta file only contained the chromosome(s) of the strain. 

python $HOME/programmes/quast-4.1/quast.py $HOME/resultsrecycler/$2/assembly_graph.cycs.fasta -R $HOME/completegenomes/$2_all_references.fasta -o $HOME/Analysis/$2/Recycler/quast_analysis_genome --min-alignment 500 --ambiguity-usage all 

python $HOME/programmes/quast-4.1/quast.py $HOME/resultsrecycler/$2/assembly_graph.cycs.fasta -R $HOME/completegenomes/merged_plasmids/$2_plasmids_references.fasta -o $HOME/Analysis/$2/Recycler/quast_analysis_plasmids --min-alignment 500 --ambiguity-usage all 

python $HOME/programmes/quast-4.1/quast.py $HOME/resultsrecycler/$2/assembly_graph.cycs.fasta -R $HOME/completegenomes/chromosome/$2_chromosome_references.fasta -o $HOME/Analysis/$2/Recycler/quast_analysis_chromosome --min-alignment 500 --ambiguity-usage all 


pspades_plasmid_genome_fraction=$(awk '/Genome fraction/ {print $4;}' $HOME/Analysis/$2/PlasmidSPAdes/quast_analysis_plasmids/report.txt)
pspades_chromosome_genome_fraction=$(awk '/Genome fraction/ {print $4;}' $HOME/Analysis/$2/PlasmidSPAdes/quast_analysis_chromosome/report.txt)


###Checking the project, remove this part in case it does not work####
####python $HOME/programmes/quast-4.1/quast.py $HOME/completegenomes/merged_plasmids/$2_plasmids_references.fasta  -R $HOME/resultsrecycler/$2/assembly_graph.cycs.fasta -o $HOME/Analysis/$2/Recycler/Test/quast_analysis_plasmids --min-alignment 500 --ambiguity-usage all
####python $HOME/programmes/quast-4.1/quast.py $HOME/completegenomes/chromosome/$2_chromosome_references.fasta -R $HOME/resultsrecycler/$2/assembly_graph.cycs.fasta -o $HOME/Analysis/$2/Recycler/Test/quast_analysis_chromosome --min-alignment 500 --ambiguity-usage all
##########################################

echo "Plasmid coverage information, plasmidSPAdes:" >> $HOME/Analysis/$2/Coverage_Information
echo $pspades_plasmid_genome_fraction >> $HOME/Analysis/$2/Coverage_Information
export "pspades_plasmid_genome_fraction"
 
echo "Chromosome coverage information, plasmidSPAdes:" >> $HOME/Analysis/$2/Coverage_Information
echo $pspades_chromosome_genome_fraction >> $HOME/Analysis/$2/Coverage_Information
export "pspades_chromosome_genome_fraction"

recycler_plasmid_genome_fraction=$(awk '/Genome fraction/ {print $4;}' $HOME/Analysis/$2/Recycler/quast_analysis_plasmids/report.txt)
recycler_chromosome_genome_fraction=$(awk '/Genome fraction/ {print $4;}' $HOME/Analysis/$2/Recycler/quast_analysis_chromosome/report.txt)


echo "Plasmid coverage information, Recycler:" >> $HOME/Analysis/$2/Coverage_Information
echo $recycler_plasmid_genome_fraction >> $HOME/Analysis/$2/Coverage_Information
export "recycler_plasmid_genome_fraction"
 
echo "Chromosome coverage information, Recycler:" >> $HOME/Analysis/$2/Coverage_Information
echo $recycler_chromosome_genome_fraction >> $HOME/Analysis/$2/Coverage_Information
export "recycler_chromosome_genome_fraction"

#######We run cBar using the contigs from SPAdes 3.8 ##############################

$HOME/programmes/cBar.1.2/cBar.pl $HOME/resultsrecycler/$2/contigs.fasta  $HOME/resultscbar/$2_cbar_analysis


python $HOME/programmes/quast-4.1/quast.py $HOME/resultsrecycler/$2/contigs.fasta -R $HOME/completegenomes/$2_all_references.fasta -o $HOME/Analysis/$2/Contig_Information --min-alignment 500 --ambiguity-usage all 


#######We check if the same strain was analyzed previously##########################

if [ -s $HOME/Analysis/All_Genomes/all_plasmidSPAdes_R ]
  then
    	sed -i "/^$3/d"  $HOME/Analysis/All_Genomes/all_plasmidSPAdes_R
fi	
	
if [ -s $HOME/Analysis/All_Genomes/all_Recycler_R ]
  then
    	sed -i "/^$3/d"  $HOME/Analysis/All_Genomes/all_Recycler_R
fi

if [ -s $HOME/Analysis/All_Genomes/all_cbar_R ]
  then
    	sed -i "/^$3/d"  $HOME/Analysis/All_Genomes/all_cbar_R
fi

if [ -s $HOME/Analysis/All_Genomes/all_plasmidfinder_R ]
  then
    	sed -i "/^$3/d"  $HOME/Analysis/All_Genomes/all_plasmidfinder_R
fi

if [ -s $HOME/Analysis/All_Genomes/cBAR_recall ]
  then
    	sed -i "/^$3/d"  $HOME/Analysis/All_Genomes/cBAR_recall
fi

if [ -s $HOME/Analysis/All_Genomes/PlasmidFinder_recall ]
  then
    	sed -i "/^$3/d"  $HOME/Analysis/All_Genomes/PlasmidFinder_recall
fi

############Exporting the contigs from SPAdes 3.8########################
if [ -s $HOME/resultsrecycler/$2/contigs.fasta ]
  then
	contigs_spades=$(ls $HOME/resultsrecycler/$2/contigs.fasta ) 
	export "contigs_spades"
else
	echo "Something is wrong with SPAdes, please check"
fi
if [ -s $HOME/Analysis/$2/Contig_Information/contigs_reports/nucmer_output/contigs.unaligned ]
  then
      unaligned_contigs_spades=$(ls $HOME/Analysis/$2/Contig_Information/contigs_reports/nucmer_output/contigs.unaligned )
      export "unaligned_contigs_spades"
else
	echo -n "" > $HOME/Analysis/$2/Contig_Information/contigs_reports/nucmer_output/contigs.unaligned
	unaligned_contigs_spades=$(ls $HOME/Analysis/$2/Contig_Information/contigs_reports/nucmer_output/contigs.unaligned)
	export "unaligned_contigs_spades"
fi	

##################PlasmidSPAdes##########################################

# # The following cmd lines were used to extract and export information from plasmidSPAdes to parsing_results.py
# 
chr_coverage=$(grep "genomic coverage is" $HOME/resultsspades/pspades_$1/spades.log | cut -d 's' -f 3 | cut -d 'c' -f 1)
echo "Median coverage calculated by plasmidSPAdes (assembly graph):" >> $HOME/Analysis/$2/Coverage_Information
echo $chr_coverage >> $HOME/Analysis/$2/Coverage_Information
export "chr_coverage" ## This variable will be used in parsing_results.py

if [ -s $HOME/resultsspades/$2/contigs.fasta ]
  then
	contigs_plasmidspades=$(ls $HOME/resultsspades/$2/contigs.fasta ) 
	export "contigs_plasmidspades"
else
	echo "Something is wrong with plasmidSPAdes, please check"
fi


if [ -s $HOME/Analysis/$2/PlasmidSPAdes/quast_analysis_genome/contigs_reports/alignments* ]
  then
	file_alignments_plasmidspades=$(ls $HOME/Analysis/$2/PlasmidSPAdes/quast_analysis_genome/contigs_reports/alignments*)
	export "file_alignments_plasmidspades"
else
	echo -n "" > $HOME/Analysis/$2/PlasmidSPAdes/quast_analysis_genome/contigs_reports/alignments*
	file_alignments_plasmidspades=$(ls $HOME/Analysis/$2/PlasmidSPAdes/quast_analysis_genome/contigs_reports/alignments*)
	export "file_alignments_plasmidspades"
fi

if [ -s $HOME/Analysis/$2/PlasmidSPAdes/quast_analysis_genome/contigs_reports/nucmer_output/contigs.unaligned ]
  then
	file_unalignments_plasmidspades=$(ls $HOME/Analysis/$2/PlasmidSPAdes/quast_analysis_genome/contigs_reports/nucmer_output/contigs.unaligned)
	export "file_unalignments_plasmidspades"
else
	echo -n "" > $HOME/Analysis/$2/PlasmidSPAdes/quast_analysis_genome/contigs_reports/nucmer_output/contigs.unaligned
	file_unalignments_plasmidspades=$(ls $HOME/Analysis/$2/PlasmidSPAdes/quast_analysis_genome/contigs_reports/nucmer_output/contigs.unaligned)
	export "file_unalignments_plasmidspades"
fi


#######################Recycler################################################

if [ -s $HOME/resultsrecycler/$2/assembly_graph.cycs.fasta ]
  then
	plasmids_recycler=$(ls $HOME/resultsrecycler/$2/assembly_graph.cycs.fasta ) 
	export "plasmids_recycler"
else
	echo "Something is wrong with Recycler, please check"
	export "plasmids_recycler"
fi

if [ -s $HOME/Analysis/$2/Recycler/quast_analysis_genome/contigs_reports/alignments* ]
  then
      	echo "There are contigs aligning to the reference plasmids"
	file_alignments_recycler=$(ls $HOME/Analysis/$2/Recycler/quast_analysis_genome/contigs_reports/alignments*)
	echo "$file_alignments_recycler"
	export "file_alignments_recycler"
else
	echo -n "" > $HOME/Analysis/$2/Recycler/quast_analysis_genome/contigs_reports/alignments*
	file_alignments_recycler=$(ls $HOME/Analysis/$2/Recycler/quast_analysis_genome/contigs_reports/alignments*)
	export "file_alignments_recycler"
fi

if [ -s $HOME/Analysis/$2/Recycler/quast_analysis_genome/contigs_reports/nucmer_output/assembly_graph.cycs.unaligned ]
  then
	file_unalignments_recycler=$(ls $HOME/Analysis/$2/Recycler/quast_analysis_genome/contigs_reports/nucmer_output/assembly_graph.cycs.unaligned)
	export "file_unalignments_recycler"
else
	echo -n "" > $HOME/Analysis/$2/Recycler/quast_analysis_genome/contigs_reports/nucmer_output/assembly_graph.cycs.unaligned
	file_unalignments_recycler=$(ls $HOME/Analysis/$2/Recycler/quast_analysis_genome/contigs_reports/nucmer_output/assembly_graph.cycs.unaligned)
	export "file_unalignments_recycler"
fi


#########################cBAR####################################################################

mkdir -p $HOME/Analysis/$2/cBAR

if [ -s $HOME/resultscbar/$2_cbar_analysis ]
  then
	cbar_spades=$(ls $HOME/resultscbar/$2_cbar_analysis ) 
	export "cbar_spades"
else
	echo "Something is wrong, please check"
fi

if [ -s $HOME/Analysis/$2/Contig_Information/contigs_reports/alignments* ]
  then
      	contigs_alignments=$(ls $HOME/Analysis/$2/Contig_Information/contigs_reports/alignments*)
	export "contigs_alignments"
else
	echo -n "" > $HOME/Analysis/$2/Contig_Information/contigs_reports/alignments
	contigs_alignments=$(ls $HOME/Analysis/$2/Contig_Information/contigs_reports/alignments)
	export "contigs_alignments"
fi




######################PlasmidFinder###########################################

##We start the analysis of the contigs using PlasmidFinder. The database of PlasmidFinder contains 121 replicon sequences, we select a percentage identity of 80% as described elsewhere. 
mkdir -p $HOME/Analysis/$2/PlasmidFinder

blastn -query $HOME/resultsrecycler/$2/contigs.fasta -db $HOME/programmes/plasmidfinder/plasmid_database.fsa -out $HOME/Analysis/$2/PlasmidFinder/$2_plasmidfinder.xml -evalue 0.00001 -outfmt 5 -perc_identity 80

file_plasmidfinder=$(ls $HOME/Analysis/$2/PlasmidFinder/$2_plasmidfinder.xml)
plasmidfinder_database=$(ls $HOME/programmes/plasmidfinder/plasmid_database.fsa)
export "file_plasmidfinder"
export "plasmidfinder_database"

export "PWD"
python $HOME/scripts/plasmidfinder_parse.py

plasmidfinder_results=$(ls  $HOME/Analysis/$2/PlasmidFinder/plasmidfinder_results )
export "plasmidfinder_results"
 


 
############Parsing results############################
mkdir -p $HOME/Analysis/All_Genomes
python $HOME/scripts/parsing_results_2.py



############Calculating Recall for cBAR and PlasmidFinder

###cBAR
python $HOME/programmes/quast-4.1/quast.py $HOME/Analysis/$2/cBAR/cBAR_recall -R $HOME/completegenomes/merged_plasmids/$2_plasmids_references.fasta -o $HOME/Analysis/$2/cBAR/quast_cbar_plasmids --min-alignment 500 --ambiguity-usage all 


if [ -s $HOME/Analysis/All_Genomes/cBAR_recall ]
  then
    	echo ""
  else
	printf "Strain\tRecall\n" >> $HOME/Analysis/All_Genomes/cBAR_recall
fi

cbar_genome_fraction=$(awk '/Genome fraction/ {print $4;}' $HOME/Analysis/$2/cBAR/quast_cbar_plasmids/report.txt)
printf "$name_complete_strain\t$cbar_genome_fraction\n" >> $HOME/Analysis/All_Genomes/cBAR_recall


###PlasmidFinder
python $HOME/programmes/quast-4.1/quast.py $HOME/Analysis/$2/PlasmidFinder/PlasmidFinder_recall -R $HOME/completegenomes/merged_plasmids/$2_plasmids_references.fasta -o $HOME/Analysis/$2/PlasmidFinder/quast_plasmidfinder_plasmids --min-alignment 500 --ambiguity-usage all 

if [ -s $HOME/Analysis/All_Genomes/PlasmidFinder_recall ]
  then
    	echo ""
  else
	printf "Strain\tRecall\n" >> $HOME/Analysis/All_Genomes/PlasmidFinder_recall
fi


if [ -s $HOME/Analysis/$2/PlasmidFinder/PlasmidFinder_results ]
  then
    	echo ""
  else
	printf "$3\t0\t-\t-\t-\n" >> $HOME/Analysis/All_Genomes/all_plasmidfinder_R
fi



plasmidfinder_genome_fraction=$(awk '/Genome fraction/ {print $4;}' $HOME/Analysis/$2/PlasmidFinder/quast_plasmidfinder_plasmids/report.txt)
printf "$name_complete_strain\t$plasmidfinder_genome_fraction\n" >> $HOME/Analysis/All_Genomes/PlasmidFinder_recall



