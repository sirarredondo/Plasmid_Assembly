Analysis and metrics reported
================

-   [Loading files and libraries](#loading-files-and-libraries)
-   [Test data](#test-data)
-   [Reconstruction per plasmid](#reconstruction-per-plasmid)
-   [Additional analysis (Supplementary Data)](#additional-analysis-supplementary-data)
    -   [PlasmidSPAdes structural report](#plasmidspades-structural-report)
    -   [Recycler chromosome fraction analysis](#recycler-chromosome-fraction-analysis)
-   [Reconstruction per genome](#reconstruction-per-genome)
    -   [PlasmidSPAdes](#plasmidspades)
        -   [Translocations between chromosome and plasmids](#translocations-between-chromosome-and-plasmids)
        -   [Dealing with repeated sequences](#dealing-with-repeated-sequences)
        -   [Downstream analysis](#downstream-analysis)
        -   [Summary of the results](#summary-of-the-results)
        -   [Overall precision and recall](#overall-precision-and-recall)
    -   [Recycler](#recycler)
        -   [Summary of the results](#summary-of-the-results-1)
        -   [Overall precision and recall](#overall-precision-and-recall-1)
    -   [cBAR](#cbar)
        -   [Translocations between chromosome and plasmids](#translocations-between-chromosome-and-plasmids-1)
        -   [Summary of the results](#summary-of-the-results-2)
        -   [Overall precision and recall](#overall-precision-and-recall-2)
    -   [PlasmidFinder](#plasmidfinder)
        -   [Summary of the results](#summary-of-the-results-3)
        -   [Overall recall and precision](#overall-recall-and-precision)
    -   [Visualization](#visualization)
        -   [Plotting the frequencies](#plotting-the-frequencies)
        -   [Plotting recall](#plotting-recall)

Loading files and libraries
---------------------------

Loading all the files necessary to reproduce our analysis

``` r
setwd("/home/arredondo/Data/All_Genomes/")
length_reference_plasmids <- read.table(file = "length_plasmids.txt", 
    header = TRUE, sep = "\t")
benchmarktablefile <- "Benchmark_perPlasmid.csv"
data_pspades <- read.table(file = "all_plasmidSPAdes_R", header = TRUE, 
    sep = "\t")
data_recycler <- read.table(file = "all_Recycler_R", header = TRUE, 
    sep = "\t")
data_cbar <- read.table(file = "all_cbar_R", header = TRUE, sep = "\t")
data_cbar_recall = read.table(file = "cBAR_recall", header = TRUE, 
    sep = "\t")
data_plasmidfinder <- read.table(file = "all_plasmidfinder_R", 
    header = TRUE, sep = "\t")
data_plasmidfinder_recall = read.table(file = "PlasmidFinder_recall", 
    header = TRUE, sep = "\t")
```

``` r
library(stats)
library(ggplot2)
library(VennDiagram)
# library(venneuler)
```

Test data
---------

The performance of the programmes was evaluated on a range of bacterial species. We selected 42 complete genome sequences previously sequenced by PacBio and Illumina Miseq or Hiseq. In total the test data contained 148 plasmids with a size from 1.55 kbp up to 338.85 kbp.

We need to sort this file in different ways to visualize better our data. So, we sort the data alphabetically and by total plasmid length (kbp)

``` r
number_order_reference <- length_reference_plasmids[order(length_reference_plasmids$Length), 
    ]
alpha_order_reference <- length_reference_plasmids[order(length_reference_plasmids$Strain), 
    ]
number_order_reference$Length = number_order_reference$Length/1000
head(number_order_reference)
```

    ##                                Strain  Length      Reference
    ## 4  Burkholderia_cenocepacia_DDS_22E-1   0.000 Total_plasmids
    ## 5           Bacillus_subtilis_BEST195   5.838 Total_plasmids
    ## 9      Enterobacter_aerogenes_CAV1320  13.981 Total_plasmids
    ## 2     Providencia_stuartii_ATCC_33672  48.866 Total_plasmids
    ## 41 Corynebacterium_callunae_DSM_20147  89.132 Total_plasmids
    ## 14       Enterobacter_cloacae_CAV1411 124.062 Total_plasmids
    ##    Identification
    ## 4       Reference
    ## 5       Reference
    ## 9       Reference
    ## 2       Reference
    ## 41      Reference
    ## 14      Reference

``` r
tail(number_order_reference)
```

    ##                           Strain  Length      Reference Identification
    ## 24 Klebsiella_pneumoniae_CAV1193 555.544 Total_plasmids      Reference
    ## 35   Kluyvera_intermedia_CAV1151 637.318 Total_plasmids      Reference
    ## 38   Enterobacter_cloacae_ECNIH2 649.705 Total_plasmids      Reference
    ## 32    Klebsiella_pneumoniae_PMK1 673.737 Total_plasmids      Reference
    ## 34 Klebsiella_pneumoniae_KPNIH27 890.805 Total_plasmids      Reference
    ## 22    Klebsiella_oxytoca_CAV1374 969.812 Total_plasmids      Reference

We see the first strain corresponds to Burkholderia cenocepacia which does not carry any reference plasmid but contained three chromosomes with a length of 1.17 Mbp, 3.21 Mbp and 3.67 Mbp. The most complex composition of plasmids corresponds to Klebsiella oxytoca CAV1374 with eleven plasmids.

However, we are also interested in the length of each reference plasmid separately so we load two vectors with the information per plasmid and bacterial isolate.

``` r
plasmid_sizes = c(5838, 13981, 48866, 4109, 85023, 33610, 90452, 
    33610, 90452, 3223, 33610, 90452, 43433, 85187, 170926, 5167, 
    55956, 110040, 1552, 5631, 198307, 121030, 54064, 38384, 
    2927, 40939, 77801, 96702, 43621, 49832, 130719, 1552, 5167, 
    5631, 130603, 107507, 272297, 1919, 10403, 51321, 58427, 
    59138, 96842, 40448, 133484, 113639, 189452, 63692, 39138, 
    212180, 88023, 85473, 58050, 194877, 176943, 111227, 50333, 
    3223, 6393, 69158, 73100, 199444, 1916, 3223, 16257, 100873, 
    109688, 129196, 15096, 243824, 113639, 15096, 243824, 113639, 
    142858, 224457, 26450, 5410, 68910, 111395, 113105, 113992, 
    62247, 255013, 60388, 50333, 284894, 36707, 106559, 5410, 
    92095, 113105, 117623, 115319, 105284, 124310, 52135, 114179, 
    100827, 1916, 3223, 3820, 4310, 4938, 44846, 70610, 135117, 
    243709, 133397, 193725, 205586, 3741, 39554, 77808, 176497, 
    250396, 3741, 257944, 166486, 49565, 77808, 43621, 82986, 
    215092, 295619, 319976, 282439, 47290, 187571, 111693, 69947, 
    304526, 268334, 80411, 338850, 89770, 113440, 227680, 332956, 
    1919, 6538, 14274, 16069, 33610, 49200, 53596, 83652, 150318)
bacterial_isolates = factor(c("Bacillus_subtilis_BEST195", "Enterobacter_aerogenes_CAV1320", 
    "Providencia_stuartii_ATCC_33672", "Corynebacterium_callunae_DSM_20147", 
    "Corynebacterium_callunae_DSM_20147", "Enterobacter_cloacae_CAV1411", 
    "Enterobacter_cloacae_CAV1411", "Enterobacter_cloacae_CAV1669", 
    "Enterobacter_cloacae_CAV1669", "Enterobacter_cloacae_CAV1311", 
    "Enterobacter_cloacae_CAV1311", "Enterobacter_cloacae_CAV1311", 
    "Enterobacter_cloacae_CAV1668", "Enterobacter_cloacae_CAV1668", 
    "Klebsiella_pneumoniae_KPN223", "Escherichia_coli_JJ1886", 
    "Escherichia_coli_JJ1886", "Escherichia_coli_JJ1886", "Escherichia_coli_JJ1886", 
    "Escherichia_coli_JJ1886", "Aeromonas_veronii_AVNIH1", "Klebsiella_pneumoniae_AATZP", 
    "Klebsiella_pneumoniae_AATZP", "Klebsiella_pneumoniae_AATZP", 
    "Klebsiella_pneumoniae_CAV1596", "Klebsiella_pneumoniae_CAV1596", 
    "Klebsiella_pneumoniae_CAV1596", "Klebsiella_pneumoniae_CAV1596", 
    "Klebsiella_pneumoniae_CAV1392", "Klebsiella_pneumoniae_CAV1392", 
    "Klebsiella_pneumoniae_CAV1392", "Escherichia_coli_JJ1887", 
    "Escherichia_coli_JJ1887", "Escherichia_coli_JJ1887", "Escherichia_coli_JJ1887", 
    "Escherichia_coli_JJ1887", "Citrobacter_freundii_CFNIH1", 
    "Enterobacter_asburiae_CAV1043", "Enterobacter_asburiae_CAV1043", 
    "Enterobacter_asburiae_CAV1043", "Enterobacter_asburiae_CAV1043", 
    "Enterobacter_asburiae_CAV1043", "Enterobacter_asburiae_CAV1043", 
    "Klebsiella_pneumoniae_KPNIH36", "Klebsiella_pneumoniae_KPNIH36", 
    "Klebsiella_pneumoniae_KPNIH36", "Enterococcus_faecium_700221", 
    "Enterococcus_faecium_700221", "Enterococcus_faecium_700221", 
    "Escherichia_coli_EC0889", "Escherichia_coli_EC0889", "Klebsiella_pneumoniae_KPNIH24", 
    "Klebsiella_pneumoniae_KPNIH24", "Klebsiella_pneumoniae_KPNIH24", 
    "Enterobacter_cloacae_ECR091", "Enterobacter_cloacae_ECR091", 
    "Enterobacter_cloacae_ECR091", "Serratia_marcescens_CAV1492", 
    "Serratia_marcescens_CAV1492", "Serratia_marcescens_CAV1492", 
    "Serratia_marcescens_CAV1492", "Serratia_marcescens_CAV1492", 
    "Citrobacter_freundii_CAV1741", "Citrobacter_freundii_CAV1741", 
    "Citrobacter_freundii_CAV1741", "Citrobacter_freundii_CAV1741", 
    "Citrobacter_freundii_CAV1741", "Citrobacter_freundii_CAV1741", 
    "Klebsiella_pneumoniae_KPNIH1", "Klebsiella_pneumoniae_KPNIH1", 
    "Klebsiella_pneumoniae_KPNIH1", "Klebsiella_pneumoniae_KPNIH10", 
    "Klebsiella_pneumoniae_KPNIH10", "Klebsiella_pneumoniae_KPNIH10", 
    "Klebsiella_pneumoniae_KPN555", "Klebsiella_pneumoniae_KPN555", 
    "Klebsiella_pneumoniae_KPN555", "Klebsiella_oxytoca_CAV1099", 
    "Klebsiella_oxytoca_CAV1099", "Klebsiella_oxytoca_CAV1099", 
    "Klebsiella_oxytoca_CAV1099", "Klebsiella_oxytoca_CAV1099", 
    "Enterobacter_cloacae_ECNIH3", "Enterobacter_cloacae_ECNIH3", 
    "Enterobacter_cloacae_ECNIH3", "Enterobacter_cloacae_ECNIH3", 
    "Klebsiella_pneumoniae_KPNIH39", "Klebsiella_pneumoniae_KPNIH39", 
    "Klebsiella_pneumoniae_KPNIH39", "Klebsiella_oxytoca_CAV1335", 
    "Klebsiella_oxytoca_CAV1335", "Klebsiella_oxytoca_CAV1335", 
    "Klebsiella_oxytoca_CAV1335", "Klebsiella_oxytoca_CAV1335", 
    "Rhodobacter_sphaeroides_2-4-1", "Rhodobacter_sphaeroides_2-4-1", 
    "Rhodobacter_sphaeroides_2-4-1", "Rhodobacter_sphaeroides_2-4-1", 
    "Rhodobacter_sphaeroides_2-4-1", "Citrobacter_freundii_CAV1321", 
    "Citrobacter_freundii_CAV1321", "Citrobacter_freundii_CAV1321", 
    "Citrobacter_freundii_CAV1321", "Citrobacter_freundii_CAV1321", 
    "Citrobacter_freundii_CAV1321", "Citrobacter_freundii_CAV1321", 
    "Citrobacter_freundii_CAV1321", "Citrobacter_freundii_CAV1321", 
    "Klebsiella_oxytoca_KONIH1", "Klebsiella_oxytoca_KONIH1", 
    "Klebsiella_oxytoca_KONIH1", "Klebsiella_pneumoniae_CAV1344", 
    "Klebsiella_pneumoniae_CAV1344", "Klebsiella_pneumoniae_CAV1344", 
    "Klebsiella_pneumoniae_CAV1344", "Klebsiella_pneumoniae_CAV1344", 
    "Klebsiella_pneumoniae_CAV1193", "Klebsiella_pneumoniae_CAV1193", 
    "Klebsiella_pneumoniae_CAV1193", "Klebsiella_pneumoniae_CAV1193", 
    "Klebsiella_pneumoniae_CAV1193", "Kluyvera_intermedia_CAV1151", 
    "Kluyvera_intermedia_CAV1151", "Kluyvera_intermedia_CAV1151", 
    "Kluyvera_intermedia_CAV1151", "Enterobacter_cloacae_ECNIH2", 
    "Enterobacter_cloacae_ECNIH2", "Enterobacter_cloacae_ECNIH2", 
    "Klebsiella_pneumoniae_PMK1", "Klebsiella_pneumoniae_PMK1", 
    "Klebsiella_pneumoniae_PMK1", "Klebsiella_pneumoniae_PMK1", 
    "Klebsiella_pneumoniae_KPNIH27", "Klebsiella_pneumoniae_KPNIH27", 
    "Klebsiella_pneumoniae_KPNIH27", "Klebsiella_pneumoniae_KPNIH27", 
    "Klebsiella_pneumoniae_KPNIH27", "Klebsiella_oxytoca_CAV1374", 
    "Klebsiella_oxytoca_CAV1374", "Klebsiella_oxytoca_CAV1374", 
    "Klebsiella_oxytoca_CAV1374", "Klebsiella_oxytoca_CAV1374", 
    "Klebsiella_oxytoca_CAV1374", "Klebsiella_oxytoca_CAV1374", 
    "Klebsiella_oxytoca_CAV1374", "Klebsiella_oxytoca_CAV1374", 
    "Klebsiella_oxytoca_CAV1374", "Klebsiella_oxytoca_CAV1374"))
```

We divide the plasmid size by 1000 to obtain kbp and we extract the genus of each bacterial isolate.

``` r
plasmid_sizes = plasmid_sizes/1000
genus_strains = sapply(strsplit(as.character(bacterial_isolates), 
    "_"), head, 1)
bacterial_isolates <- factor(bacterial_isolates, levels = number_order_reference$Strain)
df_genus = data.frame(bacterial_isolates, genus_strains, plasmid_sizes)
Genus = factor(df_genus$genus_strains)
```

We can plot finally our representation of our test data. The strains are ordered based on total plasmid length and coloured by genus.

``` r
q <- qplot(bacterial_isolates, plasmid_sizes)
q + theme_bw() + theme(axis.text.x = element_text(angle = 90, 
    hjust = 1)) + labs(x = "", y = "Plasmid length (kbp)") + 
    geom_point() + theme(panel.grid.minor = element_line(colour = "grey", 
    size = 0.1), panel.grid.major = element_line(colour = "grey", 
    size = 0.1)) + scale_y_continuous(breaks = c(0, 50, 100, 
    150, 200, 250, 300, 350, 400))
```

![](detailed_analysis_files/figure-markdown_github/unnamed-chunk-6-1.png)

Reconstruction per plasmid
--------------------------

This analysis evaluated the performance of the programs on a plasmid level. A recall value higher than 90% was considered as cut-off to define if a plasmid was predicted or no by the programs.

First we need to load the file with the recall value per plasmid, in some cases we did not get any recall value (e.g. PlasmidFinder in Gram positive isolates). For this reason, we replace NAs by 0s values.

``` r
setwd("/home/arredondo/Data/All_Genomes/")
benchmarktablefile <- "Benchmark_perPlasmid.csv"
benchmarktable = read.table(benchmarktablefile, sep = ":", stringsAsFactors = FALSE, 
    header = TRUE)
benchmarktable[is.na(benchmarktable)] <- 0
summary(benchmarktable)
```

    ##      Name                Size        PlasmidFinder         cBAR       
    ##  Length:148         Min.   :  1552   Min.   :  0.00   Min.   :  0.00  
    ##  Class :character   1st Qu.: 33610   1st Qu.:  0.00   1st Qu.: 78.54  
    ##  Mode  :character   Median : 79110   Median : 16.73   Median : 92.62  
    ##                     Mean   : 96310   Mean   : 32.02   Mean   : 85.51  
    ##                     3rd Qu.:129548   3rd Qu.: 62.18   3rd Qu.: 99.33  
    ##                     Max.   :338850   Max.   :100.00   Max.   :100.00  
    ##     Recycler      PlasmidSPAdes   
    ##  Min.   :  0.00   Min.   :  0.00  
    ##  1st Qu.:  0.00   1st Qu.: 94.63  
    ##  Median :  0.00   Median : 98.62  
    ##  Mean   : 15.61   Mean   : 87.21  
    ##  3rd Qu.:  0.00   3rd Qu.:100.00  
    ##  Max.   :100.00   Max.   :100.00

``` r
benchmarktable$Size <- benchmarktable$Size/1000
size_factor <- rep("Non-determined", nrow(benchmarktable))
benchmarktable <- cbind(benchmarktable, size_factor)
benchmarktable$size_factor <- ifelse(benchmarktable$Size < 10, 
    "Small", ifelse(benchmarktable$Size > 10 & benchmarktable$Size < 
        50, "Medium", "Large"))
benchmarktable$size_factor <- factor(benchmarktable$size_factor, 
    levels = c("Small", "Medium", "Large"))
benchmarktable$PlasmidSPAdes <- benchmarktable$PlasmidSPAdes/100
benchmarktable$Recycler <- benchmarktable$Recycler/100
benchmarktable$cBAR <- benchmarktable$cBAR/100
benchmarktable$PlasmidFinder <- benchmarktable$PlasmidFinder/100
```

``` r
colors = c("#ba495b", "#56ae6c", "#8960b3", "#b0923b")
pspades <- ggplot(benchmarktable, aes(factor(size_factor), benchmarktable$PlasmidSPAdes))
pspades <- pspades + geom_violin(scale = "width", colour = colors[1]) + 
    geom_jitter(height = 0, width = 0.1, size = 0.5) + labs(x = "", 
    y = "Recall", title = "a. PlasmidSPAdes") + theme(panel.grid.minor = element_line(colour = "grey", 
    size = 0.1), panel.grid.major = element_line(colour = "grey", 
    size = 0.25)) + theme_bw() + geom_hline(yintercept = 0.9, 
    linetype = "dashed", color = "black")
cbar <- ggplot(benchmarktable, aes(factor(size_factor), benchmarktable$cBAR))
cbar <- cbar + geom_violin(scale = "width", colour = colors[3]) + 
    geom_jitter(height = 0, width = 0.1, size = 0.5) + labs(x = "", 
    y = "Recall", title = "c. cBAR") + theme(panel.grid.minor = element_line(colour = "grey", 
    size = 0.1), panel.grid.major = element_line(colour = "grey", 
    size = 0.25)) + theme_bw() + geom_hline(yintercept = 0.9, 
    linetype = "dashed", color = "black")
recycler <- ggplot(benchmarktable, aes(factor(size_factor), benchmarktable$Recycler))
recycler <- recycler + geom_violin(scale = "width", colour = colors[2]) + 
    geom_jitter(height = 0, width = 0.1, size = 0.5) + labs(x = "", 
    y = "Recall", title = "b. Recycler") + theme(panel.grid.minor = element_line(colour = "grey", 
    size = 0.1), panel.grid.major = element_line(colour = "grey", 
    size = 0.25)) + theme_bw() + geom_hline(yintercept = 0.9, 
    linetype = "dashed", color = "black")
plasmidfinder <- ggplot(benchmarktable, aes(factor(size_factor), 
    benchmarktable$PlasmidFinder))
plasmidfinder <- plasmidfinder + geom_violin(scale = "width", 
    colour = colors[4]) + geom_jitter(height = 0, width = 0.1, 
    size = 0.5) + labs(x = "", y = "Recall", title = "d. PlasmidFinder") + 
    theme(panel.grid.minor = element_line(colour = "grey", size = 0.1), 
        panel.grid.major = element_line(colour = "grey", size = 0.25)) + 
    theme_bw() + geom_hline(yintercept = 0.9, linetype = "dashed", 
    color = "black")
multiplot <- function(..., plotlist = NULL, file, cols = 1, layout = NULL) {
    require(grid)
    # Make a list from the ... arguments and plotlist
    plots <- c(list(...), plotlist)
    numPlots = length(plots)
    # If layout is NULL, then use 'cols' to determine layout
    if (is.null(layout)) {
        # Make the panel ncol: Number of columns of plots nrow:
        # Number of rows needed, calculated from # of cols
        layout <- matrix(seq(1, cols * ceiling(numPlots/cols)), 
            ncol = cols, nrow = ceiling(numPlots/cols))
    }
    if (numPlots == 1) {
        print(plots[[1]])
    } else {
        # Set up the page
        grid.newpage()
        pushViewport(viewport(layout = grid.layout(nrow(layout), 
            ncol(layout))))
        # Make each plot, in the correct location
        for (i in 1:numPlots) {
            # Get the i,j matrix positions of the regions that contain
            # this subplot
            matchidx <- as.data.frame(which(layout == i, arr.ind = TRUE))
            print(plots[[i]], vp = viewport(layout.pos.row = matchidx$row, 
                layout.pos.col = matchidx$col))
        }
    }
}
multiplot(pspades, recycler, cbar, plasmidfinder, cols = 2)
```

![](detailed_analysis_files/figure-markdown_github/unnamed-chunk-8-1.png)

We filter plasmids with a recall value higher than 90% (upper part of Figure 1). We can overview the programs performance using a Venn Diagramm.

``` r
draw.quad.venn(area1 = nrow(subset(benchmarktable, benchmarktable$PlasmidSPAdes > 
    0.9)), area2 = nrow(subset(benchmarktable, benchmarktable$Recycler > 
    0.9)), area3 = nrow(subset(benchmarktable, benchmarktable$cBAR > 
    0.9)), area4 = nrow(subset(benchmarktable, benchmarktable$PlasmidFinder > 
    0.9)), n12 = nrow(subset(benchmarktable, benchmarktable$PlasmidSPAdes > 
    0.9 & benchmarktable$Recycler > 0.9)), n13 = nrow(subset(benchmarktable, 
    benchmarktable$PlasmidSPAdes > 0.9 & benchmarktable$cBAR > 
        0.9)), n14 = nrow(subset(benchmarktable, benchmarktable$PlasmidSPAdes > 
    0.9 & benchmarktable$PlasmidFinder > 0.9)), n23 = nrow(subset(benchmarktable, 
    benchmarktable$Recycler > 0.9 & benchmarktable$cBAR > 0.9)), 
    n24 = nrow(subset(benchmarktable, benchmarktable$Recycler > 
        0.9 & benchmarktable$PlasmidFinder > 0.9)), n34 = nrow(subset(benchmarktable, 
        benchmarktable$cBAR > 0.9 & benchmarktable$PlasmidFinder > 
            0.9)), n123 = nrow(subset(benchmarktable, benchmarktable$PlasmidSPAdes > 
        0.9 & benchmarktable$Recycler > 0.9 & benchmarktable$cBAR > 
        0.9)), n124 = nrow(subset(benchmarktable, benchmarktable$PlasmidSPAdes > 
        0.9 & benchmarktable$Recycler > 0.9 & benchmarktable$PlasmidFinder > 
        0.9)), n134 = nrow(subset(benchmarktable, benchmarktable$PlasmidSPAdes > 
        0.9 & benchmarktable$cBAR > 0.9 & benchmarktable$PlasmidFinder > 
        0.9)), n234 = nrow(subset(benchmarktable, benchmarktable$Recycler > 
        0.9 & benchmarktable$cBAR > 0.9 & benchmarktable$PlasmidFinder > 
        0.9)), n1234 = nrow(subset(benchmarktable, benchmarktable$PlasmidSPAdes > 
        0.9 & benchmarktable$Recycler > 0.9 & benchmarktable$cBAR > 
        0.9 & benchmarktable$PlasmidFinder > 0.9)), category = c("PlasmidSPAdes", 
        "Recycler", "cBAR", "PlasmidFinder"), lty = "blank", 
    fill = colors, cex = 2, cat.cex = 1.5)
```

![](detailed_analysis_files/figure-markdown_github/unnamed-chunk-9-1.png)

    ## (polygon[GRID.polygon.264], polygon[GRID.polygon.265], polygon[GRID.polygon.266], polygon[GRID.polygon.267], polygon[GRID.polygon.268], polygon[GRID.polygon.269], polygon[GRID.polygon.270], polygon[GRID.polygon.271], text[GRID.text.272], text[GRID.text.273], text[GRID.text.274], text[GRID.text.275], text[GRID.text.276], text[GRID.text.277], text[GRID.text.278], text[GRID.text.279], text[GRID.text.280], text[GRID.text.281], text[GRID.text.282], text[GRID.text.283], text[GRID.text.284], text[GRID.text.285], text[GRID.text.286], text[GRID.text.287], text[GRID.text.288], text[GRID.text.289], text[GRID.text.290])

From the Venn Diagram we can extract the 5 plasmids detected by all the programs

``` r
subset(benchmarktable, benchmarktable$PlasmidSPAdes > 0.9 & benchmarktable$Recycler > 
    0.9 & benchmarktable$cBAR > 0.9 & benchmarktable$PlasmidFinder > 
    0.9)
```

    ##                                                                                                          Name
    ## 65  >gi|828905883|gb|CP011575.1| Klebsiella pneumoniae strain CAV1392 plasmid pKPC_CAV1392, complete sequence
    ## 87   >gi|828982776|gb|CP011655.1| Citrobacter freundii strain CAV1741 plasmid pCAV1741-110, complete sequence
    ## 102               >gi|556555082|ref|NC_022649.1| Escherichia coli JJ1886 plasmid pJJ1886_2, complete sequence
    ## 108                >gi|1006701341|gb|CP014318.1| Escherichia coli JJ1887 plasmid pJJ1887-2, complete sequence
    ## 140     >gi|749295932|ref|NZ_CP008931.1| Klebsiella pneumoniae strain PMK1 plasmid pPMK1-B, complete sequence
    ##        Size PlasmidFinder cBAR Recycler PlasmidSPAdes size_factor
    ## 65   43.621             1    1        1             1      Medium
    ## 87  109.688             1    1        1             1       Large
    ## 102   5.167             1    1        1             1       Small
    ## 108   5.167             1    1        1             1       Small
    ## 140 111.693             1    1        1             1       Large

And the plasmids not correctly reconstructed by any of the programs

``` r
subset(benchmarktable, benchmarktable$PlasmidSPAdes < 0.9 & benchmarktable$Recycler < 
    0.9 & benchmarktable$cBAR < 0.9 & benchmarktable$PlasmidFinder < 
    0.9)
```

    ##                                                                                                          Name
    ## 1   >gi|1008423447|gb|CP014450.1| Enterococcus faecium strain ATCC 700221 plasmid unnamed1, complete sequence
    ## 10          >gi|470157235|ref|NC_020553.1| Corynebacterium callunae DSM 20147 plasmid pCC2, complete sequence
    ## 21     >gi|828927436|gb|CP011596.1| Klebsiella oxytoca strain CAV1099 plasmid pCAV1099-114, complete sequence
    ## 38  >gi|828939395|gb|CP011606.1| Citrobacter freundii strain CAV1321 plasmid pCAV1321-4310, complete sequence
    ## 67  >gi|828905996|gb|CP011577.1| Klebsiella pneumoniae strain CAV1392 plasmid pCAV1392-131, complete sequence
    ## 91           >gi|764911390|ref|NZ_CP008825.1| Enterobacter cloacae ECNIH2 plasmid pKPC-272, complete sequence
    ## 94           >gi|749295313|ref|NZ_CP008899.1| Enterobacter cloacae ECNIH3 plasmid pENT-8a4, complete sequence
    ## 100          >gi|749295356|ref|NZ_CP008907.1| Enterobacter cloacae ECR091 plasmid pENT-4bd, complete sequence
    ## 104               >gi|556555179|ref|NC_022651.1| Escherichia coli JJ1886 plasmid pJJ1886_5, complete sequence
    ## 105               >gi|556579581|ref|NC_022661.1| Escherichia coli JJ1886 plasmid pJJ1886_1, complete sequence
    ## 107                >gi|1006701338|gb|CP014317.1| Escherichia coli JJ1887 plasmid pJJ1887-1, complete sequence
    ## 110                >gi|1006701360|gb|CP014320.1| Escherichia coli JJ1887 plasmid pJJ1887-5, complete sequence
    ## 111                >gi|1006701503|gb|CP014321.1| Escherichia coli JJ1887 plasmid pJJ1887-4, complete sequence
    ## 115 >gi|1039487545|ref|NZ_CP015026.1| Klebsiella pneumoniae strain Kpn223 plasmid pKPN-065, complete sequence
    ## 118 >gi|1039504643|ref|NZ_CP015133.1| Klebsiella pneumoniae strain Kpn555 plasmid pKPN-d6b, complete sequence
    ##        Size PlasmidFinder    cBAR Recycler PlasmidSPAdes size_factor
    ## 1   189.452       0.00000 0.62557  0.00000       0.58759       Large
    ## 10   85.023       0.00000 0.05169  0.00000       0.00000       Large
    ## 21  113.992       0.08473 0.77352  0.00000       0.61078       Large
    ## 38    4.310       0.00000 0.89466  0.00000       0.89466       Small
    ## 67  130.719       0.62998 0.87739  0.00000       0.76749       Large
    ## 91  282.439       0.08034 0.74621  0.00000       0.12037       Large
    ## 94  255.013       0.19415 0.66626  0.00000       0.48236       Large
    ## 100 111.227       0.61916 0.66511  0.00000       0.06150       Large
    ## 104 110.040       0.19119 0.73360  0.00000       0.18940       Large
    ## 105   1.552       0.87178 0.87178  0.87178       0.00000       Small
    ## 107   1.552       0.87178 0.87178  0.87178       0.00000       Small
    ## 110 130.603       0.16533 0.72489  0.00000       0.34855       Large
    ## 111 107.507       0.25611 0.60108  0.04523       0.18287       Large
    ## 115 170.926       0.09634 0.81844  0.00000       0.12305       Large
    ## 118  26.450       0.00000 0.00000  0.00000       0.69365      Medium

Additionally, we can draw a barplot showing all the plasmids reconstructed by the programs.

``` r
setwd("/home/arredondo/Data/All_Genomes/")
benchmarktablefile <- "Benchmark_perPlasmid.csv"
benchmarktable = read.table(benchmarktablefile, sep = ":", stringsAsFactors = FALSE, 
    header = TRUE)
benchmarktable[is.na(benchmarktable)] <- 0
benchmarktable$PlasmidSPAdes <- benchmarktable$PlasmidSPAdes/100
benchmarktable$Recycler <- benchmarktable$Recycler/100
benchmarktable$cBAR <- benchmarktable$cBAR/100
benchmarktable$PlasmidFinder <- benchmarktable$PlasmidFinder/100
coverage = 0.9
min_size = 10000
med_size = 50000
## PlasmidSPAdes
subset_area1 = subset(benchmarktable, benchmarktable$PlasmidSPAdes > 
    coverage)
area1_1 = nrow(subset(subset_area1, subset_area1$Size < min_size))
area1_2 = nrow(subset(subset_area1, subset_area1$Size > min_size & 
    subset_area1$Size < med_size))
area1_3 = nrow(subset(subset_area1, subset_area1$Size > med_size))
## Recycler
subset_area2 = subset(benchmarktable, benchmarktable$Recycler > 
    coverage)
area2_1 = nrow(subset(subset_area2, subset_area2$Size < min_size))
area2_2 = nrow(subset(subset_area2, subset_area2$Size > min_size & 
    subset_area2$Size < med_size))
area2_3 = nrow(subset(subset_area2, subset_area2$Size > med_size))
## cBAR
subset_area3 = subset(benchmarktable, benchmarktable$cBAR > coverage)
area3_1 = nrow(subset(subset_area3, subset_area3$Size < min_size))
area3_2 = nrow(subset(subset_area3, subset_area3$Size > min_size & 
    subset_area3$Size < med_size))
area3_3 = nrow(subset(subset_area3, subset_area3$Size > med_size))
## PlasmidFinder
subset_area4 = subset(benchmarktable, benchmarktable$PlasmidFinder > 
    coverage)
area4_1 = nrow(subset(subset_area4, subset_area4$Size < min_size))
area4_2 = nrow(subset(subset_area4, subset_area4$Size > min_size & 
    subset_area4$Size < med_size))
area4_3 = nrow(subset(subset_area4, subset_area4$Size > med_size))
## Reference plasmids
arean_1 = nrow(subset(benchmarktable, benchmarktable$Size < min_size))
arean_2 = nrow(subset(benchmarktable, benchmarktable$Size > min_size & 
    benchmarktable$Size < med_size))
arean_3 = nrow(subset(benchmarktable, benchmarktable$Size > med_size))
distribution_plasmids <- structure(list(PlasmidSPAdes = c(area1_1, 
    area1_2, area1_3), Recycler = c(area2_1, area2_2, area2_3), 
    cBAR = c(area3_1, area3_2, area3_3), PlasmidFinder = c(area4_1, 
        area4_2, area4_3)), .Names = c("PlasmidSPAdes", "Recycler", 
    "cBAR", "PlasmidFinder"), class = "data.frame", row.names = c(NA, 
    -3L))
distribution_plasmids_ref <- structure(list(small_plasmids = c(arean_1, 
    area1_1, area2_1, area3_1, area4_1), medium_plasmids = c(arean_2, 
    area1_2, area2_2, area3_2, area4_2), large_plasmids = c(arean_3, 
    area1_3, area2_3, area3_3, area4_3)), .Names = c("Small plasmids (< 10 kbp)", 
    "Medium Plasmids (10-50 kbp)", "Large Plasmids (> 50 kbp)"), 
    class = "data.frame", row.names = c(NA, -5L))
rownames(distribution_plasmids_ref) = c("Reference Plasmids", 
    "PlasmidSPAdes", "Recycler", "cBAR", "PlasmidFinder")
par(mfrow = c(1, 1), mar = c(5, 5, 4, 10))
barplot(as.matrix(distribution_plasmids_ref), main = "", ylab = "Number of Plasmids (Recall > 0.9)", 
    cex.lab = 1, cex.main = 1.4, beside = TRUE, col = c("white", 
        colors), legend = rownames(distribution_plasmids), ylim = c(0, 
        100), legend.text = c("Reference Plasmids", "PlasmidSPAdes", 
        "Recycler", "cBAR", "PlasmidFinder"), args.legend = list(x = "topright", 
        bty = "n", inset = c(-0.45, 0)))
```

    ## Warning in plot.window(xlim, ylim, log = log, ...): "legend" is not a
    ## graphical parameter

    ## Warning in axis(if (horiz) 2 else 1, at = at.l, labels = names.arg, lty =
    ## axis.lty, : "legend" is not a graphical parameter

    ## Warning in title(main = main, sub = sub, xlab = xlab, ylab = ylab, ...):
    ## "legend" is not a graphical parameter

    ## Warning in axis(if (horiz) 1 else 2, cex.axis = cex.axis, ...): "legend" is
    ## not a graphical parameter

![](detailed_analysis_files/figure-markdown_github/unnamed-chunk-12-1.png)

Additional analysis (Supplementary Data)
========================================

PlasmidSPAdes structural report
-------------------------------

``` r
# Total genomes
total_strains <- as.character(unique(bacterial_isolates))
# Genomes with more than 1 plasmid per genome project.
genomes_more_1_plasmid <- bacterial_isolates[duplicated(bacterial_isolates)]
genomes_more_1_plasmid <- as.character(unique(genomes_more_1_plasmid))
## The following 6 genomes (it also includes in the first
## element the negative control 'B. cenocepacia',
## plasmid-free) can't be considered because they only have 1
## plasmid. So we can't evaluate on them PlasmidSPAdes problem
## merging plasmids into different components.
genomes_only_1_plasmid <- total_strains[!total_strains %in% genomes_more_1_plasmid]
# We remove from the benchmarking table the plasmids
# associated from the genome projects present in
# 'genomes_only_1_plasmid'. We have created a new file only
# containing the recall values of genome projects with more
# than 1 plasmid.
benchmark_projects_more_1_plasmids <- read.csv(file = "/home/arredondo/Data/All_Genomes/genome_project_more_1_plasmid.csv", 
    sep = "\t")
# pspades_well_predicted <-
# subset(benchmark_projects_more_1_plasmids,
# benchmark_projects_more_1_plasmids$PlasmidSPAdes >
# coverage)
pspades_well_predicted <- benchmark_projects_more_1_plasmids
pspades_well_predicted <- pspades_well_predicted[, c(1, 2, 6)]
## We use the gi accession to match rows from two dataframes
## ('pspades_well_predicted' and 'data_pspades')
data_pspades <- data_pspades[!duplicated(data_pspades), ]
data_pspades <- data_pspades[order(data_pspades$Node), ]
data_pspades_ref_plasmids <- subset(data_pspades, data_pspades$Type == 
    "Reference_plasmid")
gi_accession_v1 <- (sapply(strsplit(as.character(data_pspades_ref_plasmids$Reference_genome), 
    "_"), "[[", 2))
data_pspades_ref_plasmids <- cbind(data_pspades_ref_plasmids, 
    gi_accession_v1)
bin_component <- (sapply(strsplit(as.character(data_pspades_ref_plasmids$Node), 
    "_"), "[[", 8))
data_pspades_ref_plasmids <- cbind(data_pspades_ref_plasmids, 
    bin_component)
gi_accession_v2 <- (sapply(strsplit(as.character(pspades_well_predicted$Name), 
    "\\|"), "[[", 2))
pspades_well_predicted <- cbind(pspades_well_predicted, gi_accession_v2)
data_pspades_ref_plasmids_cov_0.9 <- data_pspades_ref_plasmids[data_pspades_ref_plasmids$gi_accession_v1 %in% 
    pspades_well_predicted$gi_accession_v2, ]
accession_plasmids <- as.character(unique(data_pspades_ref_plasmids_cov_0.9$gi_accession_v1))
data_pspades_ref_plasmids_cov_0.9_ECNIH2 <- subset(data_pspades_ref_plasmids_cov_0.9, 
    data_pspades_ref_plasmids_cov_0.9$Strain == "Enterobacter_cloacae_ECNIH2")
total_components <- NULL
for (z in unique(data_pspades_ref_plasmids_cov_0.9$Strain)) {
    data_pspades_ref_plasmids_cov_0.9_strain <- subset(data_pspades_ref_plasmids_cov_0.9, 
        data_pspades_ref_plasmids_cov_0.9$Strain == z)
    print(z)
    for (i in unique(data_pspades_ref_plasmids_cov_0.9_strain$gi_accession_v1)) {
        data_components <- data_pspades_ref_plasmids_cov_0.9_strain[data_pspades_ref_plasmids_cov_0.9_strain$gi_accession_v1 == 
            i, ]
        components <- as.character(unique(data_components$bin_component))
        vector_print <- c("Plasmid with accession", i, "is included in the component(s)", 
            components)
    }
}
```

    ## [1] "Klebsiella_pneumoniae_KPNIH10"
    ## [1] "Klebsiella_pneumoniae_KPNIH27"
    ## [1] "Escherichia_coli_JJ1887"
    ## [1] "Enterococcus_faecium_700221"
    ## [1] "Klebsiella_pneumoniae_CAV1596"
    ## [1] "Klebsiella_pneumoniae_KPNIH1"
    ## [1] "Escherichia_coli_EC0889"
    ## [1] "Klebsiella_pneumoniae_KPNIH39"
    ## [1] "Kluyvera_intermedia_CAV1151"
    ## [1] "Rhodobacter_sphaeroides_2-4-1"
    ## [1] "Klebsiella_pneumoniae_PMK1"
    ## [1] "Klebsiella_oxytoca_KONIH1"
    ## [1] "Enterobacter_cloacae_CAV1669"
    ## [1] "Enterobacter_cloacae_CAV1668"
    ## [1] "Klebsiella_oxytoca_CAV1374"
    ## [1] "Klebsiella_pneumoniae_CAV1193"
    ## [1] "Citrobacter_freundii_CAV1741"
    ## [1] "Klebsiella_oxytoca_CAV1099"
    ## [1] "Klebsiella_pneumoniae_CAV1392"
    ## [1] "Enterobacter_cloacae_CAV1311"
    ## [1] "Klebsiella_pneumoniae_KPNIH36"
    ## [1] "Klebsiella_pneumoniae_KPN555"
    ## [1] "Serratia_marcescens_CAV1492"
    ## [1] "Klebsiella_pneumoniae_KPNIH24"
    ## [1] "Klebsiella_oxytoca_CAV1335"
    ## [1] "Citrobacter_freundii_CAV1321"
    ## [1] "Enterobacter_cloacae_ECNIH2"
    ## [1] "Enterobacter_asburiae_CAV1043"
    ## [1] "Klebsiella_pneumoniae_CAV1344"
    ## [1] "Enterobacter_cloacae_CAV1411"
    ## [1] "Escherichia_coli_JJ1886"
    ## [1] "Enterobacter_cloacae_ECNIH3"
    ## [1] "Enterobacter_cloacae_ECR091"
    ## [1] "Klebsiella_pneumoniae_AATZP"
    ## [1] "Corynebacterium_callunae_DSM_20147"

``` r
write.csv(pspades_well_predicted, file = "/home/arredondo/Data/All_Genomes/pspades_well_predicted_plasmids.csv", 
    row.names = FALSE)
```

``` r
component_information_pspades <- read.csv("/home/arredondo/Data/All_Genomes/pspades_well_predicted_plasmids_components_after_revision.csv")
summary(component_information_pspades$Component_Information)
```

    ## Independent      Merged 
    ##          19         101

``` r
component_information_pspades$Size <- component_information_pspades$Size/1000
name_complete_strain <- (sapply(strsplit(as.character(component_information_pspades$Name), 
    "\\|"), "[[", 5))
component_information_pspades <- cbind(component_information_pspades, 
    name_complete_strain)
name_strain <- (sapply(strsplit(as.character(component_information_pspades$name_complete_strain), 
    "plasmid"), "[[", 1))
frequency_strains <- table(name_strain)
Strain <- factor(name_strain, levels = names(frequency_strains[order(frequency_strains, 
    decreasing = FALSE)]))
component_information_pspades <- cbind(component_information_pspades, 
    Strain)
correct_structure <- subset(component_information_pspades, component_information_pspades$Component_Information == 
    "Independent")
mean(correct_structure$Size)
```

    ## [1] 34.42089

``` r
pspades_components <- ggplot(component_information_pspades, aes(x = factor(Strain), 
    y = Size, group = Component_Information))
pspades_components + theme_bw() + theme(axis.text.x = element_text(angle = 90, 
    hjust = 1)) + labs(x = "", y = "Plasmid length (kbp)", colour = "", 
    title = "PlasmidSPAdes structural report") + geom_point(aes(color = component_information_pspades$Component_Information, 
    shape = component_information_pspades$Component_Information), 
    show.legend = TRUE, size = 2) + theme(panel.grid.minor = element_line(colour = "grey", 
    size = 0.05), panel.grid.major = element_line(colour = "grey", 
    size = 0.05)) + scale_color_manual(name = "", values = c("darkolivegreen3", 
    "firebrick"), labels = c("Independent", "Merged")) + scale_shape_manual(name = "", 
    values = c(19, 4), labels = c("Independent", "Merged"))
```

![](detailed_analysis_files/figure-markdown_github/unnamed-chunk-15-1.png)

Recycler chromosome fraction analysis
-------------------------------------

Phages Recycler

``` r
phages_recycler <- read.csv("/home/arredondo/Data/All_Genomes/phages_recycler", 
    sep = "\t", header = FALSE)
colnames(phages_recycler) <- c("Strain", "Length", "Type", "Node", 
    "Reference_genome")
## Phaster results
phages_phast_recycler <- read.csv("/home/arredondo/Data/All_Genomes/phages_recycler_phast", 
    sep = "\t", header = FALSE)
colnames(phages_phast_recycler) <- c("Strain", "Length", "Type", 
    "Node", "Reference_genome", "Phaster_results")
validation_phages_phast_recycler <- subset(phages_phast_recycler, 
    phages_phast_recycler$Phaster_results == "Confirmed")
## 
data_recycler = data_recycler[c("Strain", "Length", "Type", "Node")]
data_recycler <- data_recycler[order(data_recycler$Strain), ]
data_recycler <- data_recycler[order(data_recycler$Type, decreasing = TRUE), 
    ]
head(data_recycler)
```

    ##                          Strain Length              Type
    ## 7     Bacillus_subtilis_BEST195   5820 Reference_plasmid
    ## 16 Citrobacter_freundii_CAV1321   1303 Reference_plasmid
    ## 20 Citrobacter_freundii_CAV1741   1303 Reference_plasmid
    ## 21 Citrobacter_freundii_CAV1741 109688 Reference_plasmid
    ## 22 Citrobacter_freundii_CAV1741   1916 Reference_plasmid
    ## 23 Citrobacter_freundii_CAV1741   3223 Reference_plasmid
    ##                                  Node
    ## 7  RNODE_2_length_5892_cov_4530.17000
    ## 16  RNODE_2_length_1347_cov_102.55832
    ## 20  RNODE_5_length_1347_cov_101.13792
    ## 21 RNODE_1_length_109710_cov_49.61400
    ## 22  RNODE_2_length_1938_cov_723.33500
    ## 23  RNODE_3_length_3245_cov_642.14700

``` r
data_not_duplicates_recycler <- data_recycler[!duplicated(data_recycler$Node), 
    ]
data_not_duplicates_recycler <- data_recycler[order(data_recycler$Strain), 
    ]
data_not_duplicates_recycler <- data_not_duplicates_recycler[order(data_not_duplicates_recycler$Strain), 
    ]
false_pos_chromosome_recycler <- subset(data_not_duplicates_recycler, 
    data_not_duplicates_recycler$Type == "Chromosome")
false_pos_chromosome_recycler$Length <- false_pos_chromosome_recycler$Length/1000
phage_association <- rep("Non-determined", nrow(false_pos_chromosome_recycler))
phaster_result <- rep("No phaster result", nrow(false_pos_chromosome_recycler))
false_pos_chromosome_recycler <- cbind(false_pos_chromosome_recycler, 
    phage_association)
false_pos_chromosome_recycler <- cbind(false_pos_chromosome_recycler, 
    phaster_result)
false_pos_chromosome_recycler$phage_association <- ifelse(false_pos_chromosome_recycler$Node %in% 
    phages_recycler$Node, "Phage-associated", "No phage-associated")
false_pos_chromosome_recycler$phaster_result <- ifelse(false_pos_chromosome_recycler$Node %in% 
    validation_phages_phast_recycler$Node, "Phaster_confirmation", 
    "Unconfirmed")
```

``` r
fp_chromosome_recycler <- ggplot(false_pos_chromosome_recycler, 
    aes(x = factor(Strain), y = Length, group = interaction(phaster_result, 
        phage_association)))
fp_chromosome_recycler + theme_classic() + theme(axis.text.x = element_text(angle = 90, 
    hjust = 1)) + labs(x = "", y = "Contig length (kbp)", title = "Recycler chromosome fraction analysis") + 
    geom_point(aes(shape = phage_association, color = phaster_result), 
        show.legend = TRUE, size = 1.75) + theme(panel.grid.minor = element_line(colour = "grey", 
    size = 0.05), panel.grid.major = element_line(colour = "grey", 
    size = 0.05)) + scale_shape_manual(name = "Annotation", values = c(17, 
    19), labels = c("Others", "Potential phage-associated")) + 
    scale_color_manual(name = "Phaster identification", values = c("palegreen4", 
        "grey"), labels = c("(Pro)phage identified", "No (pro)phage identified")) + 
    guides(colour = guide_legend(order = 2), shape = guide_legend(order = 1))
```

![](detailed_analysis_files/figure-markdown_github/unnamed-chunk-16-1.png)

Reconstruction per genome
=========================

PlasmidSPAdes
-------------

The analysis given by Quast and parse in "parsing\_results.py" is available in the files with the extension "all\*.R".

We check if there are rows identicals and proceed with the analysis.

``` r
data_pspades = data_pspades[!duplicated(data_pspades), ]
data_pspades <- data_pspades[order(data_pspades$Node), ]
```

### Translocations between chromosome and plasmids

We have decided to filter contigs with &gt;500 bp and &gt; 95% identity alignment, mapping to the chromosome and to a reference plasmid. This case is diferent from the one presented above, where the whole contig mapped to the chromosome or to a reference plasmid. In this case a fragment of the contig is mapping to the chromosome and the other to a plasmid. Using Quast and Icarus we have identified which cases for PlasmidSPAdes this has occured (very unfrequent events)

``` r
translocation_pspades <- read.csv("/home/arredondo/Data/All_Genomes/translocation_events_pspades", 
    sep = "\t")
table(translocation_pspades$Strain)
```

    ## 
    ##  Citrobacter_freundii_CAV1321 Enterobacter_asburiae_CAV1043 
    ##                             1                             2 
    ##  Enterobacter_cloacae_CAV1411    Klebsiella_oxytoca_CAV1099 
    ##                             1                             1 
    ## Klebsiella_pneumoniae_CAV1344  Klebsiella_pneumoniae_KPN223 
    ##                             1                             1 
    ##  Klebsiella_pneumoniae_KPNIH1 Klebsiella_pneumoniae_KPNIH10 
    ##                             2                             1 
    ## Klebsiella_pneumoniae_KPNIH24 Klebsiella_pneumoniae_KPNIH27 
    ##                             2                             1 
    ## Klebsiella_pneumoniae_KPNIH36 Klebsiella_pneumoniae_KPNIH39 
    ##                             3                             1

We can see it is a rare event and the number of contigs per genome project with that kind of contigs.

``` r
data_pspades <- data_pspades[!(data_pspades$Node %in% translocation_pspades$Node), 
    ]
```

#### Repeated sequences mapping to different physical DNA units

Plasmids can contain repeat sequences and these sequences can be located in different physical DNA units of the same bacterial isolate. Consequently we can have different situations:

1.  Contig mapping to different plasmids.
2.  Contig mapping to plasmid and chromosome.

We are going to visualize the case of the genome project "Serratia\_marcescens\_CAV1492" to see which contigs are mapping in different locations.

``` r
data_pspades_CAV1492 <- subset(data_pspades, data_pspades$Strain == 
    "Serratia_marcescens_CAV1492")
repeat_df <- data_pspades_CAV1492[duplicated(data_pspades_CAV1492$Node), 
    ]
repeat_df[c("Node")]
```

    ##                                             Node
    ## 2092 NODE_12_length_4304_cov_102.448_component_0
    ## 2080 NODE_15_length_3664_cov_552.992_component_0
    ## 2100 NODE_15_length_3664_cov_552.992_component_0
    ## 2091 NODE_18_length_2766_cov_97.9472_component_0
    ## 2083  NODE_23_length_1370_cov_104.36_component_0
    ## 2082 NODE_27_length_1057_cov_422.241_component_0
    ## 2102 NODE_27_length_1057_cov_422.241_component_0
    ## 2095   NODE_28_length_769_cov_206.65_component_0
    ## 2067                                Quast_report

We can see for example that the "NODE\_27\_length\_1057\_cov\_422.241\_component\_0" is mapping to the chromosome and two reference plasmids. Meanwhile, the "NODE\_15\_length\_3664\_cov\_552.992\_component\_0" is present in three different reference plasmids.

``` r
data_pspades_CAV1492[data_pspades_CAV1492$Node == "NODE_27_length_1057_cov_422.241_component_0", 
    ]
```

    ##                           Strain Length              Type
    ## 2072 Serratia_marcescens_CAV1492   1057 Reference_plasmid
    ## 2082 Serratia_marcescens_CAV1492   1057 Reference_plasmid
    ## 2102 Serratia_marcescens_CAV1492   1057        Chromosome
    ##                                             Node
    ## 2072 NODE_27_length_1057_cov_422.241_component_0
    ## 2082 NODE_27_length_1057_cov_422.241_component_0
    ## 2102 NODE_27_length_1057_cov_422.241_component_0
    ##                                                                                            Reference_genome
    ## 2072  gi_828965798_gb_CP011640.1__Serratia_marcescens_strain_CAV1492_plasmid_pCAV1492-73__complete_sequence
    ## 2082 gi_828965887_gb_CP011641.1__Serratia_marcescens_strain_CAV1492_plasmid_pCAV1492-199__complete_sequence
    ## 2102                        gi_828966150_gb_CP011642.1__Serratia_marcescens_strain_CAV1492__complete_genome

``` r
data_pspades_CAV1492[data_pspades_CAV1492$Node == "NODE_15_length_3664_cov_552.992_component_0", 
    ]
```

    ##                           Strain Length              Type
    ## 2069 Serratia_marcescens_CAV1492   3664 Reference_plasmid
    ## 2080 Serratia_marcescens_CAV1492   3664 Reference_plasmid
    ## 2100 Serratia_marcescens_CAV1492   3664 Reference_plasmid
    ##                                             Node
    ## 2069 NODE_15_length_3664_cov_552.992_component_0
    ## 2080 NODE_15_length_3664_cov_552.992_component_0
    ## 2100 NODE_15_length_3664_cov_552.992_component_0
    ##                                                                                             Reference_genome
    ## 2069   gi_828965798_gb_CP011640.1__Serratia_marcescens_strain_CAV1492_plasmid_pCAV1492-73__complete_sequence
    ## 2080  gi_828965887_gb_CP011641.1__Serratia_marcescens_strain_CAV1492_plasmid_pCAV1492-199__complete_sequence
    ## 2100 gi_828965723_gb_CP011638.1__Serratia_marcescens_strain_CAV1492_plasmid_pCAV1492-6393__complete_sequence

Another example with the genome project "Klebsiella\_pneumoniae\_PMK1"

``` r
data_pspades_PMK1 <- subset(data_pspades, data_pspades$Strain == 
    "Klebsiella_pneumoniae_PMK1")
repeat_df <- data_pspades_PMK1[duplicated(data_pspades_PMK1$Node), 
    ]
repeat_df[c("Node")]
```

    ##                                             Node
    ## 1480 NODE_37_length_2317_cov_50.7661_component_0
    ## 1432 NODE_40_length_2197_cov_67.8796_component_0
    ## 1430 NODE_41_length_1960_cov_81.7003_component_0
    ## 1459 NODE_42_length_1892_cov_104.653_component_0
    ## 1486 NODE_42_length_1892_cov_104.653_component_0
    ## 1443  NODE_45_length_1714_cov_53.182_component_0
    ## 1456 NODE_47_length_1657_cov_101.802_component_0
    ## 1470 NODE_53_length_1196_cov_83.0806_component_0
    ## 1479 NODE_53_length_1196_cov_83.0806_component_0
    ## 1453  NODE_54_length_1100_cov_133.83_component_0
    ## 1476  NODE_54_length_1100_cov_133.83_component_0
    ## 1463  NODE_56_length_998_cov_92.7338_component_0
    ## 1434  NODE_57_length_960_cov_90.1901_component_0
    ## 1485  NODE_60_length_755_cov_199.537_component_0
    ## 1447  NODE_62_length_693_cov_72.5235_component_0
    ## 1466  NODE_63_length_691_cov_165.766_component_0
    ## 1467  NODE_65_length_641_cov_193.935_component_0
    ## 1482  NODE_65_length_641_cov_193.935_component_0
    ## 1406                                Quast_report

If we focus on "NODE\_42\_length\_1892\_cov\_104.653\_component\_0" we see it maps to two reference plasmids and the chromosome.

``` r
data_pspades_PMK1[data_pspades_PMK1$Node == "NODE_42_length_1892_cov_104.653_component_0", 
    ]
```

    ##                          Strain Length              Type
    ## 1437 Klebsiella_pneumoniae_PMK1   1892 Reference_plasmid
    ## 1459 Klebsiella_pneumoniae_PMK1   1892 Reference_plasmid
    ## 1486 Klebsiella_pneumoniae_PMK1   1892        Chromosome
    ##                                             Node
    ## 1437 NODE_42_length_1892_cov_104.653_component_0
    ## 1459 NODE_42_length_1892_cov_104.653_component_0
    ## 1486 NODE_42_length_1892_cov_104.653_component_0
    ##                                                                                            Reference_genome
    ## 1437   gi_749295880_ref_NZ_CP008930.1__Klebsiella_pneumoniae_strain_PMK1_plasmid_pPMK1-A__complete_sequence
    ## 1459 gi_749295989_ref_NZ_CP008933.1__Klebsiella_pneumoniae_strain_PMK1_plasmid_pPMK1-NDM__complete_sequence
    ## 1486                     gi_749295872_ref_NZ_CP008929.1__Klebsiella_pneumoniae_strain_PMK1__complete_genome

Finally, I wanted to illustrate the genome project "Rhodobacter\_sphaeroides\_2-4-1".

``` r
data_pspades_sphaeroides = subset(data_pspades, data_pspades$Strain == 
    "Rhodobacter_sphaeroides_2-4-1")
repeat_df = data_pspades_sphaeroides[duplicated(data_pspades_sphaeroides$Node), 
    ]
repeat_df[c("Node")]
```

    ##                                             Node
    ## 2063 NODE_10_length_1606_cov_563.905_component_0
    ## 2064 NODE_10_length_1606_cov_563.905_component_0
    ## 2055 NODE_11_length_1484_cov_217.888_component_0
    ## 2061 NODE_15_length_1033_cov_259.359_component_0
    ## 2040                                Quast_report

As shown in Supplementary Data, the "NODE\_10\_length\_1606\_cov\_563.905\_component\_0" was identified as a transposase and it did not allow to separate the 5 reference plasmids (all of them were merged in the same component)

``` r
data_pspades_sphaeroides[data_pspades_sphaeroides$Node == "NODE_10_length_1606_cov_563.905_component_0", 
    ]
```

    ##                             Strain Length              Type
    ## 2052 Rhodobacter_sphaeroides_2-4-1   1606 Reference_plasmid
    ## 2063 Rhodobacter_sphaeroides_2-4-1   1606        Chromosome
    ## 2064 Rhodobacter_sphaeroides_2-4-1   1606        Chromosome
    ##                                             Node
    ## 2052 NODE_10_length_1606_cov_563.905_component_0
    ## 2063 NODE_10_length_1606_cov_563.905_component_0
    ## 2064 NODE_10_length_1606_cov_563.905_component_0
    ##                                                                                                                    Reference_genome
    ## 2052 gi_484336769_ref_NZ_AKVW01000003.1__Rhodobacter_sphaeroides_2.4.1_plasmid_Ax__complete_sequence__whole_genome_shotgun_sequence
    ## 2063                                      gi_552535527_ref_NC_007493.2__Rhodobacter_sphaeroides_2.4.1_chromosome_1__complete_genome
    ## 2064                                      gi_552536492_ref_NC_007494.2__Rhodobacter_sphaeroides_2.4.1_chromosome_2__complete_genome

### Dealing with repeated sequences

As explained in the main manuscript, contigs mapping both to the reference plasmids and the chromosome were included in the plasmid fraction and considered as positive results from the prediction.

We sort the strains by strain and by type of sequence. In this way we have first the nodes corresponding to the reference plasmids. For example in the case of the "NODE\_42\_length\_1892\_cov\_104.653\_component\_0" it would only keep one row with the type "Reference\_plasmid"

``` r
data_pspades = data_pspades[c("Strain", "Length", "Type", "Node")]
data_pspades <- data_pspades[order(data_pspades$Strain), ]
data_pspades <- data_pspades[order(data_pspades$Type, decreasing = TRUE), 
    ]
head(data_pspades)
```

    ##                      Strain Length              Type
    ## 16 Aeromonas_veronii_AVNIH1   6515 Reference_plasmid
    ## 7  Aeromonas_veronii_AVNIH1   6311 Reference_plasmid
    ## 17 Aeromonas_veronii_AVNIH1   3780 Reference_plasmid
    ## 5  Aeromonas_veronii_AVNIH1   3441 Reference_plasmid
    ## 13 Aeromonas_veronii_AVNIH1  61704 Reference_plasmid
    ## 12 Aeromonas_veronii_AVNIH1   2033 Reference_plasmid
    ##                                           Node
    ## 16 NODE_10_length_6515_cov_122.388_component_0
    ## 7  NODE_11_length_6311_cov_125.623_component_0
    ## 17 NODE_15_length_3780_cov_138.725_component_0
    ## 5  NODE_17_length_3441_cov_118.229_component_0
    ## 13 NODE_1_length_61704_cov_160.884_component_0
    ## 12 NODE_20_length_2033_cov_133.153_component_0

``` r
data_not_duplicates <- data_pspades[!duplicated(data_pspades$Node), 
    ]
```

We can check the "NODE\_42\_length\_1892\_cov\_104.653\_component\_0" looking for duplicates and the type. We see there are not extra rows with this contig, and the only one has the type of "Reference\_plasmid"

``` r
data_pspades_PMK1 = subset(data_not_duplicates, data_not_duplicates$Strain == 
    "Enterobacter_cloacae_CAV1411")
repeat_df = data_pspades_PMK1[duplicated(data_pspades_PMK1$Node), 
    ]
repeat_df
```

    ## [1] Strain Length Type   Node  
    ## <0 rows> (or 0-length row.names)

``` r
data_pspades_PMK1[data_pspades_PMK1$Node == "NODE_42_length_1892_cov_104.653_component_0", 
    ]
```

    ## [1] Strain Length Type   Node  
    ## <0 rows> (or 0-length row.names)

### Downstream analysis

First we define some variables

``` r
vec_rate_chromosome <- NULL
vec_rate_plasmid <- NULL
vec_rate_unalignments <- NULL
vec_recall <- NULL
vec_chromosome_fraction <- NULL
vec_precision <- NULL
vec_total_predicted_pspades <- NULL
total_unalignments <- 0
total_ref_plasmids <- 0
total_chromosome <- 0
```

Then we extract the name of all the genome projects we have analyzed

``` r
data_pspades <- data_pspades[order(data_pspades$Strain), ]
data_not_duplicates <- data_not_duplicates[order(data_not_duplicates$Strain), 
    ]
strains = unique(data_not_duplicates$Strain)
strains
```

    ##  [1] Aeromonas_veronii_AVNIH1           Bacillus_subtilis_BEST195         
    ##  [3] Burkholderia_cenocepacia_DDS_22E-1 Citrobacter_freundii_CAV1321      
    ##  [5] Citrobacter_freundii_CAV1741       Citrobacter_freundii_CFNIH1       
    ##  [7] Corynebacterium_callunae_DSM_20147 Enterobacter_aerogenes_CAV1320    
    ##  [9] Enterobacter_asburiae_CAV1043      Enterobacter_cloacae_CAV1311      
    ## [11] Enterobacter_cloacae_CAV1411       Enterobacter_cloacae_CAV1668      
    ## [13] Enterobacter_cloacae_CAV1669       Enterobacter_cloacae_ECNIH2       
    ## [15] Enterobacter_cloacae_ECNIH3        Enterobacter_cloacae_ECR091       
    ## [17] Enterococcus_faecium_700221        Escherichia_coli_EC0889           
    ## [19] Escherichia_coli_JJ1886            Escherichia_coli_JJ1887           
    ## [21] Klebsiella_oxytoca_CAV1099         Klebsiella_oxytoca_CAV1335        
    ## [23] Klebsiella_oxytoca_CAV1374         Klebsiella_oxytoca_KONIH1         
    ## [25] Klebsiella_pneumoniae_AATZP        Klebsiella_pneumoniae_CAV1193     
    ## [27] Klebsiella_pneumoniae_CAV1344      Klebsiella_pneumoniae_CAV1392     
    ## [29] Klebsiella_pneumoniae_CAV1596      Klebsiella_pneumoniae_KPN223      
    ## [31] Klebsiella_pneumoniae_KPN555       Klebsiella_pneumoniae_KPNIH1      
    ## [33] Klebsiella_pneumoniae_KPNIH10      Klebsiella_pneumoniae_KPNIH24     
    ## [35] Klebsiella_pneumoniae_KPNIH27      Klebsiella_pneumoniae_KPNIH36     
    ## [37] Klebsiella_pneumoniae_KPNIH39      Klebsiella_pneumoniae_PMK1        
    ## [39] Kluyvera_intermedia_CAV1151        Providencia_stuartii_ATCC_33672   
    ## [41] Rhodobacter_sphaeroides_2-4-1      Serratia_marcescens_CAV1492       
    ## 42 Levels: Aeromonas_veronii_AVNIH1 ... Serratia_marcescens_CAV1492

And then we calculate all the frequencies for each bacterial strain

``` r
for (i in strains) {
    pspades_certain_genome <- subset(data_not_duplicates, data_not_duplicates$Strain == 
        i)
    pspades_certain_genome_fraction <- subset(data_pspades, data_pspades$Strain == 
        i)
    ref_plasmid <- sum(pspades_certain_genome[which(pspades_certain_genome[, 
        3] == "Reference_plasmid"), 2])
    chromosome <- sum(pspades_certain_genome[which(pspades_certain_genome[, 
        3] == "Chromosome"), 2])
    new_alignments <- sum(pspades_certain_genome[which(pspades_certain_genome[, 
        3] == "New_plasmids"), 2])
    total_unalignments <- total_unalignments + new_alignments
    total_ref_plasmids <- total_ref_plasmids + ref_plasmid
    total_chromosome <- total_chromosome + chromosome
    recall <- sum(pspades_certain_genome_fraction[which(pspades_certain_genome_fraction[, 
        3] == "Plasmid_genome_fraction"), 2])
    chromosome_genome_fraction <- sum(pspades_certain_genome_fraction[which(pspades_certain_genome_fraction[, 
        3] == "Chromosome_genome_fraction"), 2])
    rate_plasmid <- ref_plasmid/(ref_plasmid + chromosome + new_alignments)
    rate_chromosome <- chromosome/(ref_plasmid + chromosome + 
        new_alignments)
    rate_unalignments <- new_alignments/(ref_plasmid + chromosome + 
        new_alignments)
    total_predicted <- ref_plasmid + chromosome + new_alignments
    vec_rate_plasmid <- append(vec_rate_plasmid, rate_plasmid, 
        length(vec_rate_plasmid))
    vec_rate_chromosome <- append(vec_rate_chromosome, rate_chromosome, 
        length(vec_rate_chromosome))
    vec_rate_unalignments <- append(vec_rate_unalignments, rate_unalignments, 
        length(vec_rate_unalignments))
    precision <- ref_plasmid/(ref_plasmid + chromosome)
    vec_precision <- append(vec_precision, precision, length(vec_precision))
    vec_total_predicted_pspades <- append(vec_total_predicted_pspades, 
        total_predicted, length(vec_total_predicted_pspades))
    vec_recall <- append(vec_recall, recall, length(vec_recall))
    vec_chromosome_fraction <- append(vec_chromosome_fraction, 
        chromosome_genome_fraction, length(vec_chromosome_fraction))
}
```

Finally we transform some vectors to kbp

``` r
# vec_precision <- vec_precision*100
vec_total_predicted_pspades <- vec_total_predicted_pspades/1000
vec_total_predicted_pspades <- round(vec_total_predicted_pspades, 
    digits = 4)
```

And we plot the frequencies obtained by PlasmidSPAdes

``` r
dataframe_plot_pspades <- data.frame(vec_rate_chromosome, vec_rate_plasmid, 
    vec_rate_unalignments)
colnames(dataframe_plot_pspades) = c("Chromosome", "Ref_plasmid", 
    "New")
rownames(dataframe_plot_pspades) = strains
dataframe_plot_pspades = t(dataframe_plot_pspades)
dataframe_plot_pspades = prop.table(dataframe_plot_pspades, 2)
dataframe_plot_pspades = dataframe_plot_pspades[, number_order_reference$Strain]
df_total_predicted_pspades = data.frame(vec_total_predicted_pspades)
df_total_predicted_pspades = data.frame(vec_total_predicted_pspades)
df_total_predicted_pspades = cbind(df_total_predicted_pspades, 
    alpha_order_reference$Strain)
df_total_predicted_pspades = df_total_predicted_pspades[number_order_reference$Strain, 
    ]
par(mfrow = c(2, 1))
par(mar = c(0.1, 4.1, 1.1, 5.1), xpd = TRUE)
par(las = 2)
plot_pspades = barplot(dataframe_plot_pspades, horiz = FALSE, 
    col = c("white", "lightgreen", "purple3"), legend.text = FALSE, 
    args.legend = list(x = "topright", inset = c(-0.8, 0), cex = 0.7), 
    axes = FALSE, ylab = "PlasmidSPAdes")
axis(2, ylim = c(0, 1), col = "black", las = 1)
par(new = TRUE)
num_max = length(strains) - 0.5
plot_length_pspades = plot(rep(0.5:num_max, 1), df_total_predicted_pspades$vec_total_predicted_pspades, 
    axes = F, type = "b", pch = 20, xlab = "", ylim = c(0, max(df_total_predicted_pspades$vec_total_predicted_pspades)), 
    ylab = "", col = "blue", xlim = c(0, length(strains)))
axis(4, ylim = c(min(df_total_predicted_pspades$vec_total_predicted_pspades, 
    max(df_total_predicted_pspades$vec_total_predicted_pspades))), 
    col = "blue", las = 1, col.axis = "blue")
mtext("", side = 4, line = 4, cex = 0.9, col = "blue", las = 0)
```

![](detailed_analysis_files/figure-markdown_github/frequencies_pspades-1.png)

Then we plot the precision and recall of PlasmidSPAdes

``` r
vec_recall <- vec_recall/100
dataframe_both_pspades <- data.frame(vec_precision, vec_recall)
colnames(dataframe_both_pspades) <- c("Precision", "Recall")
rownames(dataframe_both_pspades) <- strains
dataframe_both_pspades <- t(dataframe_both_pspades)
dataframe_both_pspades = dataframe_both_pspades[, number_order_reference$Strain]
par(mfrow = c(2, 1))
par(mar = c(0.1, 4.1, 1.1, 5.1), xpd = TRUE)
par(las = 2)
plot_precision_recall_pspades <- barplot(dataframe_both_pspades, 
    horiz = FALSE, col = c("azure2", "dimgrey"), ylab = "PlasmidSPAdes", 
    legend.text = FALSE, beside = TRUE, args.legend = list(x = "topright", 
        inset = c(-0.8, 0), cex = 0.7), ylim = c(0, 1))
axis(2, ylim = c(0, 1), col = "black", las = 1)
par(new = TRUE)
plot_length_reference_plasmids <- plot(rep(0.5:num_max, 1), number_order_reference$Length, 
    axes = F, type = "b", pch = 20, xlab = "", ylim = c(0, max(number_order_reference$Length)), 
    ylab = "", col = "darkred", xlim = c(0, length(strains)))
axis(4, ylim = c(min(number_order_reference$Length, max(number_order_reference$Length))), 
    col = "darkred", las = 1, col.axis = "darkred")
mtext("", side = 4, line = 4, cex = 0.9, col = "darkred", las = 0)
```

![](detailed_analysis_files/figure-markdown_github/precision_recall_pspades-1.png)

### Summary of the results

We save the dataframe containning the results from PlasmidSPAdes

``` r
precision_recall_pspades_results <- t(dataframe_both_pspades)
fractions_pspades_results <- t(dataframe_plot_pspades)
summary_pspades_results <- cbind(precision_recall_pspades_results, 
    fractions_pspades_results[, c(1, 2, 3)])
colnames(summary_pspades_results) <- c("Precision", "Recall", 
    "Chromosome fraction", "Plasmid fraction", "Novel sequences fraction")
head(summary_pspades_results)
```

    ##                                    Precision  Recall Chromosome fraction
    ## Burkholderia_cenocepacia_DDS_22E-1       NaN 0.00000                 NaN
    ## Bacillus_subtilis_BEST195          0.7770809 1.00000           0.1295762
    ## Enterobacter_aerogenes_CAV1320     0.2765690 1.00000           0.7234310
    ## Providencia_stuartii_ATCC_33672    0.7658079 1.00000           0.2341921
    ## Corynebacterium_callunae_DSM_20147 0.2879351 0.04322           0.7120649
    ## Enterobacter_cloacae_CAV1411       0.4943442 0.99345           0.5056558
    ##                                    Plasmid fraction
    ## Burkholderia_cenocepacia_DDS_22E-1              NaN
    ## Bacillus_subtilis_BEST195                 0.4516938
    ## Enterobacter_aerogenes_CAV1320            0.2765690
    ## Providencia_stuartii_ATCC_33672           0.7658079
    ## Corynebacterium_callunae_DSM_20147        0.2879351
    ## Enterobacter_cloacae_CAV1411              0.4943442
    ##                                    Novel sequences fraction
    ## Burkholderia_cenocepacia_DDS_22E-1                      NaN
    ## Bacillus_subtilis_BEST195                         0.4187301
    ## Enterobacter_aerogenes_CAV1320                    0.0000000
    ## Providencia_stuartii_ATCC_33672                   0.0000000
    ## Corynebacterium_callunae_DSM_20147                0.0000000
    ## Enterobacter_cloacae_CAV1411                      0.0000000

``` r
summary(summary_pspades_results)
```

    ##    Precision          Recall       Chromosome fraction Plasmid fraction 
    ##  Min.   :0.1440   Min.   :0.0000   Min.   :0.00000     Min.   :0.01988  
    ##  1st Qu.:0.6065   1st Qu.:0.7238   1st Qu.:0.02278     1st Qu.:0.53828  
    ##  Median :0.7771   Median :0.9651   Median :0.12958     Median :0.76581  
    ##  Mean   :0.7542   Mean   :0.8041   Mean   :0.21962     Mean   :0.71730  
    ##  3rd Qu.:0.9771   3rd Qu.:0.9871   3rd Qu.:0.29945     3rd Qu.:0.96074  
    ##  Max.   :1.0000   Max.   :1.0000   Max.   :0.83723     Max.   :1.00000  
    ##  NA's   :1                         NA's   :1           NA's   :1        
    ##  Novel sequences fraction
    ##  Min.   :0.00000         
    ##  1st Qu.:0.00000         
    ##  Median :0.00000         
    ##  Mean   :0.06308         
    ##  3rd Qu.:0.01985         
    ##  Max.   :0.91062         
    ##  NA's   :1

``` r
write.csv(summary_pspades_results, file = "/home/arredondo/Data/All_Genomes/summary_results_pspades.csv", 
    row.names = TRUE)
```

### Overall precision and recall

Once the individual values for precision and recall were calculated, we decided to filter out from the analysis our negative control corresponding to the strain "Burkholderia\_cenocepacia\_DDS\_22E-1", which does not have any reference plasmid.

``` r
overall_precision_recall_pspades <- summary_pspades_results[!row.names(summary_pspades_results) %in% 
    "Burkholderia_cenocepacia_DDS_22E-1", ]
summary(overall_precision_recall_pspades)
```

    ##    Precision          Recall        Chromosome fraction Plasmid fraction 
    ##  Min.   :0.1440   Min.   :0.04322   Min.   :0.00000     Min.   :0.01988  
    ##  1st Qu.:0.6065   1st Qu.:0.85582   1st Qu.:0.02278     1st Qu.:0.53828  
    ##  Median :0.7771   Median :0.96860   Median :0.12958     Median :0.76581  
    ##  Mean   :0.7542   Mean   :0.82366   Mean   :0.21962     Mean   :0.71730  
    ##  3rd Qu.:0.9771   3rd Qu.:0.98715   3rd Qu.:0.29945     3rd Qu.:0.96074  
    ##  Max.   :1.0000   Max.   :1.00000   Max.   :0.83723     Max.   :1.00000  
    ##  Novel sequences fraction
    ##  Min.   :0.00000         
    ##  1st Qu.:0.00000         
    ##  Median :0.00000         
    ##  Mean   :0.06308         
    ##  3rd Qu.:0.01985         
    ##  Max.   :0.91062

Recycler
--------

We exactly do the same for Recycler

``` r
data_recycler = data_recycler[c("Strain", "Length", "Type", "Node")]
data_recycler <- data_recycler[order(data_recycler$Strain), ]
data_recycler <- data_recycler[order(data_recycler$Type, decreasing = TRUE), 
    ]
head(data_recycler)
```

    ##                          Strain Length              Type
    ## 7     Bacillus_subtilis_BEST195   5820 Reference_plasmid
    ## 16 Citrobacter_freundii_CAV1321   1303 Reference_plasmid
    ## 20 Citrobacter_freundii_CAV1741   1303 Reference_plasmid
    ## 21 Citrobacter_freundii_CAV1741 109688 Reference_plasmid
    ## 22 Citrobacter_freundii_CAV1741   1916 Reference_plasmid
    ## 23 Citrobacter_freundii_CAV1741   3223 Reference_plasmid
    ##                                  Node
    ## 7  RNODE_2_length_5892_cov_4530.17000
    ## 16  RNODE_2_length_1347_cov_102.55832
    ## 20  RNODE_5_length_1347_cov_101.13792
    ## 21 RNODE_1_length_109710_cov_49.61400
    ## 22  RNODE_2_length_1938_cov_723.33500
    ## 23  RNODE_3_length_3245_cov_642.14700

``` r
data_not_duplicates <- data_recycler[!duplicated(data_recycler$Node), 
    ]
data_recycler <- data_recycler[order(data_recycler$Strain), ]
data_not_duplicates <- data_not_duplicates[order(data_not_duplicates$Strain), 
    ]
vec_rate_chromosome <- NULL
vec_rate_plasmid <- NULL
vec_rate_unalignments <- NULL
vec_recall <- NULL
vec_chromosome_fraction <- NULL
vec_precision <- NULL
vec_total_predicted <- NULL
total_unalignments <- 0
total_ref_plasmids <- 0
total_chromosome <- 0
strains = unique(data_recycler$Strain)
for (i in strains) {
    recycler_certain_genome <- subset(data_not_duplicates, data_not_duplicates$Strain == 
        i)
    recycler_certain_genome_fraction <- subset(data_recycler, 
        data_recycler$Strain == i)
    ref_plasmid <- sum(recycler_certain_genome[which(recycler_certain_genome[, 
        3] == "Reference_plasmid"), 2])
    chromosome <- sum(recycler_certain_genome[which(recycler_certain_genome[, 
        3] == "Chromosome"), 2])
    new_alignments <- sum(recycler_certain_genome[which(recycler_certain_genome[, 
        3] == "New_plasmids"), 2])
    recall <- sum(recycler_certain_genome_fraction[which(recycler_certain_genome_fraction[, 
        3] == "Plasmid_genome_fraction"), 2])
    chromosome_genome_fraction <- sum(recycler_certain_genome_fraction[which(recycler_certain_genome_fraction[, 
        3] == "Chromosome_genome_fraction"), 2])
    rate_plasmid <- ref_plasmid/(ref_plasmid + chromosome + new_alignments)
    rate_chromosome <- chromosome/(ref_plasmid + chromosome + 
        new_alignments)
    rate_unalignments <- new_alignments/(ref_plasmid + chromosome + 
        new_alignments)
    total_predicted <- ref_plasmid + chromosome + new_alignments
    total_unalignments <- total_unalignments + new_alignments
    total_ref_plasmids <- total_ref_plasmids + ref_plasmid
    total_chromosome <- total_chromosome + chromosome
    vec_rate_plasmid <- append(vec_rate_plasmid, rate_plasmid, 
        length(vec_rate_plasmid))
    vec_rate_chromosome <- append(vec_rate_chromosome, rate_chromosome, 
        length(vec_rate_chromosome))
    vec_rate_unalignments <- append(vec_rate_unalignments, rate_unalignments, 
        length(vec_rate_unalignments))
    precision <- ref_plasmid/(ref_plasmid + chromosome)
    vec_precision <- append(vec_precision, precision, length(vec_precision))
    vec_total_predicted <- append(vec_total_predicted, total_predicted, 
        length(vec_total_predicted))
    vec_recall <- append(vec_recall, recall, length(vec_recall))
    vec_chromosome_fraction <- append(vec_chromosome_fraction, 
        chromosome_genome_fraction, length(vec_chromosome_fraction))
}
# vec_precision <- vec_precision*100
vec_total_predicted <- vec_total_predicted/1000
vec_total_predicted <- round(vec_total_predicted, digits = 1)
dataframe_plot_recycler <- data.frame(vec_rate_chromosome, vec_rate_plasmid, 
    vec_rate_unalignments)
colnames(dataframe_plot_recycler) <- c("Chromosome", "Ref_plasmid", 
    "New")
rownames(dataframe_plot_recycler) <- strains
dataframe_plot_recycler <- t(dataframe_plot_recycler)
par(las = 2)
dataframe_plot_recycler <- prop.table(dataframe_plot_recycler, 
    2)
dataframe_plot_recycler = dataframe_plot_recycler[, number_order_reference$Strain]
df_total_predicted_recycler = data.frame(vec_total_predicted)
df_total_predicted_recycler = data.frame(vec_total_predicted)
df_total_predicted_recycler = cbind(df_total_predicted_recycler, 
    alpha_order_reference$Strain)
df_total_predicted_recycler = df_total_predicted_recycler[number_order_reference$Strain, 
    ]
```

Plot the frequencies from Recycler

``` r
par(mfrow = c(2, 1))
par(mar = c(0.1, 4.1, 1.1, 5.1), xpd = TRUE)
par(las = 2)
plot_recycler <- barplot(dataframe_plot_recycler, horiz = FALSE, 
    col = c("white", "lightgreen", "purple3"), legend.text = FALSE, 
    args.legend = list(x = "topright", inset = c(-0.8, 0), cex = 0.7), 
    axes = FALSE, ylab = "Recycler")
axis(2, ylim = c(0, 1), col = "black", las = 1)
par(new = TRUE)
num_max <- length(strains) - 0.5
plot_length_recycler = plot(rep(0.5:num_max, 1), df_total_predicted_recycler$vec_total_predicted, 
    axes = F, type = "b", pch = 20, xlab = "", ylim = c(0, max(df_total_predicted_recycler$vec_total_predicted)), 
    ylab = "", col = "blue", xlim = c(0, length(strains)))
axis(4, ylim = c(min(df_total_predicted_recycler$vec_total_predicted, 
    max(df_total_predicted_recycler$vec_total_predicted))), col = "blue", 
    las = 1, col.axis = "blue")
mtext("", side = 4, line = 4, cex = 0.9, col = "blue", las = 0)
```

![](detailed_analysis_files/figure-markdown_github/frequencies_recycler-1.png)

Finally, we can also plot the precision and recall of Recycler

``` r
vec_recall <- vec_recall/100
dataframe_both_recycler <- data.frame(vec_precision, vec_recall)
colnames(dataframe_both_recycler) <- c("Precision", "Recall")
rownames(dataframe_both_recycler) <- strains
dataframe_both_recycler <- t(dataframe_both_recycler)
dataframe_both_recycler = dataframe_both_recycler[, number_order_reference$Strain]
par(mfrow = c(2, 1))
par(mar = c(0.1, 4.1, 1.1, 5.1), xpd = TRUE)
par(las = 2)
plot_precision_recall_recycler <- barplot(dataframe_both_recycler, 
    horiz = FALSE, col = c("azure2", "dimgrey"), ylab = "Recycler", 
    legend.text = FALSE, beside = TRUE, args.legend = list(x = "topright", 
        inset = c(-0.8, 0), cex = 0.7), ylim = c(0, 1))
axis(2, ylim = c(0, 1), col = "black", las = 1)
par(new = TRUE)
plot_length_reference_plasmids <- plot(rep(0.5:num_max, 1), number_order_reference$Length, 
    axes = F, type = "b", pch = 20, xlab = "", ylim = c(0, max(number_order_reference$Length)), 
    ylab = "", col = "darkred", xlim = c(0, length(strains)))
axis(4, ylim = c(min(number_order_reference$Length, max(number_order_reference$Length))), 
    col = "darkred", las = 1, col.axis = "darkred")
mtext("", side = 4, line = 4, cex = 0.9, col = "darkred", las = 0)
```

![](detailed_analysis_files/figure-markdown_github/precision_recall_recycler-1.png)

### Summary of the results

We save the dataframe containning the results from Recycler

``` r
precision_recall_recycler_results <- t(dataframe_both_recycler)
fractions_recycler_results <- t(dataframe_plot_recycler)
summary_recycler_results <- cbind(precision_recall_recycler_results, 
    fractions_recycler_results[, c(1, 2, 3)])
colnames(summary_recycler_results) <- c("Precision", "Recall", 
    "Chromosome fraction", "Plasmid fraction", "Novel sequences fraction")
head(summary_recycler_results)
```

    ##                                    Precision  Recall Chromosome fraction
    ## Burkholderia_cenocepacia_DDS_22E-1       NaN 0.00000                 NaN
    ## Bacillus_subtilis_BEST195          0.6476021 1.00000          0.22034370
    ## Enterobacter_aerogenes_CAV1320     1.0000000 1.00000          0.00000000
    ## Providencia_stuartii_ATCC_33672    0.9450277 1.00000          0.05497225
    ## Corynebacterium_callunae_DSM_20147 1.0000000 0.04235          0.00000000
    ## Enterobacter_cloacae_CAV1411       0.0000000 0.00000          1.00000000
    ##                                    Plasmid fraction
    ## Burkholderia_cenocepacia_DDS_22E-1              NaN
    ## Bacillus_subtilis_BEST195                 0.4049259
    ## Enterobacter_aerogenes_CAV1320            1.0000000
    ## Providencia_stuartii_ATCC_33672           0.9450277
    ## Corynebacterium_callunae_DSM_20147        1.0000000
    ## Enterobacter_cloacae_CAV1411              0.0000000
    ##                                    Novel sequences fraction
    ## Burkholderia_cenocepacia_DDS_22E-1                      NaN
    ## Bacillus_subtilis_BEST195                         0.3747304
    ## Enterobacter_aerogenes_CAV1320                    0.0000000
    ## Providencia_stuartii_ATCC_33672                   0.0000000
    ## Corynebacterium_callunae_DSM_20147                0.0000000
    ## Enterobacter_cloacae_CAV1411                      0.0000000

``` r
write.csv(summary_recycler_results, file = "/home/arredondo/Data/All_Genomes/summary_results_recycler.csv", 
    row.names = TRUE)
```

### Overall precision and recall

Once the individual values for precision and recall were calculated, we decided to filter out from the analysis our negative control corresponding to the strain "Burkholderia\_cenocepacia\_DDS\_22E-1", which does not have any reference plasmid.

``` r
overall_precision_recall_recycler <- summary_recycler_results[!row.names(summary_recycler_results) %in% 
    "Burkholderia_cenocepacia_DDS_22E-1", ]
summary(overall_precision_recall_recycler)
```

    ##    Precision            Recall        Chromosome fraction Plasmid fraction
    ##  Min.   :0.000000   Min.   :0.00000   Min.   :0.0000      Min.   :0.0000  
    ##  1st Qu.:0.000000   1st Qu.:0.00000   1st Qu.:0.2203      1st Qu.:0.0000  
    ##  Median :0.009472   Median :0.00000   Median :0.9113      Median :0.0000  
    ##  Mean   :0.298674   Mean   :0.11599   Mean   :0.6191      Mean   :0.2443  
    ##  3rd Qu.:0.673201   3rd Qu.:0.06666   3rd Qu.:1.0000      3rd Qu.:0.4049  
    ##  Max.   :1.000000   Max.   :1.00000   Max.   :1.0000      Max.   :1.0000  
    ##  NA's   :3                                                                
    ##  Novel sequences fraction
    ##  Min.   :0.00000         
    ##  1st Qu.:0.00000         
    ##  Median :0.00000         
    ##  Mean   :0.13661         
    ##  3rd Qu.:0.04955         
    ##  Max.   :1.00000         
    ## 

cBAR
----

### Translocations between chromosome and plasmids

We have decided to filter contigs with &gt;500 bp and &gt; 95% identity alignment, mapping to the chromosome and to a reference plasmid. This case is diferent from the one presented above, where the whole contig mapped to the chromosome or to a reference plasmid. In this case a fragment of the contig is mapping to the chromosome and the other to a plasmid. Using Quast and Icarus we have identified which cases for cBAR this has occured (very unfrequent events)

``` r
translocation_cbar <- read.csv("/home/arredondo/Data/All_Genomes/translocation_events_cbar", 
    sep = "\t")
table(translocation_cbar$Strain)
```

    ## 
    ## Enterococcus_faecium_700221 
    ##                           1

We can see it only has happened in one genome project and in a single contig

``` r
data_cbar = data_cbar[c("Strain", "Length", "Type", "Node")]
data_cbar <- data_cbar[order(data_cbar$Strain), ]
data_cbar <- data_cbar[order(data_cbar$Type, decreasing = TRUE), 
    ]
data_cbar <- data_cbar[!(data_cbar$Node %in% translocation_cbar$Node), 
    ]
head(data_cbar)
```

    ##                        Strain Length              Type
    ## 1482 Aeromonas_veronii_AVNIH1  56551 Reference_plasmid
    ## 1483 Aeromonas_veronii_AVNIH1  56551 Reference_plasmid
    ## 1484 Aeromonas_veronii_AVNIH1  42608 Reference_plasmid
    ## 1485 Aeromonas_veronii_AVNIH1  34591 Reference_plasmid
    ## 1492 Aeromonas_veronii_AVNIH1   6515 Reference_plasmid
    ## 1493 Aeromonas_veronii_AVNIH1   6311 Reference_plasmid
    ##                                  Node
    ## 1482 NODE_29_length_56551_cov_160.023
    ## 1483 NODE_29_length_56551_cov_160.023
    ## 1484 NODE_38_length_42608_cov_151.337
    ## 1485 NODE_43_length_34591_cov_165.807
    ## 1492  NODE_72_length_6515_cov_122.388
    ## 1493  NODE_73_length_6311_cov_125.623

``` r
data_not_duplicates <- data_cbar[!duplicated(data_cbar$Node), 
    ]
data_cbar <- data_cbar[order(data_cbar$Strain), ]
data_not_duplicates <- data_not_duplicates[order(data_not_duplicates$Strain), 
    ]
vec_rate_chromosome <- NULL
vec_rate_plasmid <- NULL
vec_rate_unalignments <- NULL
vec_precision <- NULL
vec_total_predicted <- NULL
total_unalignments <- 0
total_ref_plasmids <- 0
total_chromosome <- 0
strains = unique(data_cbar$Strain)
for (i in strains) {
    cbar_certain_genome <- subset(data_not_duplicates, data_not_duplicates$Strain == 
        i)
    ref_plasmid <- sum(cbar_certain_genome[which(cbar_certain_genome[, 
        3] == "Reference_plasmid"), 2])
    chromosome <- sum(cbar_certain_genome[which(cbar_certain_genome[, 
        3] == "Chromosome"), 2])
    new_alignments <- sum(cbar_certain_genome[which(cbar_certain_genome[, 
        3] == "New_plasmids"), 2])
    rate_plasmid <- ref_plasmid/(ref_plasmid + chromosome + new_alignments)
    rate_chromosome <- chromosome/(ref_plasmid + chromosome + 
        new_alignments)
    rate_unalignments <- new_alignments/(ref_plasmid + chromosome + 
        new_alignments)
    total_predicted <- ref_plasmid + chromosome + new_alignments
    total_unalignments <- total_unalignments + new_alignments
    total_ref_plasmids <- total_ref_plasmids + ref_plasmid
    total_chromosome <- total_chromosome + chromosome
    vec_rate_plasmid <- append(vec_rate_plasmid, rate_plasmid, 
        length(vec_rate_plasmid))
    vec_rate_chromosome <- append(vec_rate_chromosome, rate_chromosome, 
        length(vec_rate_chromosome))
    vec_rate_unalignments <- append(vec_rate_unalignments, rate_unalignments, 
        length(vec_rate_unalignments))
    precision <- ref_plasmid/(ref_plasmid + chromosome)
    vec_precision <- append(vec_precision, precision, length(vec_precision))
    vec_total_predicted <- append(vec_total_predicted, total_predicted, 
        length(vec_total_predicted))
}
# vec_precision <- vec_precision*100
vec_total_predicted <- vec_total_predicted/1000
vec_total_predicted <- round(vec_total_predicted, digits = 1)
dataframe_plot_cbar <- data.frame(vec_rate_chromosome, vec_rate_plasmid, 
    vec_rate_unalignments)
colnames(dataframe_plot_cbar) <- c("Chromosome", "Ref_plasmid", 
    "New")
rownames(dataframe_plot_cbar) <- strains
dataframe_plot_cbar <- t(dataframe_plot_cbar)
par(las = 2)
dataframe_plot_cbar <- prop.table(dataframe_plot_cbar, 2)
dataframe_plot_cbar = dataframe_plot_cbar[, number_order_reference$Strain]
df_total_predicted_cbar = data.frame(vec_total_predicted)
df_total_predicted_cbar = cbind(df_total_predicted_cbar, alpha_order_reference$Strain)
df_total_predicted_cbar = df_total_predicted_cbar[number_order_reference$Strain, 
    ]
```

Plot the frequencies of cBAR

``` r
par(mfrow = c(2, 1))
par(mar = c(0.1, 4.1, 1.1, 5.1), xpd = TRUE)
par(las = 2)
plot_cbar <- barplot(dataframe_plot_cbar, horiz = FALSE, col = c("white", 
    "lightgreen", "purple3"), legend.text = FALSE, args.legend = list(x = "topright", 
    inset = c(-0.8, 0), cex = 0.7), axes = FALSE, ylab = "cBAR")
axis(2, ylim = c(0, 1), col = "black", las = 1)
par(new = TRUE)
num_max <- length(strains) - 0.5
plot_length_cbar = plot(rep(0.5:num_max, 1), df_total_predicted_cbar$vec_total_predicted, 
    axes = F, type = "b", pch = 20, xlab = "", ylim = c(0, max(df_total_predicted_cbar$vec_total_predicted)), 
    ylab = "", col = "blue", xlim = c(0, length(strains)))
axis(4, ylim = c(min(df_total_predicted_cbar$vec_total_predicted, 
    max(df_total_predicted_cbar$vec_total_predicted))), col = "blue", 
    las = 1, col.axis = "blue")
mtext("", side = 4, line = 4, cex = 0.9, col = "blue", las = 0)
```

![](detailed_analysis_files/figure-markdown_github/frequencies_cbar-1.png)

Now we load the file with the recall values for each bacterial isolate

``` r
data_cbar_recall <- data_cbar_recall[order(data_cbar_recall$Strain), 
    ]
data_cbar_recall[is.na(data_cbar_recall)] <- 0
vec_recall = data_cbar_recall$Recall
vec_recall <- vec_recall/100
dataframe_both_cbar = data.frame(vec_precision, vec_recall)
colnames(dataframe_both_cbar) <- c("Precision", "Recall")
rownames(dataframe_both_cbar) <- strains
dataframe_both_cbar <- t(dataframe_both_cbar)
dataframe_both_cbar = dataframe_both_cbar[, number_order_reference$Strain]
par(mfrow = c(2, 1))
par(mar = c(0.1, 4.1, 1.1, 5.1), xpd = TRUE)
par(las = 2)
plot_precision_recall_cbar <- barplot(dataframe_both_cbar, horiz = FALSE, 
    col = c("azure2", "dimgrey"), ylab = "cBAR", legend.text = FALSE, 
    beside = TRUE, args.legend = list(x = "topright", inset = c(-0.8, 
        0), cex = 0.7), ylim = c(0, 1))
axis(2, ylim = c(0, 1), col = "black", las = 1)
par(new = TRUE)
plot_length_reference_plasmids <- plot(rep(0.5:num_max, 1), number_order_reference$Length, 
    axes = F, type = "b", pch = 20, xlab = "", ylim = c(0, max(number_order_reference$Length)), 
    ylab = "", col = "darkred", xlim = c(0, length(strains)))
axis(4, ylim = c(min(number_order_reference$Length, max(number_order_reference$Length))), 
    col = "darkred", las = 1, col.axis = "darkred")
mtext("", side = 4, line = 4, cex = 0.9, col = "darkred", las = 0)
```

![](detailed_analysis_files/figure-markdown_github/precision_recall_cbar-1.png)

### Summary of the results

We save the dataframe containning the results from cBar

``` r
precision_recall_cbar_results <- t(dataframe_both_cbar)
fractions_cbar_results <- t(dataframe_plot_cbar)
summary_cbar_results <- cbind(precision_recall_cbar_results, 
    fractions_cbar_results[, c(1, 2, 3)])
colnames(summary_cbar_results) <- c("Precision", "Recall", "Chromosome fraction", 
    "Plasmid fraction", "Novel sequences fraction")
head(summary_cbar_results)
```

    ##                                    Precision  Recall Chromosome fraction
    ## Burkholderia_cenocepacia_DDS_22E-1 0.0000000 0.00000           1.0000000
    ## Bacillus_subtilis_BEST195          0.0000000 0.00000           0.8813847
    ## Enterobacter_aerogenes_CAV1320     0.0000000 0.00000           0.2751727
    ## Providencia_stuartii_ATCC_33672    0.3353460 1.00000           0.6622348
    ## Corynebacterium_callunae_DSM_20147 0.7120571 0.09253           0.2003811
    ## Enterobacter_cloacae_CAV1411       0.8411966 0.96574           0.1581390
    ##                                    Plasmid fraction
    ## Burkholderia_cenocepacia_DDS_22E-1        0.0000000
    ## Bacillus_subtilis_BEST195                 0.0000000
    ## Enterobacter_aerogenes_CAV1320            0.0000000
    ## Providencia_stuartii_ATCC_33672           0.3341255
    ## Corynebacterium_callunae_DSM_20147        0.4955246
    ## Enterobacter_cloacae_CAV1411              0.8376769
    ##                                    Novel sequences fraction
    ## Burkholderia_cenocepacia_DDS_22E-1              0.000000000
    ## Bacillus_subtilis_BEST195                       0.118615298
    ## Enterobacter_aerogenes_CAV1320                  0.724827346
    ## Providencia_stuartii_ATCC_33672                 0.003639741
    ## Corynebacterium_callunae_DSM_20147              0.304094243
    ## Enterobacter_cloacae_CAV1411                    0.004184193

``` r
write.csv(summary_cbar_results, file = "/home/arredondo/Data/All_Genomes/summary_results_cbar.csv", 
    row.names = TRUE)
```

### Overall precision and recall

In this case we have considered the strain "Burkholderia cenocepacia strain 22E-1" because cBAR classified contigs as plasmids and thus corresponding to false positive results (assigned to the chromosome fraction).

``` r
overall_precision_recall_cbar <- summary_cbar_results
summary(overall_precision_recall_cbar)
```

    ##    Precision          Recall       Chromosome fraction Plasmid fraction
    ##  Min.   :0.0000   Min.   :0.0000   Min.   :0.03916     Min.   :0.0000  
    ##  1st Qu.:0.4837   1st Qu.:0.7411   1st Qu.:0.16707     1st Qu.:0.4550  
    ##  Median :0.6756   Median :0.8272   Median :0.28973     Median :0.6376  
    ##  Mean   :0.6218   Mean   :0.7576   Mean   :0.32832     Mean   :0.5822  
    ##  3rd Qu.:0.8114   3rd Qu.:0.9059   3rd Qu.:0.43452     3rd Qu.:0.7964  
    ##  Max.   :0.9608   Max.   :1.0000   Max.   :1.00000     Max.   :0.9608  
    ##  Novel sequences fraction
    ##  Min.   :0.000000        
    ##  1st Qu.:0.001273        
    ##  Median :0.013595        
    ##  Mean   :0.089473        
    ##  3rd Qu.:0.045977        
    ##  Max.   :0.724827

PlasmidFinder
-------------

``` r
data_plasmidfinder = data_plasmidfinder[c("Strain", "Length", 
    "Type", "Node")]
data_plasmidfinder <- data_plasmidfinder[order(data_plasmidfinder$Strain), 
    ]
data_plasmidfinder <- data_plasmidfinder[order(data_plasmidfinder$Type, 
    decreasing = TRUE), ]
head(data_plasmidfinder)
```

    ##                          Strain Length              Type
    ## 2      Aeromonas_veronii_AVNIH1  56551 Reference_plasmid
    ## 4     Bacillus_subtilis_BEST195   5947 Reference_plasmid
    ## 7  Citrobacter_freundii_CAV1321  11686 Reference_plasmid
    ## 8  Citrobacter_freundii_CAV1321  75582 Reference_plasmid
    ## 9  Citrobacter_freundii_CAV1321  56128 Reference_plasmid
    ## 10 Citrobacter_freundii_CAV1321  22371 Reference_plasmid
    ##                                Node
    ## 2  NODE_29_length_56551_cov_160.023
    ## 4   NODE_62_length_5947_cov_4530.17
    ## 7  NODE_39_length_11686_cov_55.8339
    ## 8  NODE_24_length_75582_cov_51.3513
    ## 9  NODE_25_length_56128_cov_83.0426
    ## 10 NODE_35_length_22371_cov_78.9859

``` r
data_not_duplicates <- data_plasmidfinder[!duplicated(data_plasmidfinder$Node), 
    ]
data_plasmidfinder <- data_plasmidfinder[order(data_plasmidfinder$Strain), 
    ]
data_not_duplicates <- data_not_duplicates[order(data_not_duplicates$Strain), 
    ]
vec_rate_chromosome <- NULL
vec_rate_plasmid <- NULL
vec_rate_unalignments <- NULL
vec_precision <- NULL
vec_total_predicted <- NULL
total_unalignments <- 0
total_ref_plasmids <- 0
total_chromosome <- 0
strains = unique(data_plasmidfinder$Strain)
for (i in strains) {
    plasmidfinder_certain_genome <- subset(data_not_duplicates, 
        data_not_duplicates$Strain == i)
    ref_plasmid <- sum(plasmidfinder_certain_genome[which(plasmidfinder_certain_genome[, 
        3] == "Reference_plasmid"), 2])
    chromosome <- sum(plasmidfinder_certain_genome[which(plasmidfinder_certain_genome[, 
        3] == "Chromosome"), 2])
    new_alignments <- sum(plasmidfinder_certain_genome[which(plasmidfinder_certain_genome[, 
        3] == "New_plasmids"), 2])
    rate_plasmid <- ref_plasmid/(ref_plasmid + chromosome + new_alignments)
    rate_chromosome <- chromosome/(ref_plasmid + chromosome + 
        new_alignments)
    rate_unalignments <- new_alignments/(ref_plasmid + chromosome + 
        new_alignments)
    total_predicted <- ref_plasmid + chromosome + new_alignments
    total_unalignments <- total_unalignments + new_alignments
    total_ref_plasmids <- total_ref_plasmids + ref_plasmid
    total_chromosome <- total_chromosome + chromosome
    vec_rate_plasmid <- append(vec_rate_plasmid, rate_plasmid, 
        length(vec_rate_plasmid))
    vec_rate_chromosome <- append(vec_rate_chromosome, rate_chromosome, 
        length(vec_rate_chromosome))
    vec_rate_unalignments <- append(vec_rate_unalignments, rate_unalignments, 
        length(vec_rate_unalignments))
    precision <- ref_plasmid/(ref_plasmid + chromosome)
    vec_precision <- append(vec_precision, precision, length(vec_precision))
    vec_total_predicted <- append(vec_total_predicted, total_predicted, 
        length(vec_total_predicted))
}
# vec_precision <- vec_precision*100
vec_total_predicted <- vec_total_predicted/1000
vec_total_predicted <- round(vec_total_predicted, digits = 1)
dataframe_plot_plasmidfinder <- data.frame(vec_rate_chromosome, 
    vec_rate_plasmid, vec_rate_unalignments)
colnames(dataframe_plot_plasmidfinder) <- c("Chromosome", "Ref_plasmid", 
    "New")
rownames(dataframe_plot_plasmidfinder) <- strains
dataframe_plot_plasmidfinder <- t(dataframe_plot_plasmidfinder)
par(las = 2)
dataframe_plot_plasmidfinder <- prop.table(dataframe_plot_plasmidfinder, 
    2)
dataframe_plot_plasmidfinder = dataframe_plot_plasmidfinder[, 
    number_order_reference$Strain]
df_total_predicted_plasmidfinder = data.frame(vec_total_predicted)
df_total_predicted_plasmidfinder = data.frame(vec_total_predicted)
df_total_predicted_plasmidfinder = cbind(df_total_predicted_plasmidfinder, 
    alpha_order_reference$Strain)
df_total_predicted_plasmidfinder = df_total_predicted_plasmidfinder[number_order_reference$Strain, 
    ]
```

Plot the frequencies of PlasmidFinder

``` r
par(mfrow = c(2, 1))
par(mar = c(0.1, 4.1, 1.1, 5.1), xpd = TRUE)
par(las = 2)
plot_plasmidfinder <- barplot(dataframe_plot_plasmidfinder, horiz = FALSE, 
    col = c("white", "lightgreen", "purple3"), legend.text = FALSE, 
    args.legend = list(x = "topright", inset = c(-0.8, 0), cex = 0.7), 
    axes = FALSE, ylab = "PlasmidFinder")
axis(2, ylim = c(0, 1), col = "black", las = 1)
par(new = TRUE)
plot_length_plasmidfinder = plot(rep(0.5:num_max, 1), df_total_predicted_plasmidfinder$vec_total_predicted, 
    axes = F, type = "b", pch = 20, xlab = "", ylim = c(0, max(df_total_predicted_plasmidfinder$vec_total_predicted)), 
    ylab = "", col = "blue", xlim = c(0, length(strains)))
axis(4, ylim = c(min(df_total_predicted_plasmidfinder$vec_total_predicted, 
    max(df_total_predicted_plasmidfinder$vec_total_predicted))), 
    col = "blue", las = 1, col.axis = "blue")
mtext("", side = 4, line = 4, cex = 0.9, col = "blue", las = 0)
```

![](detailed_analysis_files/figure-markdown_github/frequencies_plasmidfinder-1.png)

Now we load the file with the recall values of each bacterial isolate

``` r
data_plasmidfinder_recall <- data_plasmidfinder_recall[order(data_plasmidfinder_recall$Strain), 
    ]
data_plasmidfinder_recall[is.na(data_plasmidfinder_recall)] <- 0
vec_recall = data_plasmidfinder_recall$Recall
vec_recall <- vec_recall/100
dataframe_both_plasmidfinder = data.frame(vec_precision, vec_recall)
colnames(dataframe_both_plasmidfinder) <- c("Precision", "Recall")
rownames(dataframe_both_plasmidfinder) <- strains
dataframe_both_plasmidfinder <- t(dataframe_both_plasmidfinder)
dataframe_both_plasmidfinder = dataframe_both_plasmidfinder[, 
    number_order_reference$Strain]
par(mfrow = c(2, 1))
par(mar = c(0.1, 4.1, 1.1, 5.1), xpd = TRUE)
par(las = 2)
plot_precision_recall_plasmidfinder <- barplot(dataframe_both_plasmidfinder, 
    horiz = FALSE, col = c("azure2", "dimgrey"), ylab = "PlasmidFinder", 
    legend.text = FALSE, beside = TRUE, args.legend = list(x = "topright", 
        inset = c(-0.8, 0), cex = 0.7), ylim = c(0, 1))
axis(2, ylim = c(0, 1), col = "black", las = 1)
par(new = TRUE)
plot_length_reference_plasmids <- plot(rep(0.5:num_max, 1), number_order_reference$Length, 
    axes = F, type = "b", pch = 20, xlab = "", ylim = c(0, max(number_order_reference$Length)), 
    ylab = "", col = "darkred", xlim = c(0, length(strains)))
axis(4, ylim = c(min(number_order_reference$Length, max(number_order_reference$Length))), 
    col = "darkred", las = 1, col.axis = "darkred")
mtext("", side = 4, line = 4, cex = 0.9, col = "darkred", las = 0)
```

![](detailed_analysis_files/figure-markdown_github/precision_recall_plasmidfinder-1.png)

### Summary of the results

We save the dataframe containning the results from PlasmidFinder

``` r
precision_recall_plasmidfinder_results <- t(dataframe_both_plasmidfinder)
fractions_plasmidfinder_results <- t(dataframe_plot_plasmidfinder)
summary_plasmidfinder_results <- cbind(precision_recall_plasmidfinder_results, 
    fractions_plasmidfinder_results[, c(1, 2, 3)])
colnames(summary_plasmidfinder_results) <- c("Precision", "Recall", 
    "Chromosome fraction", "Plasmid fraction", "Novel sequences fraction")
head(summary_plasmidfinder_results)
```

    ##                                    Precision  Recall Chromosome fraction
    ## Burkholderia_cenocepacia_DDS_22E-1       NaN 0.00000                 NaN
    ## Bacillus_subtilis_BEST195                  1 1.00000                   0
    ## Enterobacter_aerogenes_CAV1320             1 1.00000                   0
    ## Providencia_stuartii_ATCC_33672          NaN 0.00000                 NaN
    ## Corynebacterium_callunae_DSM_20147       NaN 0.00000                 NaN
    ## Enterobacter_cloacae_CAV1411               1 0.40679                   0
    ##                                    Plasmid fraction
    ## Burkholderia_cenocepacia_DDS_22E-1              NaN
    ## Bacillus_subtilis_BEST195                 0.9223015
    ## Enterobacter_aerogenes_CAV1320            1.0000000
    ## Providencia_stuartii_ATCC_33672                 NaN
    ## Corynebacterium_callunae_DSM_20147              NaN
    ## Enterobacter_cloacae_CAV1411              1.0000000
    ##                                    Novel sequences fraction
    ## Burkholderia_cenocepacia_DDS_22E-1                      NaN
    ## Bacillus_subtilis_BEST195                        0.07769851
    ## Enterobacter_aerogenes_CAV1320                   0.00000000
    ## Providencia_stuartii_ATCC_33672                         NaN
    ## Corynebacterium_callunae_DSM_20147                      NaN
    ## Enterobacter_cloacae_CAV1411                     0.00000000

``` r
write.csv(summary_plasmidfinder_results, file = "/home/arredondo/Data/All_Genomes/summary_results_plasmidfinder.csv", 
    row.names = TRUE)
```

### Overall recall and precision

This program was designed for the plasmid assembly and typing in Enterobacteriaceae. For this reason we have decided to exclude from the metrics calculation all the samples corresponding to Gram-positive bacteria and our negative control.

``` r
excluded_strains = c("Burkholderia_cenocepacia_DDS_22E-1", "Corynebacterium_callunae_DSM_20147", 
    "Enterococcus_faecium_700221", "Providencia_stuartii_ATCC_33672")
overall_precision_recall_plasmidfinder <- summary_plasmidfinder_results[!row.names(summary_plasmidfinder_results) %in% 
    excluded_strains, ]
summary(overall_precision_recall_plasmidfinder)
```

    ##    Precision     Recall       Chromosome fraction Plasmid fraction
    ##  Min.   :1   Min.   :0.0000   Min.   :0           Min.   :0.9223  
    ##  1st Qu.:1   1st Qu.:0.2219   1st Qu.:0           1st Qu.:1.0000  
    ##  Median :1   Median :0.3230   Median :0           Median :1.0000  
    ##  Mean   :1   Mean   :0.3580   Mean   :0           Mean   :0.9962  
    ##  3rd Qu.:1   3rd Qu.:0.4343   3rd Qu.:0           3rd Qu.:1.0000  
    ##  Max.   :1   Max.   :1.0000   Max.   :0           Max.   :1.0000  
    ##  NA's   :1                    NA's   :1           NA's   :1       
    ##  Novel sequences fraction
    ##  Min.   :0.000000        
    ##  1st Qu.:0.000000        
    ##  Median :0.000000        
    ##  Mean   :0.003841        
    ##  3rd Qu.:0.000000        
    ##  Max.   :0.077699        
    ##  NA's   :1

Visualization
-------------

Finally we can replot all the figures in the same mark to obtain a better overview of the predictions.

### Plotting the frequencies

![](detailed_analysis_files/figure-markdown_github/all_figures_frequency-1.png)

### Plotting recall

![](detailed_analysis_files/figure-markdown_github/all_figures_precision_recall-1.png)
