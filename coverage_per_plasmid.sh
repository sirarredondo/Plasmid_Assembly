#!/bin/bash
# loop trough results and run quast on each genome/plasmid prediction pair
# 
# for h in plasmidfinder cbar recycler spades
#  do
#   cd results$h
#   for i in *
#    do 
#     for j in ~/completegenomes/plasmids/$i*/group*fasta
#     do
#      echo  $h : $i : $j
#      python ~/programmes/quast-4.1/quast.py $i/prediction.fas \
#        -R $j \
#        -o "$j"_comparison_$h  \
#        --min-alignment 500 \
#        --ambiguity-usage all
#     done
#    done
#   cd 
#  done
# 
# 
# extract results from quast

echo -e "Name:Size:PlasmidFinder:cBAR:Recycler:PlasmidSPAdes" >  ~/Analysis/All_Genomes/Benchmark_perPlasmid.csv
for i in ~/completegenomes/plasmids/*_references/group_*.fasta 
 do 
  echo -n $(head -1 $i)
  echo -en ":"
  echo -n $(grep -v ">" $i | wc | awk '{print $3-$1}')
  for h in plasmidfinder cbar recycler spades
   do 
    echo -en ":";
    echo -n $(grep "Genome fraction" "$i"_comparison_$h/report.tsv |cut -f 2,2); 
   done
   echo ""
 done >> ~/Analysis/All_Genomes/Benchmark_perPlasmid.csv



