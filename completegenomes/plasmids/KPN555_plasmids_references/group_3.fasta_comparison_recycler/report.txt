All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                    prediction
# contigs (>= 0 bp)         7         
# contigs (>= 1000 bp)      7         
# contigs (>= 5000 bp)      2         
# contigs (>= 10000 bp)     2         
# contigs (>= 25000 bp)     1         
# contigs (>= 50000 bp)     1         
Total length (>= 0 bp)      257779    
Total length (>= 1000 bp)   257779    
Total length (>= 5000 bp)   241173    
Total length (>= 10000 bp)  241173    
Total length (>= 25000 bp)  229765    
Total length (>= 50000 bp)  229765    
# contigs                   7         
Largest contig              229765    
Total length                257779    
Reference length            26450     
GC (%)                      57.16     
Reference GC (%)            59.91     
N50                         229765    
NG50                        229765    
N75                         229765    
NG75                        229765    
L50                         1         
LG50                        1         
L75                         1         
LG75                        1         
# unaligned contigs         7 + 0 part
Unaligned length            257779    
# N's per 100 kbp           0.00      
NGA50                       -         
