reference chromosomes:
	gi_828939549_gb_CP011610.1__Citrobacter_freundii_strain_CAV1321_plasmid_pCAV1321-135__complete_sequence (total length: 135117 bp, maximal covered length: 133537 bp)

total genome size: 135117

gap min size: 50
partial gene/operon min size: 100



assembly                 | genome    | duplication | gaps      | genes     | partial   | operons   | partial   |
                         | fraction  | ratio       | number    |           | genes     |           | operons   |
================================================================================================================
prediction               | 98.83064% | 1.01426     | 3         | -         | -         | -         | -         |
