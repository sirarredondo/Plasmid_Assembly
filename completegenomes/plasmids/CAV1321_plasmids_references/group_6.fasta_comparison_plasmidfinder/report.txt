All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                     prediction
# contigs (>= 0 bp)          2         
# contigs (>= 1000 bp)       2         
# contigs (>= 5000 bp)       2         
# contigs (>= 10000 bp)      2         
# contigs (>= 25000 bp)      1         
# contigs (>= 50000 bp)      1         
Total length (>= 0 bp)       78499     
Total length (>= 1000 bp)    78499     
Total length (>= 5000 bp)    78499     
Total length (>= 10000 bp)   78499     
Total length (>= 25000 bp)   56128     
Total length (>= 50000 bp)   56128     
# contigs                    2         
Largest contig               56128     
Total length                 78499     
Reference length             44846     
GC (%)                       49.28     
Reference GC (%)             48.42     
N50                          56128     
NG50                         56128     
N75                          22371     
NG75                         56128     
L50                          1         
LG50                         1         
L75                          2         
LG75                         1         
# misassemblies              0         
# misassembled contigs       0         
Misassembled contigs length  0         
# local misassemblies        0         
# unaligned contigs          1 + 0 part
Unaligned length             56128     
Genome fraction (%)          49.884    
Duplication ratio            1.000     
# N's per 100 kbp            0.00      
# mismatches per 100 kbp     0.00      
# indels per 100 kbp         0.00      
Largest alignment            22371     
NGA50                        -         
