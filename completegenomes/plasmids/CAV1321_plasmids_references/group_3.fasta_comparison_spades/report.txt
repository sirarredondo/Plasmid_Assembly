All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                     prediction 
# contigs (>= 0 bp)          62         
# contigs (>= 1000 bp)       26         
# contigs (>= 5000 bp)       13         
# contigs (>= 10000 bp)      9          
# contigs (>= 25000 bp)      5          
# contigs (>= 50000 bp)      3          
Total length (>= 0 bp)       479014     
Total length (>= 1000 bp)    468500     
Total length (>= 5000 bp)    432446     
Total length (>= 10000 bp)   402854     
Total length (>= 25000 bp)   342426     
Total length (>= 50000 bp)   277673     
# contigs                    33         
Largest contig               144675     
Total length                 473318     
Reference length             3820       
GC (%)                       49.70      
Reference GC (%)             52.09      
N50                          56128      
NG50                         144675     
N75                          22010      
NG75                         144675     
L50                          3          
LG50                         1          
L75                          6          
LG75                         1          
# misassemblies              0          
# misassembled contigs       0          
Misassembled contigs length  0          
# local misassemblies        0          
# unaligned contigs          31 + 0 part
Unaligned length             469561     
Genome fraction (%)          96.335     
Duplication ratio            1.021      
# N's per 100 kbp            48.17      
# mismatches per 100 kbp     0.00       
# indels per 100 kbp         0.00       
Largest alignment            3256       
NGA50                        3256       
NGA75                        3256       
LGA50                        1          
LGA75                        1          
