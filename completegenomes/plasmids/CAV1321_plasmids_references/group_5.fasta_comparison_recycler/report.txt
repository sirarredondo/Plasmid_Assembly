All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                    prediction
# contigs (>= 0 bp)         2         
# contigs (>= 1000 bp)      2         
# contigs (>= 5000 bp)      1         
# contigs (>= 10000 bp)     1         
# contigs (>= 25000 bp)     1         
# contigs (>= 50000 bp)     1         
Total length (>= 0 bp)      180835    
Total length (>= 1000 bp)   180835    
Total length (>= 5000 bp)   179532    
Total length (>= 10000 bp)  179532    
Total length (>= 25000 bp)  179532    
Total length (>= 50000 bp)  179532    
# contigs                   2         
Largest contig              179532    
Total length                180835    
Reference length            4938      
GC (%)                      51.46     
Reference GC (%)            51.42     
N50                         179532    
NG50                        179532    
N75                         179532    
NG75                        179532    
L50                         1         
LG50                        1         
L75                         1         
LG75                        1         
# unaligned contigs         2 + 0 part
Unaligned length            180835    
# N's per 100 kbp           0.00      
NGA50                       -         
