reference chromosomes:
	gi_828939390_gb_CP011605.1__Citrobacter_freundii_strain_CAV1321_plasmid_pCAV1321-3820__complete_sequence (total length: 3820 bp, maximal covered length: 3680 bp)

total genome size: 3820

gap min size: 50
partial gene/operon min size: 100



assembly                 | genome    | duplication | gaps      | genes     | partial   | operons   | partial   |
                         | fraction  | ratio       | number    |           | genes     |           | operons   |
================================================================================================================
prediction               | 96.33508% | 1.02092     | 1         | -         | -         | -         | -         |
