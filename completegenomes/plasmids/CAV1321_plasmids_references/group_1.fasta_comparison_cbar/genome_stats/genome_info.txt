reference chromosomes:
	gi_828939380_gb_CP011603.1__Citrobacter_freundii_strain_CAV1321_plasmid_pCAV1321-1916__complete_sequence (total length: 1916 bp, maximal covered length: 1814 bp)

total genome size: 1916

gap min size: 50
partial gene/operon min size: 100



assembly                 | genome    | duplication | gaps      | genes     | partial   | operons   | partial   |
                         | fraction  | ratio       | number    |           | genes     |           | operons   |
================================================================================================================
prediction               | 94.67641% | 1.00000     | 1         | -         | -         | -         | -         |
