All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                     prediction 
# contigs (>= 0 bp)          140        
# contigs (>= 1000 bp)       52         
# contigs (>= 5000 bp)       27         
# contigs (>= 10000 bp)      16         
# contigs (>= 25000 bp)      7          
# contigs (>= 50000 bp)      3          
Total length (>= 0 bp)       685273     
Total length (>= 1000 bp)    663344     
Total length (>= 5000 bp)    606838     
Total length (>= 10000 bp)   536777     
Total length (>= 25000 bp)   370312     
Total length (>= 50000 bp)   217736     
# contigs                    63         
Largest contig               111748     
Total length                 671416     
Reference length             69947      
GC (%)                       49.26      
Reference GC (%)             53.97      
N50                          34986      
NG50                         111748     
N75                          14902      
NG75                         111748     
L50                          6          
LG50                         1          
L75                          14         
LG75                         1          
# misassemblies              0          
# misassembled contigs       0          
Misassembled contigs length  0          
# local misassemblies        0          
# unaligned contigs          47 + 2 part
Unaligned length             605979     
Genome fraction (%)          93.835     
Duplication ratio            1.008      
# N's per 100 kbp            53.77      
# mismatches per 100 kbp     126.46     
# indels per 100 kbp         13.71      
Largest alignment            20973      
NGA50                        16754      
NGA75                        3216       
LGA50                        2          
LGA75                        5          
