reference chromosomes:
	gi_749295880_ref_NZ_CP008930.1__Klebsiella_pneumoniae_strain_PMK1_plasmid_pPMK1-A__complete_sequence (total length: 187571 bp, maximal covered length: 144088 bp)

total genome size: 187571

gap min size: 50
partial gene/operon min size: 100



assembly                 | genome    | duplication | gaps      | genes     | partial   | operons   | partial   |
                         | fraction  | ratio       | number    |           | genes     |           | operons   |
================================================================================================================
PMK1_cbar_recall         | 76.81784% | 1.00624     | 11        | -         | -         | -         | -         |
