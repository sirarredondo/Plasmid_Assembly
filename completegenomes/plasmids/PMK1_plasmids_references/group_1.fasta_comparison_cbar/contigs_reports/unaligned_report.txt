All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                          PMK1_cbar_recall
# fully unaligned contigs         24              
Fully unaligned length            359830          
# partially unaligned contigs     3               
    # with misassembly            0               
    # both parts are significant  3               
Partially unaligned length        9341            
# N's                             0               
