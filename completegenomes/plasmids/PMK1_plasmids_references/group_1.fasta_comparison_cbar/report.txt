All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                     PMK1_cbar_recall
# contigs (>= 0 bp)          47              
# contigs (>= 1000 bp)       40              
# contigs (>= 5000 bp)       19              
# contigs (>= 10000 bp)      12              
# contigs (>= 25000 bp)      5               
# contigs (>= 50000 bp)      2               
Total length (>= 0 bp)       511316          
Total length (>= 1000 bp)    506088          
Total length (>= 5000 bp)    460101          
Total length (>= 10000 bp)   411088          
Total length (>= 25000 bp)   277945          
Total length (>= 50000 bp)   162481          
# contigs                    47              
Largest contig               111748          
Total length                 511316          
Reference length             187571          
GC (%)                       49.38           
Reference GC (%)             52.46           
N50                          31494           
NG50                         111748          
N75                          16754           
NG75                         50733           
L50                          5               
LG50                         1               
L75                          11              
LG75                         2               
# misassemblies              1               
# misassembled contigs       1               
Misassembled contigs length  1302            
# local misassemblies        0               
# unaligned contigs          24 + 3 part     
Unaligned length             369171          
Genome fraction (%)          76.818          
Duplication ratio            1.006           
# N's per 100 kbp            0.00            
# mismatches per 100 kbp     79.12           
# indels per 100 kbp         5.55            
Largest alignment            48984           
NGA50                        12641           
NGA75                        755             
LGA50                        3               
LGA75                        21              
