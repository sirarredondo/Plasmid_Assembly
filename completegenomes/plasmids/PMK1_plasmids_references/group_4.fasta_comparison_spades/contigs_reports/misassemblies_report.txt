All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                     prediction
# misassemblies              1         
    # relocations            1         
    # translocations         0         
    # inversions             0         
# misassembled contigs       1         
Misassembled contigs length  14902     
# local misassemblies        2         
# mismatches                 124       
# indels                     4         
    # short indels           3         
    # long indels            1         
Indels length                69        
