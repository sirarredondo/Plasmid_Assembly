All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                     prediction
# contigs (>= 0 bp)          20        
# contigs (>= 1000 bp)       17        
# contigs (>= 5000 bp)       9         
# contigs (>= 10000 bp)      4         
# contigs (>= 25000 bp)      2         
# contigs (>= 50000 bp)      0         
Total length (>= 0 bp)       139851    
Total length (>= 1000 bp)    137375    
Total length (>= 5000 bp)    123428    
Total length (>= 10000 bp)   87914     
Total length (>= 25000 bp)   64291     
Total length (>= 50000 bp)   0         
# contigs                    20        
Largest contig               38038     
Total length                 139851    
Reference length             170926    
GC (%)                       50.62     
Reference GC (%)             50.58     
N50                          12064     
NG50                         11559     
N75                          8767      
NG75                         2071      
L50                          3         
LG50                         4         
L75                          6         
LG75                         11        
# misassemblies              0         
# misassembled contigs       0         
Misassembled contigs length  0         
# local misassemblies        0         
# unaligned contigs          0 + 2 part
Unaligned length             259       
Genome fraction (%)          81.844    
Duplication ratio            1.011     
# N's per 100 kbp            0.00      
# mismatches per 100 kbp     35.03     
# indels per 100 kbp         2.86      
Largest alignment            38038     
NA50                         12064     
NGA50                        11559     
NA75                         8767      
NGA75                        2071      
LA50                         3         
LGA50                        4         
LA75                         6         
LGA75                        11        
