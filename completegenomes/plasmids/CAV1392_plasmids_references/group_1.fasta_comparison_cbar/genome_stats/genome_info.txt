reference chromosomes:
	gi_828905883_gb_CP011575.1__Klebsiella_pneumoniae_strain_CAV1392_plasmid_pKPC_CAV1392__complete_sequence (total length: 43621 bp, maximal covered length: 43621 bp)

total genome size: 43621

gap min size: 50
partial gene/operon min size: 100



assembly                 | genome    | duplication | gaps      | genes     | partial   | operons   | partial   |
                         | fraction  | ratio       | number    |           | genes     |           | operons   |
================================================================================================================
prediction               | 100.00000%| 1.00177     | 0         | -         | -         | -         | -         |
