reference chromosomes:
	gi_828905932_gb_CP011576.1__Klebsiella_pneumoniae_strain_CAV1392_plasmid_pCAV1392-50__complete_sequence (total length: 49832 bp, maximal covered length: 49832 bp)

total genome size: 49832

gap min size: 50
partial gene/operon min size: 100



assembly                 | genome    | duplication | gaps      | genes     | partial   | operons   | partial   |
                         | fraction  | ratio       | number    |           | genes     |           | operons   |
================================================================================================================
prediction               | 100.00000%| 1.03319     | 0         | -         | -         | -         | -         |
