reference chromosomes:
	gi_828905996_gb_CP011577.1__Klebsiella_pneumoniae_strain_CAV1392_plasmid_pCAV1392-131__complete_sequence (total length: 130719 bp, maximal covered length: 114692 bp)

total genome size: 130719

gap min size: 50
partial gene/operon min size: 100



assembly                 | genome    | duplication | gaps      | genes     | partial   | operons   | partial   |
                         | fraction  | ratio       | number    |           | genes     |           | operons   |
================================================================================================================
prediction               | 87.73935% | 1.00344     | 3         | -         | -         | -         | -         |
