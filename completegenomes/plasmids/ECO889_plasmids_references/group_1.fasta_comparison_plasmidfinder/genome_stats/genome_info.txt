reference chromosomes:
	gi_1039515083_ref_NZ_CP015160.1__Escherichia_coli_strain_Eco889_plasmid_pECO-fce__complete_sequence (total length: 212180 bp, maximal covered length: 54143 bp)

total genome size: 212180

gap min size: 50
partial gene/operon min size: 100



assembly                 | genome    | duplication | gaps      | genes     | partial   | operons   | partial   |
                         | fraction  | ratio       | number    |           | genes     |           | operons   |
================================================================================================================
prediction               | 25.51749% | 1.01332     | 4         | -         | -         | -         | -         |
