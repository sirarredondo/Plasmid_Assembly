All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                    prediction
# contigs (>= 0 bp)         5         
# contigs (>= 1000 bp)      5         
# contigs (>= 5000 bp)      2         
# contigs (>= 10000 bp)     1         
# contigs (>= 25000 bp)     0         
# contigs (>= 50000 bp)     0         
Total length (>= 0 bp)      39401     
Total length (>= 1000 bp)   39401     
Total length (>= 5000 bp)   27738     
Total length (>= 10000 bp)  18285     
Total length (>= 25000 bp)  0         
Total length (>= 50000 bp)  0         
# contigs                   5         
Largest contig              18285     
Total length                39401     
Reference length            77808     
GC (%)                      52.52     
Reference GC (%)            53.33     
N50                         9453      
NG50                        3087      
N75                         4835      
L50                         2         
LG50                        5         
L75                         3         
# unaligned contigs         5 + 0 part
Unaligned length            39401     
# N's per 100 kbp           0.00      
NGA50                       -         
