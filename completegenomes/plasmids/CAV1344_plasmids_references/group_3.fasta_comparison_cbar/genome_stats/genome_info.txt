reference chromosomes:
	gi_828952208_gb_CP011621.1__Klebsiella_pneumoniae_strain_CAV1344_plasmid_pCAV1344-78__complete_sequence (total length: 77808 bp, maximal covered length: 70129 bp)

total genome size: 77808

gap min size: 50
partial gene/operon min size: 100



assembly                 | genome    | duplication | gaps      | genes     | partial   | operons   | partial   |
                         | fraction  | ratio       | number    |           | genes     |           | operons   |
================================================================================================================
prediction               | 90.13083% | 1.03900     | 4         | -         | -         | -         | -         |
