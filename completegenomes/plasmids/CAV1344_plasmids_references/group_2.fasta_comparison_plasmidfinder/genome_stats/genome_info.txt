reference chromosomes:
	gi_828952164_gb_CP011620.1__Klebsiella_pneumoniae_strain_CAV1344_plasmid_pCAV1344-40__complete_sequence (total length: 39554 bp, maximal covered length: 30893 bp)

total genome size: 39554

gap min size: 50
partial gene/operon min size: 100



assembly                 | genome    | duplication | gaps      | genes     | partial   | operons   | partial   |
                         | fraction  | ratio       | number    |           | genes     |           | operons   |
================================================================================================================
prediction               | 78.10335% | 1.00000     | 1         | -         | -         | -         | -         |
