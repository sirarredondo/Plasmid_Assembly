reference chromosomes:
	gi_828952302_gb_CP011622.1__Klebsiella_pneumoniae_strain_CAV1344_plasmid_pKPC_CAV1344__complete_sequence (total length: 176497 bp, maximal covered length: 176497 bp)

total genome size: 176497

gap min size: 50
partial gene/operon min size: 100



assembly                 | genome    | duplication | gaps      | genes     | partial   | operons   | partial   |
                         | fraction  | ratio       | number    |           | genes     |           | operons   |
================================================================================================================
prediction               | 100.00000%| 1.03438     | 0         | -         | -         | -         | -         |
