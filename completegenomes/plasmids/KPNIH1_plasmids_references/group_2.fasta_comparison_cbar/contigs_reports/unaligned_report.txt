All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                          prediction
# fully unaligned contigs         7         
Fully unaligned length            55604     
# partially unaligned contigs     7         
    # with misassembly            1         
    # both parts are significant  5         
Partially unaligned length        31137     
# N's                             0         
