reference chromosomes:
	gi_749295864_ref_NZ_CP008829.1__Klebsiella_pneumoniae_subsp._pneumoniae_KPNIH1_plasmid_pKPN-498__complete_sequence (total length: 243824 bp, maximal covered length: 232815 bp)

total genome size: 243824

gap min size: 50
partial gene/operon min size: 100



assembly                 | genome    | duplication | gaps      | genes     | partial   | operons   | partial   |
                         | fraction  | ratio       | number    |           | genes     |           | operons   |
================================================================================================================
prediction               | 95.48486% | 1.05982     | 15        | -         | -         | -         | -         |
