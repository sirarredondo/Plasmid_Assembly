All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                     prediction 
# contigs (>= 0 bp)          119        
# contigs (>= 1000 bp)       49         
# contigs (>= 5000 bp)       20         
# contigs (>= 10000 bp)      10         
# contigs (>= 25000 bp)      3          
# contigs (>= 50000 bp)      1          
Total length (>= 0 bp)       378890     
Total length (>= 1000 bp)    352941     
Total length (>= 5000 bp)    297190     
Total length (>= 10000 bp)   237193     
Total length (>= 25000 bp)   136276     
Total length (>= 50000 bp)   56873      
# contigs                    64         
Largest contig               56873      
Total length                 364377     
Reference length             243824     
GC (%)                       52.87      
Reference GC (%)             53.22      
N50                          13083      
NG50                         30230      
N75                          5541       
NG75                         13083      
L50                          6          
LG50                         3          
L75                          16         
LG75                         6          
# misassemblies              1          
# misassembled contigs       1          
Misassembled contigs length  5517       
# local misassemblies        1          
# unaligned contigs          19 + 8 part
Unaligned length             119891     
Genome fraction (%)          95.485     
Duplication ratio            1.060      
# N's per 100 kbp            51.32      
# mismatches per 100 kbp     79.89      
# indels per 100 kbp         3.01       
Largest alignment            56873      
NA50                         6585       
NGA50                        30230      
NGA75                        6585       
LA50                         9          
LGA50                        3          
LGA75                        9          
