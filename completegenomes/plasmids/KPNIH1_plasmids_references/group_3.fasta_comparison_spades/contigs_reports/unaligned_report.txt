All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                          prediction
# fully unaligned contigs         28        
Fully unaligned length            168879    
# partially unaligned contigs     7         
    # with misassembly            1         
    # both parts are significant  7         
Partially unaligned length        75401     
# N's                             187       
