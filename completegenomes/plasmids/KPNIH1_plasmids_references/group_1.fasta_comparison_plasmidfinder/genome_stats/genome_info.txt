reference chromosomes:
	gi_749295863_ref_NZ_CP008828.1__Klebsiella_pneumoniae_subsp._pneumoniae_KPNIH1_plasmid_pAAC154-a50__complete_sequence (total length: 15096 bp, maximal covered length: 12714 bp)

total genome size: 15096

gap min size: 50
partial gene/operon min size: 100



assembly                 | genome    | duplication | gaps      | genes     | partial   | operons   | partial   |
                         | fraction  | ratio       | number    |           | genes     |           | operons   |
================================================================================================================
prediction               | 84.22099% | 1.00000     | 1         | -         | -         | -         | -         |
