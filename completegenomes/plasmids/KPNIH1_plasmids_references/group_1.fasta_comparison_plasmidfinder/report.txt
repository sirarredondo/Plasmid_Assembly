All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                     prediction
# contigs (>= 0 bp)          5         
# contigs (>= 1000 bp)       5         
# contigs (>= 5000 bp)       5         
# contigs (>= 10000 bp)      3         
# contigs (>= 25000 bp)      1         
# contigs (>= 50000 bp)      0         
Total length (>= 0 bp)       75264     
Total length (>= 1000 bp)    75264     
Total length (>= 5000 bp)    75264     
Total length (>= 10000 bp)   62349     
Total length (>= 25000 bp)   25511     
Total length (>= 50000 bp)   0         
# contigs                    5         
Largest contig               25511     
Total length                 75264     
Reference length             15096     
GC (%)                       52.69     
Reference GC (%)             53.54     
N50                          24124     
NG50                         25511     
N75                          12714     
NG75                         25511     
L50                          2         
LG50                         1         
L75                          3         
LG75                         1         
# misassemblies              0         
# misassembled contigs       0         
Misassembled contigs length  0         
# local misassemblies        0         
# unaligned contigs          4 + 0 part
Unaligned length             62550     
Genome fraction (%)          84.221    
Duplication ratio            1.000     
# N's per 100 kbp            0.00      
# mismatches per 100 kbp     0.00      
# indels per 100 kbp         0.00      
Largest alignment            12714     
NGA50                        12714     
NGA75                        12714     
LGA50                        1         
LGA75                        1         
