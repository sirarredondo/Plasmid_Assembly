reference chromosomes:
	gi_828977532_gb_CP011648.1__Enterobacter_cloacae_strain_CAV1669_plasmid_pCAV1669-34__complete_sequence (total length: 33610 bp, maximal covered length: 33383 bp)

total genome size: 33610

gap min size: 50
partial gene/operon min size: 100



assembly                 | genome    | duplication | gaps      | genes     | partial   | operons   | partial   |
                         | fraction  | ratio       | number    |           | genes     |           | operons   |
================================================================================================================
prediction               | 99.32461% | 1.00485     | 1         | -         | -         | -         | -         |
