All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                     prediction  
# contigs (>= 0 bp)          582         
# contigs (>= 1000 bp)       98          
# contigs (>= 5000 bp)       45          
# contigs (>= 10000 bp)      27          
# contigs (>= 25000 bp)      8           
# contigs (>= 50000 bp)      6           
Total length (>= 0 bp)       1246243     
Total length (>= 1000 bp)    1127239     
Total length (>= 5000 bp)    1018752     
Total length (>= 10000 bp)   886475      
Total length (>= 25000 bp)   549135      
Total length (>= 50000 bp)   491687      
# contigs                    133         
Largest contig               135428      
Total length                 1151904     
Reference length             39138       
GC (%)                       37.42       
Reference GC (%)             35.54       
N50                          23850       
NG50                         135428      
N75                          11196       
NG75                         135428      
L50                          10          
LG50                         1           
L75                          25          
LG75                         1           
# misassemblies              0           
# misassembled contigs       0           
Misassembled contigs length  0           
# local misassemblies        0           
# unaligned contigs          119 + 4 part
Unaligned length             1109804     
Genome fraction (%)          95.148      
Duplication ratio            1.131       
# N's per 100 kbp            0.00        
# mismatches per 100 kbp     51.02       
# indels per 100 kbp         45.65       
Largest alignment            10698       
NGA50                        9104        
NGA75                        5424        
LGA50                        2           
LGA75                        4           
