reference chromosomes:
	gi_1008423628_gb_CP014451.1__Enterococcus_faecium_strain_ATCC_700221_plasmid_unnamed2__complete_sequence (total length: 63692 bp, maximal covered length: 63539 bp)

total genome size: 63692

gap min size: 50
partial gene/operon min size: 100



assembly                 | genome    | duplication | gaps      | genes     | partial   | operons   | partial   |
                         | fraction  | ratio       | number    |           | genes     |           | operons   |
================================================================================================================
prediction               | 99.75978% | 1.00619     | 1         | -         | -         | -         | -         |
