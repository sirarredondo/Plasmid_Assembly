reference chromosomes:
	gi_749296012_ref_NZ_CP008800.1__Klebsiella_pneumoniae_subsp._pneumoniae_KPNIH24_plasmid_pKPN-e44__complete_sequence (total length: 194877 bp, maximal covered length: 149594 bp)

total genome size: 194877

gap min size: 50
partial gene/operon min size: 100



assembly                 | genome    | duplication | gaps      | genes     | partial   | operons   | partial   |
                         | fraction  | ratio       | number    |           | genes     |           | operons   |
================================================================================================================
prediction               | 76.76329% | 1.05822     | 9         | -         | -         | -         | -         |
