All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                     prediction
# contigs (>= 0 bp)          8         
# contigs (>= 1000 bp)       8         
# contigs (>= 5000 bp)       7         
# contigs (>= 10000 bp)      5         
# contigs (>= 25000 bp)      1         
# contigs (>= 50000 bp)      1         
Total length (>= 0 bp)       148186    
Total length (>= 1000 bp)    148186    
Total length (>= 5000 bp)    145954    
Total length (>= 10000 bp)   130920    
Total length (>= 25000 bp)   66828     
Total length (>= 50000 bp)   66828     
# contigs                    8         
Largest contig               66828     
Total length                 148186    
Reference length             268334    
GC (%)                       50.86     
Reference GC (%)             52.61     
N50                          21680     
NG50                         8747      
N75                          12600     
L50                          2         
LG50                         6         
L75                          4         
# misassemblies              0         
# misassembled contigs       0         
Misassembled contigs length  0         
# local misassemblies        0         
# unaligned contigs          5 + 1 part
Unaligned length             68680     
Genome fraction (%)          29.634    
Duplication ratio            1.000     
# N's per 100 kbp            0.00      
# mismatches per 100 kbp     0.00      
# indels per 100 kbp         1.26      
Largest alignment            66828     
NA50                         11778     
NGA50                        -         
LA50                         2         
