reference chromosomes:
	gi_757691877_ref_NZ_CP007733.1__Klebsiella_pneumoniae_subsp._pneumoniae_KPNIH27_plasmid_pKPN-068__complete_sequence (total length: 80411 bp, maximal covered length: 80084 bp)

total genome size: 80411

gap min size: 50
partial gene/operon min size: 100



assembly                 | genome    | duplication | gaps      | genes     | partial   | operons   | partial   |
                         | fraction  | ratio       | number    |           | genes     |           | operons   |
================================================================================================================
prediction               | 99.59334% | 1.01226     | 2         | -         | -         | -         | -         |
