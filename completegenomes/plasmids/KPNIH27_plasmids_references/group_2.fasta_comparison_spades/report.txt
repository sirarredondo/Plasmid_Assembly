All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                     prediction  
# contigs (>= 0 bp)          317         
# contigs (>= 1000 bp)       96          
# contigs (>= 5000 bp)       42          
# contigs (>= 10000 bp)      24          
# contigs (>= 25000 bp)      12          
# contigs (>= 50000 bp)      3           
Total length (>= 0 bp)       998606      
Total length (>= 1000 bp)    934273      
Total length (>= 5000 bp)    785301      
Total length (>= 10000 bp)   661559      
Total length (>= 25000 bp)   500040      
Total length (>= 50000 bp)   210699      
# contigs                    134         
Largest contig               92135       
Total length                 961945      
Reference length             80411       
GC (%)                       51.61       
Reference GC (%)             51.34       
N50                          26080       
NG50                         92135       
N75                          7712        
NG75                         92135       
L50                          12          
LG50                         1           
L75                          31          
LG75                         1           
# misassemblies              0           
# misassembled contigs       0           
Misassembled contigs length  0           
# local misassemblies        1           
# unaligned contigs          124 + 0 part
Unaligned length             883339      
Genome fraction (%)          99.593      
Duplication ratio            1.012       
# N's per 100 kbp            130.57      
# mismatches per 100 kbp     16.23       
# indels per 100 kbp         6.24        
Largest alignment            35372       
NGA50                        26615       
NGA75                        26615       
LGA50                        2           
LGA75                        2           
