All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                     prediction 
# contigs (>= 0 bp)          86         
# contigs (>= 1000 bp)       66         
# contigs (>= 5000 bp)       25         
# contigs (>= 10000 bp)      16         
# contigs (>= 25000 bp)      8          
# contigs (>= 50000 bp)      4          
Total length (>= 0 bp)       669543     
Total length (>= 1000 bp)    654681     
Total length (>= 5000 bp)    551427     
Total length (>= 10000 bp)   490928     
Total length (>= 25000 bp)   376475     
Total length (>= 50000 bp)   266334     
# contigs                    86         
Largest contig               92135      
Total length                 669543     
Reference length             338850     
GC (%)                       50.70      
Reference GC (%)             52.72      
N50                          27725      
NG50                         57003      
N75                          8747       
NG75                         50368      
L50                          7          
LG50                         3          
L75                          18         
LG75                         4          
# misassemblies              1          
# misassembled contigs       1          
Misassembled contigs length  7303       
# local misassemblies        1          
# unaligned contigs          44 + 3 part
Unaligned length             423708     
Genome fraction (%)          72.369     
Duplication ratio            1.021      
# N's per 100 kbp            0.00       
# mismatches per 100 kbp     50.57      
# indels per 100 kbp         3.26       
Largest alignment            56974      
NGA50                        10793      
LGA50                        6          
