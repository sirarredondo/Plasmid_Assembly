All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                    prediction
# contigs (>= 0 bp)         2         
# contigs (>= 1000 bp)      2         
# contigs (>= 5000 bp)      2         
# contigs (>= 10000 bp)     1         
# contigs (>= 25000 bp)     0         
# contigs (>= 50000 bp)     0         
Total length (>= 0 bp)      21039     
Total length (>= 1000 bp)   21039     
Total length (>= 5000 bp)   21039     
Total length (>= 10000 bp)  12982     
Total length (>= 25000 bp)  0         
Total length (>= 50000 bp)  0         
# contigs                   2         
Largest contig              12982     
Total length                21039     
Reference length            1552      
GC (%)                      50.96     
Reference GC (%)            51.87     
N50                         12982     
NG50                        12982     
N75                         8057      
NG75                        12982     
L50                         1         
LG50                        1         
L75                         2         
LG75                        1         
# unaligned contigs         2 + 0 part
Unaligned length            21039     
# N's per 100 kbp           0.00      
NGA50                       -         
