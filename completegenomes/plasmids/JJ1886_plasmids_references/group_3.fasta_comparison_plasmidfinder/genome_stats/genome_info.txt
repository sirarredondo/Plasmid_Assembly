reference chromosomes:
	gi_556555179_ref_NC_022651.1__Escherichia_coli_JJ1886_plasmid_pJJ1886_5__complete_sequence (total length: 110040 bp, maximal covered length: 21039 bp)

total genome size: 110040

gap min size: 50
partial gene/operon min size: 100



assembly                 | genome    | duplication | gaps      | genes     | partial   | operons   | partial   |
                         | fraction  | ratio       | number    |           | genes     |           | operons   |
================================================================================================================
prediction               | 19.11941% | 1.00000     | 3         | -         | -         | -         | -         |
