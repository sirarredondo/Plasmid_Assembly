All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                     prediction
# contigs (>= 0 bp)          9         
# contigs (>= 1000 bp)       9         
# contigs (>= 5000 bp)       5         
# contigs (>= 10000 bp)      2         
# contigs (>= 25000 bp)      2         
# contigs (>= 50000 bp)      1         
Total length (>= 0 bp)       125353    
Total length (>= 1000 bp)    125353    
Total length (>= 5000 bp)    117590    
Total length (>= 10000 bp)   98567     
Total length (>= 25000 bp)   98567     
Total length (>= 50000 bp)   55956     
# contigs                    9         
Largest contig               55956     
Total length                 125353    
Reference length             55956     
GC (%)                       46.21     
Reference GC (%)             45.88     
N50                          42611     
NG50                         55956     
N75                          42611     
NG75                         55956     
L50                          2         
LG50                         1         
L75                          2         
LG75                         1         
# misassemblies              0         
# misassembled contigs       0         
Misassembled contigs length  0         
# local misassemblies        0         
# unaligned contigs          8 + 0 part
Unaligned length             69397     
Genome fraction (%)          100.000   
Duplication ratio            1.000     
# N's per 100 kbp            0.00      
# mismatches per 100 kbp     0.00      
# indels per 100 kbp         0.00      
Largest alignment            55956     
NGA50                        55956     
NGA75                        55956     
LGA50                        1         
LGA75                        1         
