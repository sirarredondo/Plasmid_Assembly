reference chromosomes:
	gi_1033167583_ref_NZ_CP014757.1__Klebsiella_pneumoniae_strain_AATZP_plasmid_pNDM-1fa__complete_sequence (total length: 54064 bp, maximal covered length: 27798 bp)

total genome size: 54064

gap min size: 50
partial gene/operon min size: 100



assembly                 | genome    | duplication | gaps      | genes     | partial   | operons   | partial   |
                         | fraction  | ratio       | number    |           | genes     |           | operons   |
================================================================================================================
prediction               | 51.41684% | 1.00000     | 3         | -         | -         | -         | -         |
