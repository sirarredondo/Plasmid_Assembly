All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                     prediction 
# contigs (>= 0 bp)          93         
# contigs (>= 1000 bp)       51         
# contigs (>= 5000 bp)       21         
# contigs (>= 10000 bp)      13         
# contigs (>= 25000 bp)      7          
# contigs (>= 50000 bp)      3          
Total length (>= 0 bp)       594575     
Total length (>= 1000 bp)    582043     
Total length (>= 5000 bp)    499445     
Total length (>= 10000 bp)   450203     
Total length (>= 25000 bp)   362222     
Total length (>= 50000 bp)   224861     
# contigs                    61         
Largest contig               118664     
Total length                 588990     
Reference length             215092     
GC (%)                       49.74      
Reference GC (%)             53.09      
N50                          38788      
NG50                         118664     
N75                          10157      
NG75                         56122      
L50                          5          
LG50                         1          
L75                          13         
LG75                         2          
# misassemblies              0          
# misassembled contigs       0          
Misassembled contigs length  0          
# local misassemblies        2          
# unaligned contigs          33 + 1 part
Unaligned length             382507     
Genome fraction (%)          99.004     
Duplication ratio            1.021      
# N's per 100 kbp            0.00       
# mismatches per 100 kbp     30.99      
# indels per 100 kbp         0.94       
Largest alignment            50075      
NGA50                        19400      
NGA75                        5244       
LGA50                        3          
LGA75                        9          
