reference chromosomes:
	gi_828933578_gb_CP011600.1__Kluyvera_intermedia_strain_CAV1151_plasmid_pCAV1151-215__complete_sequence (total length: 215092 bp, maximal covered length: 212950 bp)

total genome size: 215092

gap min size: 50
partial gene/operon min size: 100



assembly                 | genome    | duplication | gaps      | genes     | partial   | operons   | partial   |
                         | fraction  | ratio       | number    |           | genes     |           | operons   |
================================================================================================================
prediction               | 99.00415% | 1.02130     | 3         | -         | -         | -         | -         |
