All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                     prediction
# contigs (>= 0 bp)          5         
# contigs (>= 1000 bp)       5         
# contigs (>= 5000 bp)       4         
# contigs (>= 10000 bp)      4         
# contigs (>= 25000 bp)      3         
# contigs (>= 50000 bp)      2         
Total length (>= 0 bp)       161715    
Total length (>= 1000 bp)    161715    
Total length (>= 5000 bp)    157534    
Total length (>= 10000 bp)   157534    
Total length (>= 25000 bp)   143798    
Total length (>= 50000 bp)   114983    
# contigs                    5         
Largest contig               58861     
Total length                 161715    
Reference length             295619    
GC (%)                       46.88     
Reference GC (%)             47.20     
N50                          56122     
NG50                         13736     
N75                          28815     
L50                          2         
LG50                         4         
L75                          3         
# misassemblies              0         
# misassembled contigs       0         
Misassembled contigs length  0         
# local misassemblies        1         
# unaligned contigs          4 + 0 part
Unaligned length             102854    
Genome fraction (%)          19.827    
Duplication ratio            1.004     
# N's per 100 kbp            0.00      
# mismatches per 100 kbp     87.01     
# indels per 100 kbp         5.12      
Largest alignment            58861     
NGA50                        -         
