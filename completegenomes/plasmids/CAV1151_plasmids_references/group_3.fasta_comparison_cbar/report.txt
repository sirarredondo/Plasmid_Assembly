All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                     prediction 
# contigs (>= 0 bp)          56         
# contigs (>= 1000 bp)       50         
# contigs (>= 5000 bp)       17         
# contigs (>= 10000 bp)      11         
# contigs (>= 25000 bp)      7          
# contigs (>= 50000 bp)      2          
Total length (>= 0 bp)       471902     
Total length (>= 1000 bp)    467312     
Total length (>= 5000 bp)    377746     
Total length (>= 10000 bp)   338972     
Total length (>= 25000 bp)   283588     
Total length (>= 50000 bp)   114983     
# contigs                    56         
Largest contig               58861      
Total length                 471902     
Reference length             215092     
GC (%)                       50.21      
Reference GC (%)             53.09      
N50                          30499      
NG50                         56122      
N75                          7748       
NG75                         38788      
L50                          6          
LG50                         2          
L75                          13         
LG75                         4          
# misassemblies              0          
# misassembled contigs       0          
Misassembled contigs length  0          
# local misassemblies        2          
# unaligned contigs          28 + 1 part
Unaligned length             299287     
Genome fraction (%)          82.665     
Duplication ratio            1.024      
# N's per 100 kbp            0.00       
# mismatches per 100 kbp     33.18      
# indels per 100 kbp         1.12       
Largest alignment            38788      
NGA50                        9191       
NGA75                        2022       
LGA50                        5          
LGA75                        19         
