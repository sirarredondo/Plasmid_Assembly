reference chromosomes:
	gi_749296015_ref_NZ_CP007729.1__Klebsiella_pneumoniae_subsp._pneumoniae_KPNIH10_plasmid_pKPN-498__complete_sequence (total length: 243824 bp, maximal covered length: 15529 bp)

total genome size: 243824

gap min size: 50
partial gene/operon min size: 100



assembly                 | genome    | duplication | gaps      | genes     | partial   | operons   | partial   |
                         | fraction  | ratio       | number    |           | genes     |           | operons   |
================================================================================================================
prediction               | 6.36894%  | 1.52128     | 2         | -         | -         | -         | -         |
