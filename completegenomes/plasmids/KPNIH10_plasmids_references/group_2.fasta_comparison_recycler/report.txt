All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                    prediction
# contigs (>= 0 bp)         1         
# contigs (>= 1000 bp)      1         
# contigs (>= 5000 bp)      0         
# contigs (>= 10000 bp)     0         
# contigs (>= 25000 bp)     0         
# contigs (>= 50000 bp)     0         
Total length (>= 0 bp)      2146      
Total length (>= 1000 bp)   2146      
Total length (>= 5000 bp)   0         
Total length (>= 10000 bp)  0         
Total length (>= 25000 bp)  0         
Total length (>= 50000 bp)  0         
# contigs                   1         
Largest contig              2146      
Total length                2146      
Reference length            243824    
GC (%)                      40.68     
Reference GC (%)            53.22     
N50                         2146      
N75                         2146      
L50                         1         
L75                         1         
# unaligned contigs         1 + 0 part
Unaligned length            2146      
# N's per 100 kbp           0.00      
NGA50                       -         
