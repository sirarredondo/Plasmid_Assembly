reference chromosomes:
	gi_1039435159_ref_NZ_CP014763.1__Klebsiella_pneumoniae_strain_KPNIH39_plasmid_pKPN-332__complete_sequence (total length: 284894 bp, maximal covered length: 210405 bp)

total genome size: 284894

gap min size: 50
partial gene/operon min size: 100



assembly                 | genome    | duplication | gaps      | genes     | partial   | operons   | partial   |
                         | fraction  | ratio       | number    |           | genes     |           | operons   |
================================================================================================================
prediction               | 73.85378% | 1.00302     | 12        | -         | -         | -         | -         |
