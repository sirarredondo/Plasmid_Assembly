reference chromosomes:
	gi_828927201_gb_CP011594.1__Klebsiella_oxytoca_strain_CAV1099_plasmid_pCAV1099-111__complete_sequence (total length: 111395 bp, maximal covered length: 111395 bp)

total genome size: 111395

gap min size: 50
partial gene/operon min size: 100



assembly                 | genome    | duplication | gaps      | genes     | partial   | operons   | partial   |
                         | fraction  | ratio       | number    |           | genes     |           | operons   |
================================================================================================================
prediction               | 100.00000%| 1.00069     | 0         | -         | -         | -         | -         |
