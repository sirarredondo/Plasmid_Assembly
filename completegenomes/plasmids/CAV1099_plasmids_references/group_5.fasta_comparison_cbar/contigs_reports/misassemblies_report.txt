All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                     prediction
# misassemblies              0         
    # relocations            0         
    # translocations         0         
    # inversions             0         
# misassembled contigs       0         
Misassembled contigs length  0         
# local misassemblies        1         
# mismatches                 10        
# indels                     2         
    # short indels           1         
    # long indels            1         
Indels length                43        
