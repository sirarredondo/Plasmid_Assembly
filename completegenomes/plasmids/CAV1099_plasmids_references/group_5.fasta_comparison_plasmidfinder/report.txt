All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                     prediction
# contigs (>= 0 bp)          2         
# contigs (>= 1000 bp)       2         
# contigs (>= 5000 bp)       2         
# contigs (>= 10000 bp)      2         
# contigs (>= 25000 bp)      2         
# contigs (>= 50000 bp)      2         
Total length (>= 0 bp)       171670    
Total length (>= 1000 bp)    171670    
Total length (>= 5000 bp)    171670    
Total length (>= 10000 bp)   171670    
Total length (>= 25000 bp)   171670    
Total length (>= 50000 bp)   171670    
# contigs                    2         
Largest contig               112462    
Total length                 171670    
Reference length             113992    
GC (%)                       53.41     
Reference GC (%)             51.52     
N50                          112462    
NG50                         112462    
N75                          59208     
NG75                         112462    
L50                          1         
LG50                         1         
L75                          2         
LG75                         1         
# misassemblies              0         
# misassembled contigs       0         
Misassembled contigs length  0         
# local misassemblies        0         
# unaligned contigs          1 + 1 part
Unaligned length             171050    
Genome fraction (%)          0.544     
Duplication ratio            1.000     
# N's per 100 kbp            0.00      
# mismatches per 100 kbp     0.00      
# indels per 100 kbp         0.00      
Largest alignment            620       
NGA50                        -         
