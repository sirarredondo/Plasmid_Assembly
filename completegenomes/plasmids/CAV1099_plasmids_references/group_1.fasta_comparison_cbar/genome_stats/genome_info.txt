reference chromosomes:
	gi_828927103_gb_CP011592.1__Klebsiella_oxytoca_strain_CAV1099_plasmid_pCAV1099-5410__complete_sequence (total length: 5410 bp, maximal covered length: 5259 bp)

total genome size: 5410

gap min size: 50
partial gene/operon min size: 100



assembly                 | genome    | duplication | gaps      | genes     | partial   | operons   | partial   |
                         | fraction  | ratio       | number    |           | genes     |           | operons   |
================================================================================================================
prediction               | 97.20887% | 1.00000     | 1         | -         | -         | -         | -         |
