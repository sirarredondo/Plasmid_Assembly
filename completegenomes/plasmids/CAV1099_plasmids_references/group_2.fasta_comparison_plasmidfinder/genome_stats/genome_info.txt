reference chromosomes:
	gi_828927110_gb_CP011593.1__Klebsiella_oxytoca_strain_CAV1099_plasmid_pCAV1099-69__complete_sequence (total length: 68910 bp, maximal covered length: 59208 bp)

total genome size: 68910

gap min size: 50
partial gene/operon min size: 100



assembly                 | genome    | duplication | gaps      | genes     | partial   | operons   | partial   |
                         | fraction  | ratio       | number    |           | genes     |           | operons   |
================================================================================================================
prediction               | 85.92077% | 1.00000     | 1         | -         | -         | -         | -         |
