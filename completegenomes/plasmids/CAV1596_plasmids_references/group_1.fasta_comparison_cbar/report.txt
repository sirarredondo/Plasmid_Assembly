All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                     prediction 
# contigs (>= 0 bp)          19         
# contigs (>= 1000 bp)       16         
# contigs (>= 5000 bp)       9          
# contigs (>= 10000 bp)      5          
# contigs (>= 25000 bp)      2          
# contigs (>= 50000 bp)      0          
Total length (>= 0 bp)       158756     
Total length (>= 1000 bp)    156215     
Total length (>= 5000 bp)    138647     
Total length (>= 10000 bp)   108878     
Total length (>= 25000 bp)   62664      
Total length (>= 50000 bp)   0          
# contigs                    19         
Largest contig               34148      
Total length                 158756     
Reference length             2927       
GC (%)                       48.56      
Reference GC (%)             46.53      
N50                          23370      
NG50                         34148      
N75                          8423       
NG75                         34148      
L50                          3          
LG50                         1          
L75                          7          
LG75                         1          
# misassemblies              0          
# misassembled contigs       0          
Misassembled contigs length  0          
# local misassemblies        0          
# unaligned contigs          18 + 0 part
Unaligned length             155752     
Genome fraction (%)          100.000    
Duplication ratio            1.026      
# N's per 100 kbp            0.00       
# mismatches per 100 kbp     0.00       
# indels per 100 kbp         0.00       
Largest alignment            3004       
NGA50                        3004       
NGA75                        3004       
LGA50                        1          
LGA75                        1          
