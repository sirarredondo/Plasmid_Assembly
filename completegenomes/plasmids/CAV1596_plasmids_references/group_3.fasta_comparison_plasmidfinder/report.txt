All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                     prediction
# contigs (>= 0 bp)          3         
# contigs (>= 1000 bp)       3         
# contigs (>= 5000 bp)       2         
# contigs (>= 10000 bp)      1         
# contigs (>= 25000 bp)      1         
# contigs (>= 50000 bp)      0         
Total length (>= 0 bp)       44095     
Total length (>= 1000 bp)    44095     
Total length (>= 5000 bp)    42571     
Total length (>= 10000 bp)   34148     
Total length (>= 25000 bp)   34148     
Total length (>= 50000 bp)   0         
# contigs                    3         
Largest contig               34148     
Total length                 44095     
Reference length             77801     
GC (%)                       44.41     
Reference GC (%)             45.42     
N50                          34148     
NG50                         8423      
N75                          34148     
L50                          1         
LG50                         2         
L75                          1         
# misassemblies              0         
# misassembled contigs       0         
Misassembled contigs length  0         
# local misassemblies        0         
# unaligned contigs          2 + 0 part
Unaligned length             9947      
Genome fraction (%)          43.891    
Duplication ratio            1.000     
# N's per 100 kbp            0.00      
# mismatches per 100 kbp     5.86      
# indels per 100 kbp         0.00      
Largest alignment            34148     
NA50                         34148     
NGA50                        -         
NA75                         34148     
LA50                         1         
LA75                         1         
