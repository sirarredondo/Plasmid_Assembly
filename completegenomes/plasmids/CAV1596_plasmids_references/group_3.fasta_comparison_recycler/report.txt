All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                    prediction
# contigs (>= 0 bp)         3         
# contigs (>= 1000 bp)      3         
# contigs (>= 5000 bp)      2         
# contigs (>= 10000 bp)     1         
# contigs (>= 25000 bp)     1         
# contigs (>= 50000 bp)     1         
Total length (>= 0 bp)      232209    
Total length (>= 1000 bp)   232209    
Total length (>= 5000 bp)   230063    
Total length (>= 10000 bp)  220610    
Total length (>= 25000 bp)  220610    
Total length (>= 50000 bp)  220610    
# contigs                   3         
Largest contig              220610    
Total length                232209    
Reference length            77801     
GC (%)                      58.60     
Reference GC (%)            45.42     
N50                         220610    
NG50                        220610    
N75                         220610    
NG75                        220610    
L50                         1         
LG50                        1         
L75                         1         
LG75                        1         
# unaligned contigs         3 + 0 part
Unaligned length            232209    
# N's per 100 kbp           0.00      
NGA50                       -         
