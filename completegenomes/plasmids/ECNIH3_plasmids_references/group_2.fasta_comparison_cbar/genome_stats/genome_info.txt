reference chromosomes:
	gi_749295313_ref_NZ_CP008899.1__Enterobacter_cloacae_ECNIH3_plasmid_pENT-8a4__complete_sequence (total length: 255013 bp, maximal covered length: 169904 bp)

total genome size: 255013

gap min size: 50
partial gene/operon min size: 100



assembly                 | genome    | duplication | gaps      | genes     | partial   | operons   | partial   |
                         | fraction  | ratio       | number    |           | genes     |           | operons   |
================================================================================================================
prediction               | 66.62562% | 1.00271     | 8         | -         | -         | -         | -         |
