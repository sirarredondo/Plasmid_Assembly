reference chromosomes:
	gi_749295352_ref_NZ_CP008901.1__Enterobacter_cloacae_ECNIH3_plasmid_pKPC-47e__complete_sequence (total length: 50333 bp, maximal covered length: 8258 bp)

total genome size: 50333

gap min size: 50
partial gene/operon min size: 100



assembly                 | genome    | duplication | gaps      | genes     | partial   | operons   | partial   |
                         | fraction  | ratio       | number    |           | genes     |           | operons   |
================================================================================================================
prediction               | 16.40673% | 1.00000     | 1         | -         | -         | -         | -         |
