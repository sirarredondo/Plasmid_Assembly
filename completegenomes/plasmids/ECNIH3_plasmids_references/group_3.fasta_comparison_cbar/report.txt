All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                     prediction 
# contigs (>= 0 bp)          20         
# contigs (>= 1000 bp)       18         
# contigs (>= 5000 bp)       13         
# contigs (>= 10000 bp)      11         
# contigs (>= 25000 bp)      5          
# contigs (>= 50000 bp)      0          
Total length (>= 0 bp)       332677     
Total length (>= 1000 bp)    331001     
Total length (>= 5000 bp)    315675     
Total length (>= 10000 bp)   297525     
Total length (>= 25000 bp)   190097     
Total length (>= 50000 bp)   0          
# contigs                    20         
Largest contig               49338      
Total length                 332677     
Reference length             60388      
GC (%)                       48.28      
Reference GC (%)             42.48      
N50                          27522      
NG50                         49338      
N75                          20884      
NG75                         49338      
L50                          5          
LG50                         1          
L75                          8          
LG75                         1          
# misassemblies              1          
# misassembled contigs       1          
Misassembled contigs length  39781      
# local misassemblies        0          
# unaligned contigs          18 + 0 part
Unaligned length             271720     
Genome fraction (%)          100.000    
Duplication ratio            1.009      
# N's per 100 kbp            0.00       
# mismatches per 100 kbp     9.94       
# indels per 100 kbp         6.62       
Largest alignment            39036      
NGA50                        39036      
NGA75                        21176      
LGA50                        1          
LGA75                        2          
