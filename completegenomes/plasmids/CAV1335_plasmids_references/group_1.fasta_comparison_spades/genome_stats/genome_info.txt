reference chromosomes:
	gi_828945252_gb_CP011613.1__Klebsiella_oxytoca_strain_CAV1335_plasmid_pCAV1335-5410__complete_sequence (total length: 5410 bp, maximal covered length: 5410 bp)

total genome size: 5410

gap min size: 50
partial gene/operon min size: 100



assembly                 | genome    | duplication | gaps      | genes     | partial   | operons   | partial   |
                         | fraction  | ratio       | number    |           | genes     |           | operons   |
================================================================================================================
prediction               | 100.00000%| 1.01423     | 0         | -         | -         | -         | -         |
