All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                          prediction
# fully unaligned contigs         38        
Fully unaligned length            423694    
# partially unaligned contigs     1         
    # with misassembly            0         
    # both parts are significant  1         
Partially unaligned length        25187     
# N's                             0         
