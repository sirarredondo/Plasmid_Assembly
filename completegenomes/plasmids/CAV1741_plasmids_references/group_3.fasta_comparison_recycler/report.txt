All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                    prediction
# contigs (>= 0 bp)         5         
# contigs (>= 1000 bp)      5         
# contigs (>= 5000 bp)      2         
# contigs (>= 10000 bp)     2         
# contigs (>= 25000 bp)     2         
# contigs (>= 50000 bp)     2         
Total length (>= 0 bp)      295662    
Total length (>= 1000 bp)   295662    
Total length (>= 5000 bp)   289220    
Total length (>= 10000 bp)  289220    
Total length (>= 25000 bp)  289220    
Total length (>= 50000 bp)  289220    
# contigs                   5         
Largest contig              179532    
Total length                295662    
Reference length            16257     
GC (%)                      51.13     
Reference GC (%)            46.95     
N50                         179532    
NG50                        179532    
N75                         109688    
NG75                        179532    
L50                         1         
LG50                        1         
L75                         2         
LG75                        1         
# unaligned contigs         5 + 0 part
Unaligned length            295662    
# N's per 100 kbp           0.00      
NGA50                       -         
