reference chromosomes:
	gi_828982668_gb_CP011654.1__Citrobacter_freundii_strain_CAV1741_plasmid_pCAV1741-101__complete_sequence (total length: 100873 bp, maximal covered length: 100133 bp)

total genome size: 100873

gap min size: 50
partial gene/operon min size: 100



assembly                 | genome    | duplication | gaps      | genes     | partial   | operons   | partial   |
                         | fraction  | ratio       | number    |           | genes     |           | operons   |
================================================================================================================
prediction               | 99.26640% | 1.01368     | 3         | -         | -         | -         | -         |
