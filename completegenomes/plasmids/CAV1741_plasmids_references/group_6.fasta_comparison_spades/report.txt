All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                     prediction 
# contigs (>= 0 bp)          21         
# contigs (>= 1000 bp)       14         
# contigs (>= 5000 bp)       8          
# contigs (>= 10000 bp)      7          
# contigs (>= 25000 bp)      4          
# contigs (>= 50000 bp)      3          
Total length (>= 0 bp)       342728     
Total length (>= 1000 bp)    341215     
Total length (>= 5000 bp)    322436     
Total length (>= 10000 bp)   316824     
Total length (>= 25000 bp)   279027     
Total length (>= 50000 bp)   250212     
# contigs                    15         
Largest contig               109765     
Total length                 341806     
Reference length             129196     
GC (%)                       50.92      
Reference GC (%)             51.58      
N50                          71670      
NG50                         109765     
N75                          28815      
NG75                         109765     
L50                          2          
LG50                         1          
L75                          4          
LG75                         1          
# misassemblies              1          
# misassembled contigs       1          
Misassembled contigs length  71670      
# local misassemblies        0          
# unaligned contigs          10 + 1 part
Unaligned length             226261     
Genome fraction (%)          99.957     
Duplication ratio            0.895      
# N's per 100 kbp            0.00       
# mismatches per 100 kbp     0.00       
# indels per 100 kbp         0.00       
Largest alignment            51203      
NGA50                        28815      
NGA75                        20467      
LGA50                        2          
LGA75                        3          
