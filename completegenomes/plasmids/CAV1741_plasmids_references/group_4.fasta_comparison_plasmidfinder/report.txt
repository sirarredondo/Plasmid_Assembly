All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                     prediction
# contigs (>= 0 bp)          3         
# contigs (>= 1000 bp)       3         
# contigs (>= 5000 bp)       3         
# contigs (>= 10000 bp)      3         
# contigs (>= 25000 bp)      3         
# contigs (>= 50000 bp)      2         
Total length (>= 0 bp)       210250    
Total length (>= 1000 bp)    210250    
Total length (>= 5000 bp)    210250    
Total length (>= 10000 bp)   210250    
Total length (>= 25000 bp)   210250    
Total length (>= 50000 bp)   181435    
# contigs                    3         
Largest contig               109765    
Total length                 210250    
Reference length             100873    
GC (%)                       50.30     
Reference GC (%)             52.10     
N50                          109765    
NG50                         109765    
N75                          71670     
NG75                         109765    
L50                          1         
LG50                         1         
L75                          2         
LG75                         1         
# misassemblies              0         
# misassembled contigs       0         
Misassembled contigs length  0         
# local misassemblies        0         
# unaligned contigs          2 + 1 part
Unaligned length             209591    
Genome fraction (%)          0.653     
Duplication ratio            1.000     
# N's per 100 kbp            0.00      
# mismatches per 100 kbp     303.49    
# indels per 100 kbp         0.00      
Largest alignment            659       
NGA50                        -         
