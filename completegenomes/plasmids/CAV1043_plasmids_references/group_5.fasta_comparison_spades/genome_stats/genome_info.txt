reference chromosomes:
	gi_828921939_gb_CP011589.1__Enterobacter_asburiae_strain_CAV1043_plasmid_pKPC_CAV1043__complete_sequence (total length: 59138 bp, maximal covered length: 57200 bp)

total genome size: 59138

gap min size: 50
partial gene/operon min size: 100



assembly                 | genome    | duplication | gaps      | genes     | partial   | operons   | partial   |
                         | fraction  | ratio       | number    |           | genes     |           | operons   |
================================================================================================================
prediction               | 96.72292% | 1.00538     | 4         | -         | -         | -         | -         |
