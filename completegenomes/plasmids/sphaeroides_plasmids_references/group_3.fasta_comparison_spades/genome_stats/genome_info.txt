reference chromosomes:
	gi_484336777_ref_NZ_AKVW01000007.1__Rhodobacter_sphaeroides_2.4.1_plasmid_Dx__complete_sequence__whole_genome_shotgun_sequence (total length: 52135 bp, maximal covered length: 47634 bp)

total genome size: 52135

gap min size: 50
partial gene/operon min size: 100



assembly                 | genome    | duplication | gaps      | genes     | partial   | operons   | partial   |
                         | fraction  | ratio       | number    |           | genes     |           | operons   |
================================================================================================================
prediction               | 91.36664% | 1.03432     | 3         | -         | -         | -         | -         |
