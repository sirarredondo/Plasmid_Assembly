All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                          prediction
# fully unaligned contigs         7         
Fully unaligned length            237024    
# partially unaligned contigs     2         
    # with misassembly            1         
    # both parts are significant  1         
Partially unaligned length        1939      
# N's                             0         
