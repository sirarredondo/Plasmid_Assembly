reference chromosomes:
	gi_959195734_gb_CP013326.1__Klebsiella_pneumoniae_strain_CAV1193_plasmid_pCAV1193-78__complete_sequence (total length: 77808 bp, maximal covered length: 37578 bp)

total genome size: 77808

gap min size: 50
partial gene/operon min size: 100



assembly                 | genome    | duplication | gaps      | genes     | partial   | operons   | partial   |
                         | fraction  | ratio       | number    |           | genes     |           | operons   |
================================================================================================================
prediction               | 48.29581% | 1.00003     | 3         | -         | -         | -         | -         |
