All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                     prediction 
# contigs (>= 0 bp)          52         
# contigs (>= 1000 bp)       39         
# contigs (>= 5000 bp)       20         
# contigs (>= 10000 bp)      14         
# contigs (>= 25000 bp)      4          
# contigs (>= 50000 bp)      1          
Total length (>= 0 bp)       457081     
Total length (>= 1000 bp)    447789     
Total length (>= 5000 bp)    395983     
Total length (>= 10000 bp)   352301     
Total length (>= 25000 bp)   219177     
Total length (>= 50000 bp)   125842     
# contigs                    52         
Largest contig               125842     
Total length                 457081     
Reference length             77808      
GC (%)                       51.71      
Reference GC (%)             53.33      
N50                          23005      
NG50                         125842     
N75                          10157      
NG75                         125842     
L50                          5          
LG50                         1          
L75                          14         
LG75                         1          
# misassemblies              0          
# misassembled contigs       0          
Misassembled contigs length  0          
# local misassemblies        0          
# unaligned contigs          36 + 1 part
Unaligned length             384993     
Genome fraction (%)          88.936     
Duplication ratio            1.042      
# N's per 100 kbp            0.00       
# mismatches per 100 kbp     0.00       
# indels per 100 kbp         0.00       
Largest alignment            34676      
NGA50                        12308      
NGA75                        4075       
LGA50                        2          
LGA75                        5          
