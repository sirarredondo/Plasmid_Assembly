reference chromosomes:
	gi_959190550_gb_CP013325.1__Klebsiella_pneumoniae_strain_CAV1193_plasmid_pKPC_CAV1193__complete_sequence (total length: 49565 bp, maximal covered length: 49565 bp)

total genome size: 49565

gap min size: 50
partial gene/operon min size: 100



assembly                 | genome    | duplication | gaps      | genes     | partial   | operons   | partial   |
                         | fraction  | ratio       | number    |           | genes     |           | operons   |
================================================================================================================
prediction               | 100.00000%| 1.02974     | 0         | -         | -         | -         | -         |
