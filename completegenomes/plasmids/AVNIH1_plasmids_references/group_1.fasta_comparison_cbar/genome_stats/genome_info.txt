reference chromosomes:
	gi_1033023972_ref_NZ_CP014775.1__Aeromonas_veronii_strain_AVNIH1_plasmid_pASP-a58__complete_sequence (total length: 198307 bp, maximal covered length: 165927 bp)

total genome size: 198307

gap min size: 50
partial gene/operon min size: 100



assembly                 | genome    | duplication | gaps      | genes     | partial   | operons   | partial   |
                         | fraction  | ratio       | number    |           | genes     |           | operons   |
================================================================================================================
prediction               | 83.67178% | 1.01227     | 5         | -         | -         | -         | -         |
