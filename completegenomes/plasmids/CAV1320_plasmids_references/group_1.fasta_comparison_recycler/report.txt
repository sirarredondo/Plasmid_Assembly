All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                     prediction
# contigs (>= 0 bp)          1         
# contigs (>= 1000 bp)       1         
# contigs (>= 5000 bp)       1         
# contigs (>= 10000 bp)      1         
# contigs (>= 25000 bp)      0         
# contigs (>= 50000 bp)      0         
Total length (>= 0 bp)       13981     
Total length (>= 1000 bp)    13981     
Total length (>= 5000 bp)    13981     
Total length (>= 10000 bp)   13981     
Total length (>= 25000 bp)   0         
Total length (>= 50000 bp)   0         
# contigs                    1         
Largest contig               13981     
Total length                 13981     
Reference length             13981     
GC (%)                       54.71     
Reference GC (%)             54.71     
N50                          13981     
NG50                         13981     
N75                          13981     
NG75                         13981     
L50                          1         
LG50                         1         
L75                          1         
LG75                         1         
# misassemblies              0         
# misassembled contigs       0         
Misassembled contigs length  0         
# local misassemblies        0         
# unaligned contigs          0 + 0 part
Unaligned length             0         
Genome fraction (%)          100.000   
Duplication ratio            1.000     
# N's per 100 kbp            0.00      
# mismatches per 100 kbp     0.00      
# indels per 100 kbp         0.00      
Largest alignment            13981     
NA50                         13981     
NGA50                        13981     
NA75                         13981     
NGA75                        13981     
LA50                         1         
LGA50                        1         
LA75                         1         
LGA75                        1         
