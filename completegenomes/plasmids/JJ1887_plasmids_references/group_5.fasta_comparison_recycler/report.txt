All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                     prediction 
# contigs (>= 0 bp)          15         
# contigs (>= 1000 bp)       15         
# contigs (>= 5000 bp)       4          
# contigs (>= 10000 bp)      1          
# contigs (>= 25000 bp)      1          
# contigs (>= 50000 bp)      0          
Total length (>= 0 bp)       87076      
Total length (>= 1000 bp)    87076      
Total length (>= 5000 bp)    61634      
Total length (>= 10000 bp)   42611      
Total length (>= 25000 bp)   42611      
Total length (>= 50000 bp)   0          
# contigs                    15         
Largest contig               42611      
Total length                 87076      
Reference length             107507     
GC (%)                       43.84      
Reference GC (%)             52.02      
N50                          8225       
NG50                         5631       
N75                          4863       
NG75                         1634       
L50                          2          
LG50                         3          
L75                          5          
LG75                         11         
# misassemblies              0          
# misassembled contigs       0          
Misassembled contigs length  0          
# local misassemblies        0          
# unaligned contigs          14 + 0 part
Unaligned length             82213      
Genome fraction (%)          4.523      
Duplication ratio            1.000      
# N's per 100 kbp            0.00       
# mismatches per 100 kbp     0.00       
# indels per 100 kbp         0.00       
Largest alignment            4863       
NGA50                        -          
