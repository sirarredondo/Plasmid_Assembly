reference chromosomes:
	gi_1006701360_gb_CP014320.1__Escherichia_coli_JJ1887_plasmid_pJJ1887-5__complete_sequence (total length: 130603 bp, maximal covered length: 45522 bp)

total genome size: 130603

gap min size: 50
partial gene/operon min size: 100



assembly                 | genome    | duplication | gaps      | genes     | partial   | operons   | partial   |
                         | fraction  | ratio       | number    |           | genes     |           | operons   |
================================================================================================================
prediction               | 34.85525% | 1.00859     | 10        | -         | -         | -         | -         |
