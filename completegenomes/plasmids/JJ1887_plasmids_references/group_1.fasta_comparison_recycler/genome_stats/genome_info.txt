reference chromosomes:
	gi_1006701338_gb_CP014317.1__Escherichia_coli_JJ1887_plasmid_pJJ1887-1__complete_sequence (total length: 1552 bp, maximal covered length: 1353 bp)

total genome size: 1552

gap min size: 50
partial gene/operon min size: 100



assembly                 | genome    | duplication | gaps      | genes     | partial   | operons   | partial   |
                         | fraction  | ratio       | number    |           | genes     |           | operons   |
================================================================================================================
prediction               | 87.17784% | 1.00000     | 1         | -         | -         | -         | -         |
