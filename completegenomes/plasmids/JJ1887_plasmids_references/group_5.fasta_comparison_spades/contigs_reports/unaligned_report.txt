All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                          prediction
# fully unaligned contigs         119       
Fully unaligned length            2636235   
# partially unaligned contigs     3         
    # with misassembly            0         
    # both parts are significant  3         
Partially unaligned length        25252     
# N's                             466       
