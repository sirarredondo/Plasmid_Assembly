reference chromosomes:
	gi_752806468_ref_NZ_CP007558.1__Citrobacter_freundii_CFNIH1_plasmid_pKEC-a3c__complete_sequence (total length: 272297 bp, maximal covered length: 52887 bp)

total genome size: 272297

gap min size: 50
partial gene/operon min size: 100



assembly                 | genome    | duplication | gaps      | genes     | partial   | operons   | partial   |
                         | fraction  | ratio       | number    |           | genes     |           | operons   |
================================================================================================================
prediction               | 19.42254% | 1.00004     | 2         | -         | -         | -         | -         |
