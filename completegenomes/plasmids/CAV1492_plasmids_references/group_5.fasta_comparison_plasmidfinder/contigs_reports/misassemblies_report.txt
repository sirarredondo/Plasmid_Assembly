All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                     prediction
# misassemblies              0         
    # relocations            0         
    # translocations         0         
    # inversions             0         
# misassembled contigs       0         
Misassembled contigs length  0         
# local misassemblies        0         
# mismatches                 8         
# indels                     0         
    # short indels           0         
    # long indels            0         
Indels length                0         
