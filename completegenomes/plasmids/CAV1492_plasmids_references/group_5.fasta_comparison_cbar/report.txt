All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                     prediction 
# contigs (>= 0 bp)          22         
# contigs (>= 1000 bp)       22         
# contigs (>= 5000 bp)       8          
# contigs (>= 10000 bp)      5          
# contigs (>= 25000 bp)      4          
# contigs (>= 50000 bp)      2          
Total length (>= 0 bp)       310967     
Total length (>= 1000 bp)    310967     
Total length (>= 5000 bp)    276171     
Total length (>= 10000 bp)   251597     
Total length (>= 25000 bp)   228645     
Total length (>= 50000 bp)   139123     
# contigs                    22         
Largest contig               85610      
Total length                 310967     
Reference length             199444     
GC (%)                       51.08      
Reference GC (%)             51.13      
N50                          45295      
NG50                         53513      
N75                          22952      
NG75                         45295      
L50                          3          
LG50                         2          
L75                          5          
LG75                         3          
# misassemblies              0          
# misassembled contigs       0          
Misassembled contigs length  0          
# local misassemblies        0          
# unaligned contigs          10 + 0 part
Unaligned length             129391     
Genome fraction (%)          91.300     
Duplication ratio            1.004      
# N's per 100 kbp            0.00       
# mismatches per 100 kbp     11.53      
# indels per 100 kbp         0.55       
Largest alignment            85610      
NA50                         7523       
NGA50                        45295      
NGA75                        22952      
LA50                         4          
LGA50                        2          
LGA75                        3          
