All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                     prediction 
# contigs (>= 0 bp)          33         
# contigs (>= 1000 bp)       27         
# contigs (>= 5000 bp)       10         
# contigs (>= 10000 bp)      6          
# contigs (>= 25000 bp)      4          
# contigs (>= 50000 bp)      2          
Total length (>= 0 bp)       343286     
Total length (>= 1000 bp)    342419     
Total length (>= 5000 bp)    300269     
Total length (>= 10000 bp)   267867     
Total length (>= 25000 bp)   230690     
Total length (>= 50000 bp)   139123     
# contigs                    27         
Largest contig               85610      
Total length                 342419     
Reference length             3223       
GC (%)                       50.80      
Reference GC (%)             56.35      
N50                          47340      
NG50                         85610      
N75                          14477      
NG75                         85610      
L50                          3          
LG50                         1          
L75                          6          
LG75                         1          
# misassemblies              0          
# misassembled contigs       0          
Misassembled contigs length  0          
# local misassemblies        0          
# unaligned contigs          26 + 0 part
Unaligned length             339161     
Genome fraction (%)          100.000    
Duplication ratio            1.011      
# N's per 100 kbp            0.00       
# mismatches per 100 kbp     0.00       
# indels per 100 kbp         0.00       
Largest alignment            3258       
NGA50                        3258       
NGA75                        3258       
LGA50                        1          
LGA75                        1          
