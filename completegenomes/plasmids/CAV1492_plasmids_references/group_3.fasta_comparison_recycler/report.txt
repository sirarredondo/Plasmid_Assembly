All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                    prediction
# contigs (>= 0 bp)         2         
# contigs (>= 1000 bp)      2         
# contigs (>= 5000 bp)      1         
# contigs (>= 10000 bp)     0         
# contigs (>= 25000 bp)     0         
# contigs (>= 50000 bp)     0         
Total length (>= 0 bp)      9133      
Total length (>= 1000 bp)   9133      
Total length (>= 5000 bp)   7672      
Total length (>= 10000 bp)  0         
Total length (>= 25000 bp)  0         
Total length (>= 50000 bp)  0         
# contigs                   2         
Largest contig              7672      
Total length                9133      
Reference length            69158     
GC (%)                      44.49     
Reference GC (%)            49.21     
N50                         7672      
N75                         7672      
L50                         1         
L75                         1         
# unaligned contigs         2 + 0 part
Unaligned length            9133      
# N's per 100 kbp           0.00      
NGA50                       -         
