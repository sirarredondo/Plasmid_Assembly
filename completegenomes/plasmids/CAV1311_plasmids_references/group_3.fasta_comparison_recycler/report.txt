All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                    prediction
# contigs (>= 0 bp)         8         
# contigs (>= 1000 bp)      8         
# contigs (>= 5000 bp)      5         
# contigs (>= 10000 bp)     2         
# contigs (>= 25000 bp)     1         
# contigs (>= 50000 bp)     0         
Total length (>= 0 bp)      92244     
Total length (>= 1000 bp)   92244     
Total length (>= 5000 bp)   84037     
Total length (>= 10000 bp)  61389     
Total length (>= 25000 bp)  43320     
Total length (>= 50000 bp)  0         
# contigs                   8         
Largest contig              43320     
Total length                92244     
Reference length            90452     
GC (%)                      50.70     
Reference GC (%)            56.99     
N50                         18069     
NG50                        18069     
N75                         9434      
NG75                        9434      
L50                         2         
LG50                        2         
L75                         3         
LG75                        3         
# unaligned contigs         8 + 0 part
Unaligned length            92244     
# N's per 100 kbp           0.00      
NGA50                       -         
