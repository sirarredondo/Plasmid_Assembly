reference chromosomes:
	gi_828895818_gb_CP011569.1__Enterobacter_cloacae_strain_CAV1311_plasmid_pCAV1311-3223__complete_sequence (total length: 3223 bp, maximal covered length: 3074 bp)

total genome size: 3223

gap min size: 50
partial gene/operon min size: 100



assembly                 | genome    | duplication | gaps      | genes     | partial   | operons   | partial   |
                         | fraction  | ratio       | number    |           | genes     |           | operons   |
================================================================================================================
prediction               | 95.37698% | 1.00000     | 1         | -         | -         | -         | -         |
