reference chromosomes:
	gi_828958913_gb_CP011634.1__Klebsiella_oxytoca_strain_CAV1374_plasmid_pCAV1374-228__complete_sequence (total length: 227680 bp, maximal covered length: 226470 bp)

total genome size: 227680

gap min size: 50
partial gene/operon min size: 100



assembly                 | genome    | duplication | gaps      | genes     | partial   | operons   | partial   |
                         | fraction  | ratio       | number    |           | genes     |           | operons   |
================================================================================================================
prediction               | 99.46855% | 1.01927     | 6         | -         | -         | -         | -         |
