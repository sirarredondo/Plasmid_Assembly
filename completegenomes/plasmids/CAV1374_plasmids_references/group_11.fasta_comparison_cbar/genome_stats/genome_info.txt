reference chromosomes:
	gi_828959145_gb_CP011635.1__Klebsiella_oxytoca_strain_CAV1374_plasmid_pKPC_CAV1374__complete_sequence (total length: 332956 bp, maximal covered length: 330075 bp)

total genome size: 332956

gap min size: 50
partial gene/operon min size: 100



assembly                 | genome    | duplication | gaps      | genes     | partial   | operons   | partial   |
                         | fraction  | ratio       | number    |           | genes     |           | operons   |
================================================================================================================
prediction               | 99.13472% | 1.00306     | 5         | -         | -         | -         | -         |
