All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                    prediction
# contigs (>= 0 bp)         2         
# contigs (>= 1000 bp)      2         
# contigs (>= 5000 bp)      2         
# contigs (>= 10000 bp)     1         
# contigs (>= 25000 bp)     0         
# contigs (>= 50000 bp)     0         
Total length (>= 0 bp)      30128     
Total length (>= 1000 bp)   30128     
Total length (>= 5000 bp)   30128     
Total length (>= 10000 bp)  22167     
Total length (>= 25000 bp)  0         
Total length (>= 50000 bp)  0         
# contigs                   2         
Largest contig              22167     
Total length                30128     
Reference length            227680    
GC (%)                      55.74     
Reference GC (%)            53.83     
N50                         22167     
N75                         7961      
L50                         1         
L75                         2         
# unaligned contigs         2 + 0 part
Unaligned length            30128     
# N's per 100 kbp           0.00      
NGA50                       -         
