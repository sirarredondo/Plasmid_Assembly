All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                     prediction 
# contigs (>= 0 bp)          89         
# contigs (>= 1000 bp)       60         
# contigs (>= 5000 bp)       30         
# contigs (>= 10000 bp)      24         
# contigs (>= 25000 bp)      11         
# contigs (>= 50000 bp)      7          
Total length (>= 0 bp)       870887     
Total length (>= 1000 bp)    856377     
Total length (>= 5000 bp)    781452     
Total length (>= 10000 bp)   737617     
Total length (>= 25000 bp)   534000     
Total length (>= 50000 bp)   408636     
# contigs                    75         
Largest contig               79377      
Total length                 867485     
Reference length             14274      
GC (%)                       51.90      
Reference GC (%)             48.78      
N50                          45316      
NG50                         79377      
N75                          16736      
NG75                         79377      
L50                          8          
LG50                         1          
L75                          17         
LG75                         1          
# misassemblies              0          
# misassembled contigs       0          
Misassembled contigs length  0          
# local misassemblies        0          
# unaligned contigs          71 + 0 part
Unaligned length             853277     
Genome fraction (%)          98.403     
Duplication ratio            1.012      
# N's per 100 kbp            0.00       
# mismatches per 100 kbp     0.00       
# indels per 100 kbp         0.00       
Largest alignment            9478       
NGA50                        9478       
NGA75                        2146       
LGA50                        1          
LGA75                        2          
