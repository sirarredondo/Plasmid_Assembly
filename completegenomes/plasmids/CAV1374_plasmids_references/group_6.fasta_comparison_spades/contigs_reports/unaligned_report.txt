All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                          prediction
# fully unaligned contigs         62        
Fully unaligned length            717029    
# partially unaligned contigs     4         
    # with misassembly            0         
    # both parts are significant  4         
Partially unaligned length        98408     
# N's                             0         
