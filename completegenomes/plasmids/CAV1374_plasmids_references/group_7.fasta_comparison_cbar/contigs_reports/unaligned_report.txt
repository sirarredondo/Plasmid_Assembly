All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                          prediction
# fully unaligned contigs         56        
Fully unaligned length            755400    
# partially unaligned contigs     0         
    # with misassembly            0         
    # both parts are significant  0         
Partially unaligned length        0         
# N's                             0         
