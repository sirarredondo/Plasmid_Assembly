reference chromosomes:
	gi_828958596_gb_CP011631.1__Klebsiella_oxytoca_strain_CAV1374_plasmid_pCAV1374-54__complete_sequence (total length: 53596 bp, maximal covered length: 53596 bp)

total genome size: 53596

gap min size: 50
partial gene/operon min size: 100



assembly                 | genome    | duplication | gaps      | genes     | partial   | operons   | partial   |
                         | fraction  | ratio       | number    |           | genes     |           | operons   |
================================================================================================================
prediction               | 100.00000%| 1.00287     | 0         | -         | -         | -         | -         |
