All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                     prediction 
# contigs (>= 0 bp)          58         
# contigs (>= 1000 bp)       50         
# contigs (>= 5000 bp)       29         
# contigs (>= 10000 bp)      23         
# contigs (>= 25000 bp)      11         
# contigs (>= 50000 bp)      6          
Total length (>= 0 bp)       808106     
Total length (>= 1000 bp)    802322     
Total length (>= 5000 bp)    752390     
Total length (>= 10000 bp)   709602     
Total length (>= 25000 bp)   516394     
Total length (>= 50000 bp)   350743     
# contigs                    58         
Largest contig               79377      
Total length                 808106     
Reference length             16069      
GC (%)                       51.84      
Reference GC (%)             59.34      
N50                          40287      
NG50                         79377      
N75                          17756      
NG75                         79377      
L50                          8          
LG50                         1          
L75                          16         
LG75                         1          
# misassemblies              0          
# misassembled contigs       0          
Misassembled contigs length  0          
# local misassemblies        0          
# unaligned contigs          55 + 0 part
Unaligned length             794716     
Genome fraction (%)          82.370     
Duplication ratio            1.012      
# N's per 100 kbp            0.00       
# mismatches per 100 kbp     0.00       
# indels per 100 kbp         0.00       
Largest alignment            10011      
NGA50                        10011      
NGA75                        1356       
LGA50                        1          
LGA75                        3          
