All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                     prediction
# contigs (>= 0 bp)          3         
# contigs (>= 1000 bp)       3         
# contigs (>= 5000 bp)       3         
# contigs (>= 10000 bp)      2         
# contigs (>= 25000 bp)      2         
# contigs (>= 50000 bp)      2         
Total length (>= 0 bp)       196661    
Total length (>= 1000 bp)    196661    
Total length (>= 5000 bp)    196661    
Total length (>= 10000 bp)   188088    
Total length (>= 25000 bp)   188088    
Total length (>= 50000 bp)   188088    
# contigs                    3         
Largest contig               119231    
Total length                 196661    
Reference length             111227    
GC (%)                       46.67     
Reference GC (%)             53.02     
N50                          119231    
NG50                         119231    
N75                          68857     
NG75                         119231    
L50                          1         
LG50                         1         
L75                          2         
LG75                         1         
# misassemblies              0         
# misassembled contigs       0         
Misassembled contigs length  0         
# local misassemblies        0         
# unaligned contigs          2 + 0 part
Unaligned length             127804    
Genome fraction (%)          61.916    
Duplication ratio            1.000     
# N's per 100 kbp            0.00      
# mismatches per 100 kbp     0.00      
# indels per 100 kbp         1.45      
Largest alignment            68857     
NGA50                        68857     
LGA50                        1         
