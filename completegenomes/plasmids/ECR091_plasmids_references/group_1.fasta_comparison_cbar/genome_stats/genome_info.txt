reference chromosomes:
	gi_749295354_ref_NZ_CP008906.1__Enterobacter_cloacae_ECR091_plasmid_pENT-08e__complete_sequence (total length: 176943 bp, maximal covered length: 176349 bp)

total genome size: 176943

gap min size: 50
partial gene/operon min size: 100



assembly                 | genome    | duplication | gaps      | genes     | partial   | operons   | partial   |
                         | fraction  | ratio       | number    |           | genes     |           | operons   |
================================================================================================================
prediction               | 99.66430% | 1.00316     | 2         | -         | -         | -         | -         |
