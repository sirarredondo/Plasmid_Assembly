All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                    prediction
# contigs (>= 0 bp)         7         
# contigs (>= 1000 bp)      7         
# contigs (>= 5000 bp)      3         
# contigs (>= 10000 bp)     2         
# contigs (>= 25000 bp)     1         
# contigs (>= 50000 bp)     1         
Total length (>= 0 bp)      94558     
Total length (>= 1000 bp)   94558     
Total length (>= 5000 bp)   83089     
Total length (>= 10000 bp)  73655     
Total length (>= 25000 bp)  56315     
Total length (>= 50000 bp)  56315     
# contigs                   7         
Largest contig              56315     
Total length                94558     
Reference length            50333     
GC (%)                      55.49     
Reference GC (%)            52.17     
N50                         56315     
NG50                        56315     
N75                         17340     
NG75                        56315     
L50                         1         
LG50                        1         
L75                         2         
LG75                        1         
# unaligned contigs         7 + 0 part
Unaligned length            94558     
# N's per 100 kbp           0.00      
NGA50                       -         
