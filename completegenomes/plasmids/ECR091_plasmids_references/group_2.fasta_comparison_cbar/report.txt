All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                     prediction
# contigs (>= 0 bp)          12        
# contigs (>= 1000 bp)       10        
# contigs (>= 5000 bp)       7         
# contigs (>= 10000 bp)      5         
# contigs (>= 25000 bp)      3         
# contigs (>= 50000 bp)      3         
Total length (>= 0 bp)       296283    
Total length (>= 1000 bp)    294571    
Total length (>= 5000 bp)    290761    
Total length (>= 10000 bp)   276612    
Total length (>= 25000 bp)   243796    
Total length (>= 50000 bp)   243796    
# contigs                    12        
Largest contig               119231    
Total length                 296283    
Reference length             111227    
GC (%)                       47.14     
Reference GC (%)             53.02     
N50                          68857     
NG50                         119231    
N75                          55708     
NG75                         119231    
L50                          2         
LG50                         1         
L75                          3         
LG75                         1         
# misassemblies              0         
# misassembled contigs       0         
Misassembled contigs length  0         
# local misassemblies        0         
# unaligned contigs          7 + 0 part
Unaligned length             223103    
Genome fraction (%)          66.511    
Duplication ratio            1.002     
# N's per 100 kbp            0.00      
# mismatches per 100 kbp     22.98     
# indels per 100 kbp         2.70      
Largest alignment            68857     
NGA50                        68857     
LGA50                        1         
