All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                     prediction 
# contigs (>= 0 bp)          22         
# contigs (>= 1000 bp)       21         
# contigs (>= 5000 bp)       9          
# contigs (>= 10000 bp)      5          
# contigs (>= 25000 bp)      3          
# contigs (>= 50000 bp)      2          
Total length (>= 0 bp)       244034     
Total length (>= 1000 bp)    243244     
Total length (>= 5000 bp)    211463     
Total length (>= 10000 bp)   181817     
Total length (>= 25000 bp)   150568     
Total length (>= 50000 bp)   115633     
# contigs                    22         
Largest contig               65587      
Total length                 244034     
Reference length             40448      
GC (%)                       53.40      
Reference GC (%)             56.26      
N50                          34935      
NG50                         65587      
N75                          9793       
NG75                         65587      
L50                          3          
LG50                         1          
L75                          6          
LG75                         1          
# misassemblies              0          
# misassembled contigs       0          
Misassembled contigs length  0          
# local misassemblies        0          
# unaligned contigs          17 + 0 part
Unaligned length             215053     
Genome fraction (%)          79.955     
Duplication ratio            1.004      
# N's per 100 kbp            0.00       
# mismatches per 100 kbp     0.00       
# indels per 100 kbp         0.00       
Largest alignment            16132      
NGA50                        4288       
LGA50                        2          
