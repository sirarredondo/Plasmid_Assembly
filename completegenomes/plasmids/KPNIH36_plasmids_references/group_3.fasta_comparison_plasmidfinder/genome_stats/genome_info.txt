reference chromosomes:
	gi_1040187156_ref_NZ_CP014650.1__Klebsiella_pneumoniae_strain_KPNIH36_plasmid_pKpQIL-6e6__complete_sequence (total length: 113639 bp, maximal covered length: 100522 bp)

total genome size: 113639

gap min size: 50
partial gene/operon min size: 100



assembly                 | genome    | duplication | gaps      | genes     | partial   | operons   | partial   |
                         | fraction  | ratio       | number    |           | genes     |           | operons   |
================================================================================================================
prediction               | 88.45731% | 1.00000     | 2         | -         | -         | -         | -         |
