Command line: /hpc/dla_mm/salonso/SPAdes-3.8.2-Linux/bin/spades.py	-t	4	--only-assembler	-1	/hpc/dla_mm/salonso/SRR2965739/trim_1.fq	-2	/hpc/dla_mm/salonso/SRR2965739/trim_2.fq	-o	/hpc/dla_mm/salonso/additional_data_3_8/recycler_SRR2965739	

System information:
  SPAdes version: 3.8.2
  Python version: 2.7.11
  OS: Linux-3.10.0-327.10.1.el7.x86_64-x86_64-with-centos-7.2.1511-Core

Output dir: /hpc/dla_mm/salonso/additional_data_3_8/recycler_SRR2965739
Mode: ONLY assembling (without read error correction)
Debug mode is turned OFF

Dataset parameters:
  Multi-cell mode (you should set '--sc' flag if input data was obtained with MDA (single-cell) technology or --meta flag if processing metagenomic dataset)
  Reads:
    Library number: 1, library type: paired-end
      orientation: fr
      left reads: ['/hpc/dla_mm/salonso/SRR2965739/trim_1.fq']
      right reads: ['/hpc/dla_mm/salonso/SRR2965739/trim_2.fq']
      interlaced reads: not specified
      single reads: not specified
Assembly parameters:
  k: automatic selection based on read length
  Repeat resolution is enabled
  Mismatch careful mode is turned OFF
  MismatchCorrector will be SKIPPED
  Coverage cutoff is turned OFF
Other parameters:
  Dir for temp files: /hpc/dla_mm/salonso/additional_data_3_8/recycler_SRR2965739/tmp
  Threads: 4
  Memory limit (in Gb): 125

