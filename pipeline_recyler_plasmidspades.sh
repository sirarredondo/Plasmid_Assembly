#!/bin/bash
#requires python 2.7
#requires NCBI-tools
#requires quast-4.1.tar.gz
#requires spades-8.2
#requires recycler
#

CURRENTDIR=$(pwd)

##Checks
##Check for command line arguments
if [ $# -eq 0 ]; then
    echo """
## This is the script used to run PlasmidSPAdes, SPAdes and Recycler. 
## The script has two arguments: 1st SRA link ; 2nd three possible flags: --pspades, --recycler or --both
## The SRA link can be:
##Strain SRA Bioproject
Aeromonas veronii strain AVNIH1 SRR3465535 PRJNA279607
Bacillus subtilis subsp. natto BEST195 DRR016448 PRJDA38027
Burkholderia cenocepacia strain DDS 22E-1 SRR1618480 PRJNA244014
Citrobacter freundii CFNIH1 SRR1284629 PRJNA202883
Citrobacter freundii strain CAV1321 SRR2965690 PRJNA246471
Citrobacter freundii strain CAV1741 SRR2965739 PRJNA246471
Corynebacterium callunae DSM 20147 SRR892039 PRJNA185570
Enterobacter aerogenes strain CAV1320 SRR2965748 PRJNA246471
Enterobacter asburiae strain CAV1043 SRR2965752 PRJNA246471
Enterobacter cloacae ECNIH2 SRR1515967 PRJNA202893
Enterobacter cloacae ECNIH3 SRR1576778 PRJNA202894
Enterobacter cloacae ECR091 SRR1576808 PRJNA202892
Enterobacter cloacae strain CAV1311 SRR2965815 PRJNA246471
Enterobacter cloacae strain CAV1411 SRR2965820 PRJNA246471
Enterobacter cloacae strain CAV1668 SRR2965612 PRJNA246471
Enterobacter cloacae strain CAV1669 SRR2965616 PRJNA246471
Enterococcus faecium strain ATCC 700221 SRR3176159 PRJNA311738
Escherichia coli JJ1886 SRR933487 PRJNA211153
Escherichia coli JJ1887 SRR933489 PRJNA211153"""
    exit
fi

##Check software
command -v prefetch  >/dev/null 2>&1 || { echo >&2 "sra-toolkit required but not found.  Aborting."; exit 1; }
command -v seqtk   >/dev/null 2>&1 || { echo >&2 "seqtk required but not found.  Aborting."; exit 1; }
if [[ ! $(python --version 2>&1) == *2\.7* ]]; then
 printf "Python 2.7 required but not found. Aborting\n"; exit 1
fi

command python $CURRENTDIR/programmes/SPAdes-3.8.2-Linux/bin/spades.py\
 >  /dev/null 2>&1 || { echo >&2 "SPAdes-3.8.2 required but not found in folder programmes or not properly installed. Aborting."; exit 1; }
command python $CURRENTDIR/programmes/Recycler/recycle.py --help\
 > /dev/null 2>&1 || { echo >&2 "Recycler required but not found in folder programmes or not properly installed. Aborting."; exit 1; }

command python $CURRENTDIR/programmes/SPAdes-3.8.2-Linux/bin/plasmidspades.py\
 > /dev/null 2>&1 || { echo >&2 "PlasmidSPAdes required but not found in folder programmes or exits with error. Aborting."; exit 1; }


command -v bwa > /dev/null 2>&1 || { echo >&2 "bwa required but not found. Aborting."; exit 1; }
command -v samtools> /dev/null 2>&1 || { echo >&2 "samtools required but not found. Aborting."; exit 1; }


#Download data with NCBI tools
prefetch -v $1


READDIR=$CURRENTDIR/reads/
mkdir -p $READDIR
if [[ ! -d $READDIR/$1 ]]; then
  fastq-dump --outdir $READDIR/$1 --gzip --skip-technical --readids --dumpbase --split-files --clip $1
else
  printf "reads $1 already extracted\n"
fi

array=($(ls $READDIR/$1))

fq_1=${array[0]}
fq_2=${array[1]}

if [[ ! -f $READDIR/$1/trim_1.fq ]];then
 seqtk trimfq $READDIR/$1/$fq_1 > $READDIR/$1/trim_1.fq
else
 printf "forward reads $1 already trimmed\n"
fi
if [[ ! -f $READDIR/$1/trim_2.fq ]];then
 seqtk trimfq $READDIR/$1/$fq_2 > $READDIR/$1/trim_2.fq
else
 printf "reverse reads $1 already trimmed\n"
fi

mkdir -p new_results
args=("$@")
data_pspades=$CURRENTDIR/new_results/pspades_$1
data_recycler=$CURRENTDIR/new_results/recycler_$1
for i in "${args[@]}"
do
	if [ "$i" == '--pspades' -o "$i" == '--both' ];then
           	python $CURRENTDIR/programmes/SPAdes-3.8.2-Linux/bin/plasmidspades.py -t 4 --only-assembler -1 $READDIR/$1/trim_1.fq -2 $READDIR/$1/trim_2.fq -o $data_pspades
	fi
	if [ "$i" == '--recycler' -o "$i" == '--both' ];then
                python $CURRENTDIR/programmes/SPAdes-3.8.2-Linux/bin/spades.py -t 4 --only-assembler -1 $READDIR/$1/trim_1.fq -2 $READDIR/$1/trim_2.fq -o $data_recycler

		kmer=$(grep "Running assembler" $data_recycler/spades.log | tail -1 | cut -d "K" -f 2)

		python $CURRENTDIR/programmes/Recycler/make_fasta_from_fastg.py -g $data_recycler/assembly_graph.fastg

		bwa index $data_recycler/assembly_graph.nodes.fasta

		bwa mem $data_recycler/assembly_graph.nodes.fasta $READDIR/$1/trim_1.fq $READDIR/$1/trim_2.fq|samtools view -buS - > $data_recycler/reads_pe.bam

		samtools view -bF 0x0800 $data_recycler/reads_pe.bam > $data_recycler/reads_pe_primary.bam

		samtools sort $data_recycler/reads_pe_primary.bam > $data_recycler/reads_pe_primary.sort.bam

		samtools index $data_recycler/reads_pe_primary.sort.bam

		python $CURRENTDIR/programmes/Recycler/recycle.py -g $data_recycler/assembly_graph.fastg -b $data_recycler/reads_pe_primary.sort.bam -k $kmer 		
        fi

done

