
Analysis <a name="analysis"></a>
=====================================
You can check [here](detailed_analysis.md) the analysis and metrics reported in the manuscript. The files
for the analysis are available at [/Analysis/All_Genomes](/Analysis/All_Genomes)


How the main files were generated <a name="input"></a>
=====================================

You can check [here](generating_files.md) how we carried out the plasmid prediction using PlasmidSPAdes, Recycler, cBAR and PlasmidFinder. 