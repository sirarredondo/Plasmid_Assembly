# -*- coding: utf-8 -*-
"""
Created on Wed Jun 29 15:15:06 2016

@author: arredondo
"""
#
import os
#
##We export the variables from the bash script.These variables correspond to data as the name of the strain being analyzed or the location of different files
##created by Quast with the preliminary results of PlasmidSPAdes,Recycler,cBar and PlasmidFinder. 

name_strain=os.environ["strain"]
name_complete_strain=os.environ["name_complete_strain"]


##Variables from PlasmidSPAdes
chr_coverage=os.environ["chr_coverage"]
contigs_plasmidspades=os.environ["contigs_plasmidspades"]
file_alignments_plasmidspades=os.environ["file_alignments_plasmidspades"]
file_unalignments_plasmidspades=os.environ["file_unalignments_plasmidspades"]
pspades_plasmid_genome_fraction=os.environ["pspades_plasmid_genome_fraction"]
pspades_chromosome_genome_fraction=os.environ["pspades_chromosome_genome_fraction"]

##Importing the contigs from SPAdes 3.8
contigs_spades=os.environ["contigs_spades"]
unaligned_contigs_spades=os.environ["unaligned_contigs_spades"]

##Variables from Recycler
plasmids_recycler=os.environ["plasmids_recycler"]
file_alignments_recycler=os.environ["file_alignments_recycler"]
file_unalignments_recycler=os.environ["file_unalignments_recycler"]
recycler_plasmid_genome_fraction=os.environ["recycler_plasmid_genome_fraction"]
recycler_chromosome_genome_fraction=os.environ["recycler_chromosome_genome_fraction"]

###Variables from cBar
contigs_alignments=os.environ["contigs_alignments"]
cbar_spades=os.environ["cbar_spades"]

###Variables from PlasmidFinder
plasmidfinder_results=os.environ["plasmidfinder_results"]

path=os.environ["PWD"]



#####Creating files for the outcome
os.chdir(path+"/Analysis")
pspades=open(path+"/Analysis/"+name_strain+"/PlasmidSPAdes/PlasmidSPAdes_results","w+")
unalignments_pspades=open(path+"/Analysis/"+name_strain+"/PlasmidSPAdes/unalignments_pspades","w+")
recycler=open(path+"/Analysis/"+name_strain+"/Recycler/Recycler_results","w+")
unalignments_recycler=open(path+"/Analysis/"+name_strain+"/Recycler/unalignments_recycler","w+")
cbar=open(path+"/Analysis/"+name_strain+"/cBAR/cBAR_results","w+")
cbar_recall=open(path+"/Analysis/"+name_strain+"/cBAR/cBAR_recall","w+")
unalignments_cbar=open(path+"/Analysis/"+name_strain+"/cBAR/unalignments_cbar","w+")
plasmidfinder_output=open(path+"/Analysis/"+name_strain+"/PlasmidFinder/PlasmidFinder_results","w+")
plasmidfinder_recall=open(path+"/Analysis/"+name_strain+"/PlasmidFinder/PlasmidFinder_recall","w+")
unalignments_plasmidfinder=open(path+"/Analysis/"+name_strain+"/PlasmidFinder/unalignments_plasmidfinder","w+")

def new_pSPAdes(contigs_plasmidspades=contigs_plasmidspades,file_alignments_plasmidspades=file_alignments_plasmidspades,file_unalignments_plasmidspades=file_unalignments_plasmidspades,median_coverage=chr_coverage,name_strain=name_strain):
    import re 
    import os
    file=open(contigs_plasmidspades,'r')
    list=[]
    #We read the contigs.fasta file obtained from plasmidSPAdes
    condition=os.stat(contigs_plasmidspades).st_size == 0
    if condition==True:
        return "No file provided or no plasmids detected by plasmidSPAdes"
    else:
        header= file.readline()
        while len(header) != 0: #this while loop is used to read a fasta file and store the sequences in a list
            seq = []
            seq.append(header[1:-1])
            r = ''
            while True:
                s=file.readline()
                if len(s)==0 or s[0] == '>':
                    header = s
                    break
                r=r+s[:-1]
            seq.append(r)
            list.append(seq)
        lista1=list
        dict_components={}
        dict_length={}
        dict_components_chromosome={}
        dict_components_plasmid={}
        dict_components_new_plasmids={}
        dict_nodes={}
#	
	#We generate the keys of the different dictionaries. The keys correspond to the name of the component assigned by plasmidSPAdes
        for i in lista1:
            sep=re.split('\_',i[0])
            name_component=sep[7]
            dict_components[name_component]=[]
            dict_length[name_component]=0
            dict_components_plasmid[name_component]=0
            dict_components_new_plasmids[name_component]=0
            dict_components_chromosome[name_component]=0
            dict_nodes[i[0]]=""

        #The length of each component is calculated:
        for i in lista1:
            sep=re.split('\_',i[0])
            length_contig=int(sep[3])
            name_component=sep[7]
            dict_components[name_component].append(i[0])
            dict_length[name_component]+=length_contig
            dict_nodes[i[0]]=i[1]

       
	dict_original=dict_length
        
       # We start to write the file with the results obtained by plasmidSPAdes
       #pspades.write("Plasmids predicted by pspades"+"\n")
        for i in dict_original.keys():
	  length=str(dict_original[i])
	  pspades.write("Plasmid"+'\t'+i+'\t'+length+'\n')
	  
        list_integers=[]
        list_contigs_plasmids=[]
        list_all_plasmids=[]
        for i in lista1:
            sep=re.split('\_',i[0])
            integer=sep[-1]
            integer=int(integer)
            list_contigs_plasmids.append(integer)
            if integer in list_integers:
                continue
            else:
                list_integers.append(integer)
        dict_test={}
        dict_contigs={}
        dict_coverage={}
        dict_contigs2={}
       
        for i in list_integers:
            llave=str(i)
            dict_test[llave]=0
            dict_contigs[llave]=0
            dict_coverage[llave]=0
            dict_contigs2[llave]=0
        total_length=0
        
        for i in lista1:
            sep=re.split('\_',i[0])
            integer=sep[-1]
            integer2=int(sep[3])
            total_length+=integer2
            integer3=float(sep[5])
            name_plasmid=str(sep[1])
            list_all_plasmids.append(name_plasmid)
            if integer2>1000:
                dict_contigs2[integer]+=1
                dict_coverage[integer]+=integer3
            dict_contigs[integer]+=1
            dict_test[integer]+=integer2    

        #We write the number of contigs (integer3) greater than 1000bp of each component
#        pspades.write('\n')
#        pspades.write("Number of contigs > 1000pb composing each plasmid"+"\n")
        for i in dict_contigs2:
            num_contigs=str(dict_contigs2[i])
            pspades.write("Plasmid"+"\t"+i+"\t"+"number of contigs"+"\t"+num_contigs+"\n")

	#Using the chromosome coverage available in the spades.log file, we can calculate the coverage ratio of each component
#        pspades.write("\n"+"Median coverage of each plasmid"+"\n")
        for i in dict_coverage:
            numerador=dict_coverage[i]
            denominador=float(dict_contigs2[i])
            coverage_plasmid=numerador/denominador
            ratio=coverage_plasmid/float(median_coverage)
            str_ratio=str(ratio)
            str_name_plasmid=str(i)
            str_coverage=str(coverage_plasmid)
            pspades.write("Plasmid"+"\t"+str_name_plasmid+"\t"+"have a coverage of"+"\t"+str_coverage+"\t"+"and a ratio (copy number) of"+"\t"+str_ratio+"\n")
#    
    #Below we start to analyze the output given by Quast and Nucmer    
    import csv
    dict_alignments={}
    dict_alignments_chromosome={}
    dict_alignments_plasmids={}
#    
    #We check the length of the contigs from each component which correspond to reference plasmids. 
    os.chdir(path+"/Analysis/All_Genomes")
    all_strains=open("all_plasmidSPAdes_R","a+")
    chr_contigs_pspades=open("chr_contigs_plasmidSPAdes","a+")
    first_line=all_strains.readline()
    if first_line.startswith("Strain"):
        print ""
    else:
        all_strains.write("Strain"+"\t"+"Length"+"\t"+"Type"+"\t"+"Node"+"\t"+"Reference_genome"+"\n")
    all_strains.write(name_complete_strain+"\t"+pspades_plasmid_genome_fraction+"\t"+"Plasmid_genome_fraction"+"\t"+"Quast_report"+"\t"+"-"+"\n")
    all_strains.write(name_complete_strain+"\t"+pspades_chromosome_genome_fraction+"\t"+"Chromosome_genome_fraction"+"\t"+"Quast_report"+"\t"+"-"+"\n")
    with open(file_alignments_plasmidspades) as tsv:
        condition=os.stat(file_alignments_plasmidspades).st_size == 0
        if condition==True:
            print ""
        else:
            for line in csv.reader(tsv, dialect="excel-tab"):
                list_nodes=line[1:len(line)]
                list_nodes = map( lambda x: x, set (list_nodes) )
                genbank=line[0]
                sep=re.split('\_',genbank)
                if sep[-1]=="genome":
                    dict_alignments_chromosome[genbank]=list_nodes
                if sep[-1]=="sequence":
                    dict_alignments_plasmids[genbank]=list_nodes
            for i in dict_alignments_plasmids:
                 total_nodes=dict_alignments_plasmids[i]
                 for n in total_nodes:
                     sep=re.split('\_',n)
                     all_strains.write(name_complete_strain+"\t"+sep[3]+"\t"+"Reference_plasmid"+"\t"+n+"\t"+i+"\n")
            for i in dict_alignments_chromosome:
                 total_nodes=dict_alignments_chromosome[i]
                 print dict_alignments_chromosome
                 for n in total_nodes:
                     sep=re.split('\_',n)
                     all_strains.write(name_complete_strain+"\t"+sep[3]+"\t"+"Chromosome"+"\t"+n+"\t"+i+"\n")
                     chr_contigs_pspades.write(">"+name_strain+"_"+sep[1]+"_"+sep[3]+"\n")
                     chr_contigs_pspades.write(dict_nodes[n]+"\n")
    dict_unalignments={}
    dict_unalignments["No_reference"]=[]
    with open(file_unalignments_plasmidspades) as tsv:
        condition=os.stat(file_unalignments_plasmidspades).st_size == 0
        if condition==True:
            print ""
        else:    
            for line in csv.reader(tsv, dialect="excel-tab"):
                sequences_unaligned=line[0]
                unalignments_pspades.write(">"+sequences_unaligned+"\n")
                unalignments_pspades.write(dict_nodes[sequences_unaligned]+"\n")
                dict_unalignments["No_reference"].append(line[0])
                sep=re.split('\_',sequences_unaligned)
                all_strains.write(name_complete_strain+"\t"+sep[3]+"\t"+"New_plasmids"+"\t"+sequences_unaligned+"\t"+"No_reference"+"\n")
                

     
new_pSPAdes(contigs_plasmidspades=contigs_plasmidspades,file_alignments_plasmidspades=file_alignments_plasmidspades,file_unalignments_plasmidspades=file_unalignments_plasmidspades,median_coverage=chr_coverage,name_strain=name_strain)

print "PlasmidSPAdes results have been generated"
###############################Recycler####################################################################

def new_Recycler(file_recycler=plasmids_recycler,file_alignments_recycler=file_alignments_recycler,file_unalignments_recycler=file_unalignments_recycler,name_strain=name_strain):
    import re 
    import os
    import csv
    file=open(file_recycler,'r')
    list=[]
    #We read the contigs.fasta file obtained from plasmidSPAdes
    condition=os.stat(file_recycler).st_size == 0
    if condition==True:
        return "No file provided or no plasmids detected by Recycler"
    else:
        header= file.readline()
        while len(header) != 0: #this while loop is used to read a fasta file and store the sequences in a list
            seq = []
            seq.append(header[1:-1])
            r = ''
            while True:
                s=file.readline()
                if len(s)==0 or s[0] == '>':
                    header = s
                    break
                r=r+s[:-1]
            seq.append(r)
            list.append(seq)
        lista1=list
        
        dict_nodes={}
        for i in lista1:
            plasmid=i[0]
            sep=re.split('\_',plasmid)
            length=sep[3]
            coverage=sep[-1]
            recycler.write("Plasmid"+"\t"+length+"\t"+"with_a_coverage_of"+"\t"+coverage+"\n")
            dict_nodes[plasmid]=i[1]
    
    dict_alignments_chromosome={}
    dict_alignments_plasmids={}

    os.chdir(path+"/Analysis/All_Genomes")
    all_strains=open("all_Recycler_R","a+")
    chr_contigs_recycler=open(path+"/Analysis/All_Genomes/chr_contigs_Recycler","a+")
    first_line=all_strains.readline()
    if first_line.startswith("Strain"):
        print ""
    else:
        all_strains.write("Strain"+"\t"+"Length"+"\t"+"Type"+"\t"+"Node"+"\t"+"Reference_genome"+"\n")
     
    if recycler_plasmid_genome_fraction=="":
	all_strains.write(name_complete_strain+"\t"+"0.0"+"\t"+"Plasmid_genome_fraction"+"\t"+"Quast_report"+"\t"+"-"+"\n")
    else:
	all_strains.write(name_complete_strain+"\t"+recycler_plasmid_genome_fraction+"\t"+"Plasmid_genome_fraction"+"\t"+"Quast_report"+"\t"+"-"+"\n")
    if recycler_chromosome_genome_fraction=="":
	all_strains.write(name_complete_strain+"\t"+"0.0"+"\t"+"Chromosome_genome_fraction"+"\t"+"Quast_report"+"\t"+"-"+"\n")
    else:
	all_strains.write(name_complete_strain+"\t"+recycler_chromosome_genome_fraction+"\t"+"Chromosome_genome_fraction"+"\t"+"Quast_report"+"\t"+"-"+"\n")
        
    with open(file_alignments_recycler) as tsv:
        condition=os.stat(file_alignments_recycler).st_size == 0
        if condition==True:
	  for i in lista1:
	    plasmid=i[0]
	    sep=re.split('\_',plasmid)
	    length=sep[3]
	    recycler.write("Plasmid_number_"+sep[1]+"\t"+"with_length_of"+"\t"+sep[3]+"\t"+"is_considered_as_new_plasmid"+"\n")
	    all_strains.write(name_complete_strain+"\t"+sep[3]+"\t"+"New_plasmids"+"\t"+plasmid+"\t"+"No_reference"+"\n")
	    unalignments_recycler.write(">"+plasmid+"\n")
	    unalignments_recycler.write(i[1]+"\n")
          return "No file provided or no plasmids detected by Recycler"
        else:     
            for line in csv.reader(tsv, dialect="excel-tab"):
                list_nodes=line[1:len(line)]
                list_nodes = map( lambda x: x, set (list_nodes) )
                genbank=line[0]
                sep=re.split('\_',genbank)
                if sep[-1]=="genome":
                    dict_alignments_chromosome[genbank]=list_nodes
                if sep[-1]=="sequence":
                    dict_alignments_plasmids[genbank]=list_nodes
            for i in dict_alignments_plasmids:
                total_nodes=dict_alignments_plasmids[i]
                for n in total_nodes:
                    sep=re.split('\_',n)
                    all_strains.write(name_complete_strain+"\t"+sep[3]+"\t"+"Reference_plasmid"+"\t"+n+"\t"+i+"\n")
                    recycler.write("Plasmid_number_"+sep[1]+"\t"+"with_length_of"+"\t"+sep[3]+"\t"+i+"\n")
            for i in dict_alignments_chromosome:
                total_nodes=dict_alignments_chromosome[i]
                for n in total_nodes:
                    sep=re.split('\_',n)
                    all_strains.write(name_complete_strain+"\t"+sep[3]+"\t"+"Chromosome"+"\t"+n+"\t"+i+"\n")
                    recycler.write("Plasmid_number_"+sep[1]+"\t"+"with_length_of"+"\t"+sep[3]+"\t"+i+"\n")
                    chr_contigs_recycler.write(">"+name_strain+"_"+sep[1]+"_"+sep[3]+"\n")
                    chr_contigs_recycler.write(dict_nodes[n]+"\n")
   
    dict_unalignments={}
    dict_unalignments["No_reference"]=[]
    with open(file_unalignments_recycler) as tsv:
        condition=os.stat(file_unalignments_recycler).st_size == 0
        if condition==True:
            print ""
            #pspades.write("No contigs unalign to the reference plasmids or no file provided")
        else:    
            for line in csv.reader(tsv, dialect="excel-tab"):
                sequences_unaligned=line[0]
                seq2=sequences_unaligned	
                unalignments_recycler.write(">"+seq2+"\n")
                unalignments_recycler.write(dict_nodes[sequences_unaligned]+"\n")
                dict_unalignments["No_reference"].append(line[0])
                sep=re.split('\_',sequences_unaligned)
                all_strains.write(name_complete_strain+"\t"+sep[3]+"\t"+"New_plasmids"+"\t"+sequences_unaligned+"\t"+"No_reference"+"\n")
                recycler.write("Plasmid_number_"+sep[1]+"\t"+"with_length_of"+"\t"+sep[3]+"\t"+"is not present in the reference assembly"+"\n")
new_Recycler(file_recycler=plasmids_recycler,file_alignments_recycler=file_alignments_recycler,file_unalignments_recycler=file_unalignments_recycler,name_strain=name_strain)

print "Recycler results have been generated"


################################################cBAR#############################################################################


def cbar(file_contigs=contigs_alignments,file_cbar=cbar_spades,contigs_spades=contigs_spades,contigs_unaligning=unaligned_contigs_spades):
    import csv
    import re
    import os
    os.chdir(path+"/Analysis/"+name_strain+"/cBAR")
    cbar=open("cBAR_results","w+")
    
    dict_chr={}
    dict_plasmids={}
    list_unalignments=[]
    
    file=open(contigs_spades,'r')
    list=[]
    #We read the contigs.fasta file obtained from SPAdes
    condition=os.stat(contigs_spades).st_size == 0
    if condition==True:
        return "No file provided or no plasmids detected by SPAdes"
    else:
        header= file.readline()
        while len(header) != 0: #this while loop is used to read a fasta file and store the sequences in a list
            seq = []
            seq.append(header[1:-1])
            r = ''
            while True:
                s=file.readline()
                if len(s)==0 or s[0] == '>':
                    header = s
                    break
                r=r+s[:-1]
            seq.append(r)
            list.append(seq)
        lista1=list
    
    dict_nodes_spades={}
    
    for i in lista1:
      dict_nodes_spades[i[0]]=""
      dict_nodes_spades[i[0]]=i[1]
    
   
    with open(file_contigs) as tsv:
        condition=os.stat(file_contigs).st_size == 0
        if condition==True:
            print "No file provided or no plasmids detected by Recycler"
        else:
            for line in csv.reader(tsv, dialect="excel-tab"):
                sep=re.split('\_',line[0])
                if sep[-1]=="genome":
                    dict_chr[line[0]]=[]
                    dict_chr[line[0]].append(line[1:])
                else:
                    dict_plasmids[line[0]]=[]
                    dict_plasmids[line[0]].append(line[1:])
    with open(contigs_unaligning) as tsv:
        condition=os.stat(contigs_unaligning).st_size == 0
        if condition==True:
            print "No contigs unaligning"
        else:
	    for line in csv.reader(tsv, dialect="excel-tab"):
	      list_unalignments.append(line[0])            
    with open(file_cbar) as tsv:
        condition=os.stat(file_cbar).st_size == 0
        if condition==True:
            print "There is some error with cbar, please check"
        else:
            os.chdir(path+"/Analysis/All_Genomes")
            all_strains=open("all_cbar_R","a+")
            first_line=all_strains.readline()
            if first_line.startswith("Strain"):
                print ""
            else:
                all_strains.write("Strain"+"\t"+"Length"+"\t"+"Type"+"\t"+"Node"+"\t"+"Reference_genome"+"\n")
            list_nodes=[]
            for line in csv.reader(tsv, dialect="excel-tab"):
                sep=re.split('\_',line[0])
                if len(sep) > 3:
                    length=int(sep[3])
                    if length > 500:
                        if line[2]=="Plasmid":
                            for i in dict_plasmids:
                                name_node=line[0]
                                sep=re.split('\_',name_node)
                                for n in dict_plasmids[i]:
                                    if name_node in n:
				        short_name_plasmid=re.split('\_',i)
				        print short_name_plasmid
                                        cbar.write(name_node+"\t"+"Reference_plasmid"+"\t"+i+"\n")
                                        all_strains.write(name_complete_strain+"\t"+sep[3]+"\t"+"Reference_plasmid"+"\t"+name_node+"\t"+i+"\n")
                                        if name_node in list_nodes:
					  print ""
					else:
					  cbar_recall.write(">"+name_node+"\n")
					  cbar_recall.write(dict_nodes_spades[name_node]+"\n")
					  list_nodes.append(name_node)
                            for w in dict_chr:
				name_node=line[0]
				sep=re.split('\_',name_node)
				for y in dict_chr[w]:
				    if name_node in y:
					cbar.write(name_node+"\t"+"Chromosome"+"\t"+w+"\n")
					all_strains.write(name_complete_strain+"\t"+sep[3]+"\t"+"Chromosome"+"\t"+name_node+"\t"+w+"\n")
					if name_node in list_nodes:
					  print ""
					else:
					  cbar_recall.write(">"+name_node+"\n")
					  cbar_recall.write(dict_nodes_spades[name_node]+"\n")
					  list_nodes.append(name_node)
				 
			    if name_node in list_unalignments:
				cbar.write(name_node+"\t"+"New_plasmids"+"\n")
				all_strains.write(name_complete_strain+"\t"+sep[3]+"\t"+"New_plasmids"+"\t"+name_node+"\t"+"No_reference"+"\n")
				unalignments_cbar.write(">"+name_node+"\n")
				unalignments_cbar.write(dict_nodes_spades[name_node]+"\n")
				if name_node in list_nodes:
				  print ""
				else:
				  cbar_recall.write(">"+name_node+"\n")
				  cbar_recall.write(dict_nodes_spades[name_node]+"\n")
				  list_nodes.append(name_node)
				  
cbar(file_contigs=contigs_alignments,file_cbar=cbar_spades,contigs_spades=contigs_spades,contigs_unaligning=unaligned_contigs_spades)

print "cBAR results have been generated"

###################PlasmidFinder#########################################################


def plasmidfinder(file_contigs=contigs_alignments,file_plasmidfinder=plasmidfinder_results,contigs_spades=contigs_spades,contigs_unaligning=unaligned_contigs_spades):
    
    import csv
    import re
    import os
    
    file=open(contigs_spades,'r')
    list=[]
    #We read the contigs.fasta file obtained from plasmidSPAdes
    condition=os.stat(contigs_spades).st_size == 0
    if condition==True:
        return "No file provided or no plasmids detected by plasmidSPAdes"
    else:
        header= file.readline()
        while len(header) != 0: #this while loop is used to read a fasta file and store the sequences in a list
            seq = []
            seq.append(header[1:-1])
            r = ''
            while True:
                s=file.readline()
                if len(s)==0 or s[0] == '>':
                    header = s
                    break
                r=r+s[:-1]
            seq.append(r)
            list.append(seq)
        lista1=list
    
    dict_nodes_spades={}
    list_unalignments=[]
    
    for i in lista1:
      dict_nodes_spades[i[0]]=""
      dict_nodes_spades[i[0]]=i[1]
    
    dict_chr={}
    dict_plasmids={}
    
    with open(contigs_unaligning) as tsv:
        condition=os.stat(contigs_unaligning).st_size == 0
        if condition==True:
            print "No contigs unaligning"
        else:
	    for line in csv.reader(tsv, dialect="excel-tab"):
	      list_unalignments.append(line[0])            
    
    
    with open(file_contigs) as tsv:
        condition=os.stat(file_contigs).st_size == 0
        if condition==True:
            print "No file provided or no plasmids detected by Recycler"
        else:
            for line in csv.reader(tsv, dialect="excel-tab"):
                sep=re.split('\_',line[0])
                if sep[-1]=="genome":
                    dict_chr[line[0]]=[]
                    dict_chr[line[0]].append(line[1:])
                else:
                    dict_plasmids[line[0]]=[]
                    dict_plasmids[line[0]].append(line[1:])
    
    os.chdir(path+"/Analysis/All_Genomes")
    all_strains=open("all_plasmidfinder_R","a+")
    first_line=all_strains.readline()
    if first_line.startswith("Strain"):
      print ""
    else:
      all_strains.write("Strain"+"\t"+"Length"+"\t"+"Type"+"\t"+"Node"+"\t"+"Reference_genome"+"\n")
    with open(file_plasmidfinder) as tsv:
      condition=os.stat(file_plasmidfinder).st_size == 0
      if condition==True:
	print "There is some error with PlasmidFinder or no plasmids have been detected"
      else:
	for line in csv.reader(tsv, dialect="excel-tab"):
	  name_node=line[0]
	  sep=re.split('\_',line[0])
	  if int(sep[3])>500:
	    for i in dict_plasmids:
	      for n in dict_plasmids[i]:
		if name_node in n:
		  plasmidfinder_output.write(name_node+"\t"+"Reference_plasmid"+"\n")
		  all_strains.write(name_complete_strain+"\t"+sep[3]+"\t"+"Reference_plasmid"+"\t"+name_node+"\t"+i+"\n")
		  plasmidfinder_recall.write(">"+name_node+"\n")
		  plasmidfinder_recall.write(dict_nodes_spades[name_node]+"\n")
		
	
	    for y in dict_chr:
	      for z in dict_chr[y]:
		if name_node in z:
		  plasmidfinder_output.write(name_node+"\t"+"Chromosome"+"\n")
		  all_strains.write(name_complete_strain+"\t"+sep[3]+"\t"+"Chromosome"+"\t"+name_node+"\t"+y+"\n")
		
	    if name_node in list_unalignments:
		  plasmidfinder_output.write(name_node+"\t"+"New_plasmids"+"\n")
		  all_strains.write(name_complete_strain+"\t"+sep[3]+"\t"+"New_plasmids"+"\t"+name_node+"\t"+"No_reference"+"\n")
		  unalignments_plasmidfinder.write(">"+name_node+"\n")
		  unalignments_plasmidfinder.write(dict_nodes_spades[name_node]+"\n")

	

plasmidfinder(contigs_alignments,plasmidfinder_results,contigs_spades=contigs_spades,contigs_unaligning=unaligned_contigs_spades)

print "PlasmidFinder results have been generated"