# -*- coding: utf-8 -*-
"""
Created on Mon Jul 11 09:38:25 2016

@author: arredondo
"""
import os
import re
dict_nodes={}
dict_hsp={}
dict_length={}
name_strain=os.environ["strain"]
file_plasmidfinder=os.environ["file_plasmidfinder"]
path=os.environ["PWD"]
os.chdir(path+"/Analysis/"+name_strain+"/PlasmidFinder")
plasmidfinder=open('plasmidfinder_results',"w+")
def parse_plasmidfinder(file_plasmidfinder=file_plasmidfinder):
    with open(file_plasmidfinder) as f:
        list_blast = f.read().splitlines() 
        for i in list_blast:
            if i.startswith("  <Iteration_query-def>"):
                sep=re.split('\>',i)
                sep_def=re.split('\<',sep[1])
                dict_nodes[sep_def[0]]=[]
                dict_hsp[sep_def[0]]=[]
                dict_length[sep_def[0]]=[]
            elif i.startswith("  <Hit_id>"):
                sep2=re.split('\>',i)
                sep2_def=re.split('\<',sep2[1])
                id_plasmid=sep2_def[0]
                dict_nodes[sep_def[0]].append(id_plasmid)
            elif i.startswith("  <Hit_len>"):
                sep_len=re.split('\>',i)
                seplen_def=re.split('\<',sep_len[1])
                length_plasmid=seplen_def[0]
                dict_length[sep_def[0]].append(length_plasmid)
            elif i.startswith("      <Hsp_align-len>"):
                sep3=re.split('\>',i)
                sep3_def=re.split('\<',sep3[1])
                hsp_score=sep3_def[0]
                dict_hsp[sep_def[0]].append(hsp_score)
    
                
    for i in dict_nodes.keys():
        if dict_nodes[i]!=[]:
            id_plasmids=dict_nodes[i]
            hsp_score=dict_hsp[i]
            hsp_score=hsp_score[0]
            length=dict_length[i]
            length=length[0]
            print hsp_score
            print length
            hsp_score=float(hsp_score)
            length=float(length)
            if hsp_score/length > 0.6:
	      plasmidfinder.write(i+"\t"+str(id_plasmids)+"\t"+str(hsp_score)+ "\t"+str(length)+"\n")

parse_plasmidfinder(file_plasmidfinder)

