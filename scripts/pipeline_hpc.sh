#!/bin/bash
set -x
set -u
#$ -S /bin/bash
#$ -o /hpc/dla_mm/salonso/outcomes
#$ -e /hpc/dla_mm/salonso/outcomes
#$ -cwd
#$ -M S.ArredondoAlonso@umcutrecht.nl
#$ -m beas
#$ -l h_vmem=80G
#$ -l h_rt=40:00:00
#$ -N BEST195_3_3_8	


echo "Checking the existence of the virtual environment."
echo "Checking..."
if [ -e /hpc/local/CentOS7/dla_mm/tools/miniconda2/envs/py27 ];then
	echo "Virtual environment already created"
else
	echo "We are going to create a new environment"
fi

export CONDA_OLD_PS1="[\u@\h \W]\$";

source activate /hpc/local/CentOS7/dla_mm/tools/miniconda2/envs/py27

prefetch -v $1

fastq-dump --outdir /hpc/dla_mm/salonso/$1 --gzip --skip-technical --readids --dumpbase --split-files --clip $1

rm /home/dla_mm/salonso/ncbi/public/sra/$1*

array=($(ls /hpc/dla_mm/salonso/$1))

fq_1=${array[0]}
fq_2=${array[1]}

seqtk trimfq /hpc/dla_mm/salonso/$1/$fq_1 > /hpc/dla_mm/salonso/$1/trim_1.fq
seqtk trimfq /hpc/dla_mm/salonso/$1/$fq_2 > /hpc/dla_mm/salonso/$1/trim_2.fq

args=("$@")
data_pspades=/hpc/dla_mm/salonso/additional_data_3_8/pspades_$1
data_recycler=/hpc/dla_mm/salonso/additional_data_3_8/recycler_$1
for i in "${args[@]}"
do
	if [ "$i" == '--pspades' -o "$i" == '--both' ];then
		python /hpc/dla_mm/salonso/SPAdes-3.8.2-Linux/bin/plasmidspades.py -t 4 --only-assembler -1 /hpc/dla_mm/salonso/$1/trim_1.fq -2 /hpc/dla_mm/salonso/$1/trim_2.fq -o $data_pspades
	fi
	if [ "$i" == '--recycler' -o "$i" == '--both' ];then
                python /hpc/dla_mm/salonso/SPAdes-3.8.2-Linux/bin/spades.py -t 4 --only-assembler -1 /hpc/dla_mm/salonso/$1/trim_1.fq -2 /hpc/dla_mm/salonso/$1/trim_2.fq -o $data_recycler

		kmer=$(grep "Running assembler" $data_recycler/spades.log | tail -1 | cut -d "K" -f 2)

		python /hpc/dla_mm/salonso/Recycler/make_fasta_from_fastg.py -g $data_recycler/assembly_graph.fastg

		bwa index $data_recycler/assembly_graph.nodes.fasta

		bwa mem $data_recycler/assembly_graph.nodes.fasta /hpc/dla_mm/salonso/$1/trim_1.fq /hpc/dla_mm/salonso/$1/trim_2.fq|samtools view -buS - > $data_recycler/reads_pe.bam

		samtools view -bF 0x0800 $data_recycler/reads_pe.bam > $data_recycler/reads_pe_primary.bam

		samtools sort $data_recycler/reads_pe_primary.bam > $data_recycler/reads_pe_primary.sort.bam

		samtools index $data_recycler/reads_pe_primary.sort.bam

		python /hpc/dla_mm/salonso/Recycler/recycle.py -g $data_recycler/assembly_graph.fastg -b $data_recycler/reads_pe_primary.sort.bam -k $kmer 		
        fi

done
		


