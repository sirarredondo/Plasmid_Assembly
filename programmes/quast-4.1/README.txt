QUAST README

QUAST stands for QUality ASsessment Tool.
The tool evaluates genome assemblies by computing various metrics. 
You can find all project news and the latest version of the tool at 
http://quast.sf.net 

A comprehensive HTML user manual is available in manual.html file of the package 
or at http://quast.sf.net/manual.html

Please refer to the INSTALL.txt file for installation instructions.

Please refer to the LICENSE.txt file for copyrights and citing instructions.
